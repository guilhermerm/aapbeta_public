<?php

class enviaEmail{
    
    public  $destinatarioemail;
    public  $destinatarionome;
    public  $assunto;   
    public  $mensagemHtml;
    public  $mensagem;
    
    /* 
     *    F U N Ç Õ E S   P R I N C I P A I S
     */
    
    function prepararEmail($destinatarionome,$destinatarioemail,$assunto,$mensagemHtml){
           $this->destinatarioemail = $destinatarioemail;
           $this->destinatarionome  = $destinatarionome;
           $this->assunto           = $assunto;
           $this->mensagemHtml      = $mensagemHtml;
           $this->mensagem          = $mensagemHtml;
    }
   
    function enviarEmail(){
        
        require_once 'phpmailer/PHPMailerAutoload.php';
        $mail = new PHPMailer;
        //$mail->SMTPDebug = 0;
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';      // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'prog1ufes20152@gmail.com';         // SMTP username
        $mail->Password = 'prog120152';                       // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to
        
        $mail->setFrom('prog1ufes20152@gmail.com', 'AAPbeta');
        $mail->addAddress($this->destinatarioemail, $this->destinatarionome);     // Add a recipient
        //$mail->addAddress('ellen@example.com');               // Name is optional
        $mail->addReplyTo('prog1ufes20152@gmail.com', 'AAPbeta');
        //$mail->addCC('cc@example.com');
        //$mail->addBCC('bcc@example.com');
        //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = '';
        $mail->Body    = $mensagemHtml;
        $mail->AltBody = $mensagem;
        
        if(!$mail->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
            return false;
        } else {
            return true;
        }
        

    }
        
}