--LISTAAASS DE EXERCICIOS

INSERT INTO `aeapbeta`.`listadeexercicio` (`id`, `titulo`, `texto`, `prazoenvio`, `socializar`, `datasocializacao`, `datapublicacao`, `oculto`, `datacriacao`, `deletado`, `permitirenvio`) VALUES (NULL, 'Lista 01 - 13/08/2015', 'Atualize o seu Wiki Individual com as soluções dos exercícios realizados.
Lembre-se, um programa não é apenas o código, realize os planos de teste, a solução, a codificação e os resultados dos testes.', '2015-09-12 00:00:00', '1', '2015-09-10 00:00:00', '2015-09-08 00:00:00', '0', '2015-08-13 00:00:00', '0', '1');



INSERT INTO `aeapbeta`.`listadeexercicio` (`id`, `titulo`, `texto`, `prazoenvio`, `socializar`, `datasocializacao`, `datapublicacao`, `oculto`, `datacriacao`, `deletado`, `permitirenvio`) VALUES (NULL, 'Lista 02 - 27/08/2015', 'Atualize o seu Wiki Individual com as soluções dos exercícios realizados.
Lembre-se, um programa não é apenas o código, realize os planos de teste, a solução, a codificação e os resultados dos testes.', '2015-09-13 00:00:00', '1', '2015-09-13 00:00:00', '2015-09-08 00:00:00', '0', '2015-08-27 00:00:00', '0', '1');



INSERT INTO `aeapbeta`.`listadeexercicio` (`id`, `titulo`, `texto`, `prazoenvio`, `socializar`, `datasocializacao`, `datapublicacao`, `oculto`, `datacriacao`, `deletado`, `permitirenvio`) VALUES (NULL, 'Lista 03 - 03/09/2015', 'Atualize o seu Wiki Individual com o relatório das soluções dos exercícios realizados.
Lembre-se, um programa não é apenas o código, realize os planos de teste, a solução, a codificação e os resultados dos testes.
Vá construindo o seu relatório de desenvolvimento de cada exercício enquanto o resolve. Inclua nesse relatório todas as atividades realizadas, tanto aquelas bem sucedidas quanto as tentativas sem sucesso.', '2015-09-13 00:00:00', '1', '2015-09-13 00:00:00', '2015-09-08 00:00:00', '0', '2015-09-03 00:00:00', '0', '1');




-- EXERCICIOSSS

INSERT INTO `aeapbeta`.`exercicio` (`id`, `titulo`, `enunciado`, `mostraplano`, `tiposolucao`, `datacriacao`, `deletado`) VALUES (NULL, 'Lista 1 - Exercício 2', 'Elabore uma função em Haskell que encontre a área de um retângulo, nos quais os vértices do retângulo são pontos de um plano cartesiano, utilize como dados de entrada dois vértices diagonalmente opostos do retângulo. Exemplo de interface da função:
    areaRetangulo <Parâmetros> = <Corpo da Função>', '0', 'mcp', '2015-08-13 00:00:00', '0'), (NULL, 'Lista 1 - Exercício 3', 'Dadas 2 coordenadas do plano cartesiano, especificar uma função haskell que determine a distância (mínima) entre as 2 coordenadas.', '0', 'mcp', '2015-08-13 00:00:00', '0');

INSERT INTO `aeapbeta`.`exercicio` (`id`, `titulo`, `enunciado`, `mostraplano`, `tiposolucao`, `datacriacao`, `deletado`) VALUES (NULL, 'Lista 1 - Exercício 4', 'Descreva uma função Haskell que calcule a área de um triângulo que se conhece as coordenadas dos três vértices dele: (x1,y1), (x2,y2) e (x3,y3).
', '0', 'mcp', '2015-08-13 00:00:00', '0'), (NULL, 'Lista 2 - Exercício 1', 'Codifique a solução do problema passada na última aula:
Dado um retângulo no plano cartesiano, definido por um par de coordenadas (vértices opostos do retângulo) e dois pontos (duas coordenadas), elaborar uma função que retorne: True, se os dois pontos estiverem no mesmo quadrante interno do retângulo, ou False, em caso contrário.
', '0', 'mcp', '2015-08-13 00:00:00', '0'), (NULL, 'Lista 2 - Exercício 2', 'Dado um retângulo no plano cartesiano, definido por um par de coordenadas (vértices opostos do retângulo) e trêz pontos (trêz coordenadas), elaborar uma função em Haskell que retorne em qual região do plano cartesiano está cada uma das 3 coordenadas.
Sendo:
1 - Quadrante interno 1 (do retângulo)
2 - Quadrante interno 2 (do retângulo)
3 - Quadrante interno 3 (do retângulo)
4 - Quadrante interno 4 (do retângulo)
5 - Ponto interno ao retângulo sobre a fronteira dos quadrantes (fronteira interna)
6 - Ponto no limite do retângulo (fronteira externa)
7 - Fora do retângulo', '0', 'mcp', '2015-08-13 00:00:00', '0'), (NULL, 'Lista 3 - Exercício 1', 'Elabore um programa e codifique-o em Haskell para receber a coordenada (xc,yc) do centro de um círculo, o raio r desse círculo e um ponto (x,y) do plano cartesiano e retornar os seguintes valores inteiros:
 
0, se o ponto (x,y) está alinhado na mesma ordenada yc do centro do círculo;
1, se o ponto (x,y) está na metade superior do círculo;
2, se o ponto (x,y) está na metade inferior do círculo;
3, se o ponto (x,y) está na borda do círculo;
4, se o ponto (x,y) está fora do círculo.', '0', 'mcp', '2015-09-03 00:00:00', '0'), (NULL, 'Lista 3 - Exercício 2', 'Elabore um programa e codifique-o em Haskell para receber o salário de um funcionário e calcular o imposto de renda que incide sobre esse salário, conforme a tabela a seguir:
 
Faixa do Salário   - Taxa Imposto de Renda
< 1700             - 0% 
[ 1700 ... 8000 )  - 10% 
[ 8000 ... 15000 ) - 20% 
>= 15000           - 30% 
 
Exemplos de uso:
imposto 1600.00
0.0 
imposto 1700.00
170.00
', '0', 'mcp', '2015-09-03 00:00:00', '0'), (NULL, 'Lista 3 - Exercício 3', 'Calcular o desconto por falta que um funcionário deve ter em seu salário. Considere que cada falta implica em um desconto de 1/30 do salário dele.
 
descfalt <salario> <falta>
', '0', 'mcp', '2015-09-03 00:00:00', '0'), (NULL, 'Lista 3 - Exercício 4', 'Dado que um dia normal de trabalho corresponda à 8 horas, faça uma função que receba o salário de um funcionário e calcule o valor que corresponda a uma hora extra trabalhada por esse funcionário. Considere que o valor de cada hora extra seja 20% superior à hora normal de trabalho dele. A hora normal é dada pela expressão: 
 
22 * 8 = 176 
1 hora salário = salario / 176 ', '0', 'mcp', '2015-09-03 00:00:00', '0'), (NULL, 'Lista 3 - Exercício 5', 'Dados o salário mensal, a quantidade de faltas e a quantidade de horas extras de um funcionário, calcular o salário líquido dele. Use as fórmulas a seguir:
 
<salario bruto> = <salario mensal> + <adicional por hora extra>
<salario liquido> = <salario bruto> - <desconto por falta> - imposto de renda>
<imposto de renda> = (<salario bruto> - < desconto por falta>) * <taxa IR>
<taxa IR> = taxa/100
 	1 hora extra = (salario/ 176) * 1.2', '0', 'mcp', '2015-09-03 00:00:00', '0');
