-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 03/12/2015 às 09:08
-- Versão do servidor: 5.5.44-0ubuntu0.14.04.1
-- Versão do PHP: 5.5.9-1ubuntu4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de dados: `aeapbeta`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `comentario`
--

CREATE TABLE IF NOT EXISTS `comentario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(11) NOT NULL,
  `idsolucao` int(11) NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `datacriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletado` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=22 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `exercicio`
--

CREATE TABLE IF NOT EXISTS `exercicio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `enunciado` text COLLATE utf8_unicode_ci NOT NULL,
  `interface` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `mostraplano` tinyint(4) NOT NULL DEFAULT '0',
  `nivel` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `tiposolucao` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `datacriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletado` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=25 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `exerplanoteste`
--

CREATE TABLE IF NOT EXISTS `exerplanoteste` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idexercicio` int(11) NOT NULL,
  `deletado` tinyint(4) NOT NULL DEFAULT '0',
  `datacriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=78 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `exerteste`
--

CREATE TABLE IF NOT EXISTS `exerteste` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idplanoteste` int(11) NOT NULL,
  `descricao` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `entrada` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `saidaesperada` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `deletado` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=22 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `listadeexercicio`
--

CREATE TABLE IF NOT EXISTS `listadeexercicio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `prazoenvio` datetime NOT NULL,
  `socializar` tinyint(4) NOT NULL,
  `datasocializacao` datetime NOT NULL,
  `datapublicacao` datetime NOT NULL,
  `oculto` tinyint(4) NOT NULL,
  `datacriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletado` tinyint(4) NOT NULL DEFAULT '0',
  `permitirenvio` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=23 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `lista_exercicio`
--

CREATE TABLE IF NOT EXISTS `lista_exercicio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ide` int(11) NOT NULL,
  `idlst` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=206 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `perfil`
--

CREATE TABLE IF NOT EXISTS `perfil` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `perfil` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `projeto`
--

CREATE TABLE IF NOT EXISTS `projeto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `enunciado` text COLLATE utf8_unicode_ci NOT NULL,
  `datacriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletado` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `projetosolucao`
--

CREATE TABLE IF NOT EXISTS `projetosolucao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idprojeto` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `solucao` text COLLATE utf8_unicode_ci NOT NULL,
  `codigohaskell` text COLLATE utf8_unicode_ci NOT NULL,
  `instrucoes` text COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `datacriacao` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deletado` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `socializacao`
--

CREATE TABLE IF NOT EXISTS `socializacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idlst` int(11) NOT NULL,
  `ide` int(11) NOT NULL,
  `socializar` tinyint(1) NOT NULL DEFAULT '1',
  `para` tinyint(4) NOT NULL,
  `datacriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=43 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `solucao`
--

CREATE TABLE IF NOT EXISTS `solucao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idexercicio` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `compreensao` text COLLATE utf8_unicode_ci NOT NULL,
  `solucao` text COLLATE utf8_unicode_ci NOT NULL,
  `codigohaskell` text COLLATE utf8_unicode_ci NOT NULL,
  `funcaoprincipal` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `socializar` tinyint(4) NOT NULL DEFAULT '0',
  `datacriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletado` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=227 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `soluplanoteste`
--

CREATE TABLE IF NOT EXISTS `soluplanoteste` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(11) NOT NULL,
  `idsolucao` int(11) NOT NULL,
  `deletado` tinyint(4) NOT NULL DEFAULT '0',
  `datacriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=221 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `soluresultado`
--

CREATE TABLE IF NOT EXISTS `soluresultado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idplanoteste` int(11) NOT NULL,
  `saidahugs` text COLLATE utf8_unicode_ci NOT NULL,
  `saidaresultado` text COLLATE utf8_unicode_ci NOT NULL,
  `deletado` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=28 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `soluteste`
--

CREATE TABLE IF NOT EXISTS `soluteste` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idplanoteste` int(11) NOT NULL,
  `descricao` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `entrada` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `saidaesperada` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `deletado` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=303 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_perfil` smallint(6) NOT NULL,
  `matricula` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `nome` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `foto` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `senha` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `deletado` tinyint(4) NOT NULL DEFAULT '0',
  `primeiroacesso` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_usuario_perfil` (`id_perfil`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `fk_usuario_perfil` FOREIGN KEY (`id_perfil`) REFERENCES `perfil` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
