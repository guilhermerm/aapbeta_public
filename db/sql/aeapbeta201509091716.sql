-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 09/09/2015 às 17:15
-- Versão do servidor: 5.5.44-0ubuntu0.14.04.1
-- Versão do PHP: 5.5.9-1ubuntu4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de dados: `aeapbeta`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `comentario`
--

CREATE TABLE IF NOT EXISTS `comentario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(11) NOT NULL,
  `idsolucao` int(11) NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `datacriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletado` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `exercicio`
--

CREATE TABLE IF NOT EXISTS `exercicio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `enunciado` text COLLATE utf8_unicode_ci NOT NULL,
  `mostraplano` tinyint(4) NOT NULL DEFAULT '0',
  `tiposolucao` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `datacriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletado` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Fazendo dump de dados para tabela `exercicio`
--

INSERT INTO `exercicio` (`id`, `titulo`, `enunciado`, `mostraplano`, `tiposolucao`, `datacriacao`, `deletado`) VALUES
(1, 'Lista 1 - ExercÃƒÂ­cio 2', 'Elabore uma função em Haskell que encontre a área de um retângulo, nos quais os vértices do retângulo são pontos de um plano cartesiano, utilize como dados de entrada dois vértices diagonalmente opostos do retângulo. Exemplo de interface da função:\r\n    areaRetangulo <Parâmetros> = <Corpo da Função>', 0, 'mcp', '2015-08-13 03:00:00', 0),
(2, 'Lista 1 - ExercÃƒÂ­cio 3', 'Dadas 2 coordenadas do plano cartesiano, especificar uma função haskell que determine a distância (mínima) entre as 2 coordenadas.', 0, 'mcp', '2015-08-13 03:00:00', 0),
(3, 'Lista 1 - ExercÃƒÂ­cio 4', 'Descreva uma função Haskell que calcule a área de um triângulo que se conhece as coordenadas dos três vértices dele: (x1,y1), (x2,y2) e (x3,y3).\r\n', 0, 'mcp', '2015-08-13 03:00:00', 0),
(4, 'Lista 2 - ExercÃƒÂ­cio 1', 'Codifique a solução do problema passada na última aula:\r\nDado um retângulo no plano cartesiano, definido por um par de coordenadas (vértices opostos do retângulo) e dois pontos (duas coordenadas), elaborar uma função que retorne: True, se os dois pontos estiverem no mesmo quadrante interno do retângulo, ou False, em caso contrário.\r\n', 0, 'mcp', '2015-08-13 03:00:00', 0),
(5, 'Lista 2 - ExercÃƒÂ­cio 2', 'Dado um retângulo no plano cartesiano, definido por um par de coordenadas (vértices opostos do retângulo) e trêz pontos (trêz coordenadas), elaborar uma função em Haskell que retorne em qual região do plano cartesiano está cada uma das 3 coordenadas.\r\nSendo:\r\n1 - Quadrante interno 1 (do retângulo)\r\n2 - Quadrante interno 2 (do retângulo)\r\n3 - Quadrante interno 3 (do retângulo)\r\n4 - Quadrante interno 4 (do retângulo)\r\n5 - Ponto interno ao retângulo sobre a fronteira dos quadrantes (fronteira interna)\r\n6 - Ponto no limite do retângulo (fronteira externa)\r\n7 - Fora do retângulo', 0, 'mcp', '2015-08-13 03:00:00', 0),
(6, 'Lista 3 - ExercÃƒÂ­cio 1', 'Elabore um programa e codifique-o em Haskell para receber a coordenada (xc,yc) do centro de um círculo, o raio r desse círculo e um ponto (x,y) do plano cartesiano e retornar os seguintes valores inteiros:\r\n \r\n0, se o ponto (x,y) está alinhado na mesma ordenada yc do centro do círculo;\r\n1, se o ponto (x,y) está na metade superior do círculo;\r\n2, se o ponto (x,y) está na metade inferior do círculo;\r\n3, se o ponto (x,y) está na borda do círculo;\r\n4, se o ponto (x,y) está fora do círculo.', 0, 'mcp', '2015-09-03 03:00:00', 0),
(7, 'Lista 3 - ExercÃƒÂ­cio 2', 'Elabore um programa e codifique-o em Haskell para receber o salário de um funcionário e calcular o imposto de renda que incide sobre esse salário, conforme a tabela a seguir:\r\n \r\nFaixa do Salário   - Taxa Imposto de Renda\r\n< 1700             - 0% \r\n[ 1700 ... 8000 )  - 10% \r\n[ 8000 ... 15000 ) - 20% \r\n>= 15000           - 30% \r\n \r\nExemplos de uso:\r\nimposto 1600.00\r\n0.0 \r\nimposto 1700.00\r\n170.00\r\n', 0, 'mcp', '2015-09-03 03:00:00', 0),
(8, 'Lista 3 - ExercÃƒÂ­cio 3', 'Calcular o desconto por falta que um funcionário deve ter em seu salário. Considere que cada falta implica em um desconto de 1/30 do salário dele.\r\n \r\ndescfalt <salario> <falta>\r\n', 0, 'mcp', '2015-09-03 03:00:00', 0),
(9, 'Lista 3 - ExercÃƒÂ­cio 4', 'Dado que um dia normal de trabalho corresponda à 8 horas, faça uma função que receba o salário de um funcionário e calcule o valor que corresponda a uma hora extra trabalhada por esse funcionário. Considere que o valor de cada hora extra seja 20% superior à hora normal de trabalho dele. A hora normal é dada pela expressão: \r\n \r\n22 * 8 = 176 \r\n1 hora salário = salario / 176 ', 0, 'mcp', '2015-09-03 03:00:00', 0),
(10, 'Lista 3 - ExercÃƒÂ­cio 5', 'Dados o salário mensal, a quantidade de faltas e a quantidade de horas extras de um funcionário, calcular o salário líquido dele. Use as fórmulas a seguir:\r\n \r\n<salario bruto> = <salario mensal> + <adicional por hora extra>\r\n<salario liquido> = <salario bruto> - <desconto por falta> - imposto de renda>\r\n<imposto de renda> = (<salario bruto> - < desconto por falta>) * <taxa IR>\r\n<taxa IR> = taxa/100\r\n 	1 hora extra = (salario/ 176) * 1.2', 0, 'mcp', '2015-09-03 03:00:00', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `exerplanoteste`
--

CREATE TABLE IF NOT EXISTS `exerplanoteste` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idexercicio` int(11) NOT NULL,
  `deletado` tinyint(4) NOT NULL DEFAULT '0',
  `datacriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `exerteste`
--

CREATE TABLE IF NOT EXISTS `exerteste` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idplanoteste` int(11) NOT NULL,
  `descricao` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `entrada` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `saidaesperada` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `deletado` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `listadeexercicio`
--

CREATE TABLE IF NOT EXISTS `listadeexercicio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `prazoenvio` datetime NOT NULL,
  `socializar` tinyint(4) NOT NULL,
  `datasocializacao` datetime NOT NULL,
  `datapublicacao` datetime NOT NULL,
  `oculto` tinyint(4) NOT NULL,
  `datacriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletado` tinyint(4) NOT NULL DEFAULT '0',
  `permitirenvio` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Fazendo dump de dados para tabela `listadeexercicio`
--

INSERT INTO `listadeexercicio` (`id`, `titulo`, `texto`, `prazoenvio`, `socializar`, `datasocializacao`, `datapublicacao`, `oculto`, `datacriacao`, `deletado`, `permitirenvio`) VALUES
(1, 'Lista 01 - 13/08/2015', 'Atualize o seu Wiki Individual com as soluções dos exercícios realizados.\r\nLembre-se, um programa não é apenas o código, realize os planos de teste, a solução, a codificação e os resultados dos testes.', '2015-09-12 00:00:00', 1, '2015-09-10 00:00:00', '2015-09-08 00:00:00', 0, '2015-08-13 03:00:00', 0, 1),
(2, 'Lista 02 - 27/08/2015', 'Atualize o seu Wiki Individual com as soluções dos exercícios realizados.\r\nLembre-se, um programa não é apenas o código, realize os planos de teste, a solução, a codificação e os resultados dos testes.', '2015-09-13 00:00:00', 1, '2015-09-13 00:00:00', '2015-09-08 00:00:00', 0, '2015-08-27 03:00:00', 0, 1),
(3, 'Lista 03 - 03/09/2015', 'Atualize o seu Wiki Individual com o relatório das soluções dos exercícios realizados.\r\nLembre-se, um programa não é apenas o código, realize os planos de teste, a solução, a codificação e os resultados dos testes.\r\nVá construindo o seu relatório de desenvolvimento de cada exercício enquanto o resolve. Inclua nesse relatório todas as atividades realizadas, tanto aquelas bem sucedidas quanto as tentativas sem sucesso.', '2015-09-13 00:00:00', 1, '2015-09-13 00:00:00', '2015-09-08 00:00:00', 0, '2015-09-03 03:00:00', 0, 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `lista_exercicio`
--

CREATE TABLE IF NOT EXISTS `lista_exercicio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ide` int(11) NOT NULL,
  `idlst` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `perfil`
--

CREATE TABLE IF NOT EXISTS `perfil` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `perfil` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Fazendo dump de dados para tabela `perfil`
--

INSERT INTO `perfil` (`id`, `perfil`) VALUES
(1, 'Administrador'),
(2, 'Professor'),
(3, 'Aluno');

-- --------------------------------------------------------

--
-- Estrutura para tabela `solucao`
--

CREATE TABLE IF NOT EXISTS `solucao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idexercicio` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `compreensao` text COLLATE utf8_unicode_ci NOT NULL,
  `solucao` text COLLATE utf8_unicode_ci NOT NULL,
  `codigohaskell` text COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `datacriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletado` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `soluplanoteste`
--

CREATE TABLE IF NOT EXISTS `soluplanoteste` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(11) NOT NULL,
  `idsolucao` int(11) NOT NULL,
  `deletado` tinyint(4) NOT NULL DEFAULT '0',
  `datacriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `soluteste`
--

CREATE TABLE IF NOT EXISTS `soluteste` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idplanoteste` int(11) NOT NULL,
  `descricao` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `entrada` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `saidaesperada` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `deletado` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_perfil` smallint(6) NOT NULL,
  `matricula` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `nome` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `foto` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `senha` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `deletado` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_usuario_perfil` (`id_perfil`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Fazendo dump de dados para tabela `usuario`
--

INSERT INTO `usuario` (`id`, `id_perfil`, `matricula`, `nome`, `email`, `foto`, `descricao`, `senha`, `deletado`) VALUES
(1, 1, '0000000000', 'Administrador', 'prog1ufes20152@gmail.com', 'user_adm.png', 'Administrador do Ambiente', '123456', 0);

--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `fk_usuario_perfil` FOREIGN KEY (`id_perfil`) REFERENCES `perfil` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
