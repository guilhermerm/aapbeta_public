<?php //
/*
* Classe de conexão a banco de dados MySQL Orientado a Objetos
* Autor:  Jair Rebello
* Fonte: http://www.escolacriatividade.com/php-orientado-a-objetos-conexao-a-banco-de-dados/
*/
 
class connectBD {
 
   /*
   * Declaração dos atributos da classe de conexão
   */
 
   private $host; // Endereço do servidor do banco de dados
   private $bd; // Banco de dados utilizado na conexão
   private $usuario; // Usuário do banco de dados que possua acesso ao schema
   private $senha; // Senha do usuário
   private $sql; // Consulta a ser executada
   
   public  $conexao; //Guarda conexão conectada
 
//   //Guilherme: Alteração minha
//   function connectBD() {
//        
//  
//   }
//   
   function conectar(){
      /*
      * Método que conecta ao banco de dados passando
      * os valores necessários para que a conexão ocorra
      */       
      //require_once "config.php";
     
//PRODUÇÃO
//$host    = "aap.inf.ufes.br";
//$bd      = "*******"; 
//$usuario = "********"; 
//$senha   = "*********";

//LOCALHOST
//$host    = "localhost";
//$bd      = "aeapbeta"; 
//$usuario = "root"; 
//$senha   = "root";

//LOCALHOSTSHOW (Para apresentação)
$host    = "localhost";
$bd      = "aeapshow"; 
$usuario = "root"; 
$senha   = "root";

       
      $this->host    = $host;
      $this->bd      = $bd;
      $this->usuario = $usuario;
      $this->senha   = $senha;
      
      $this->conexao = @mysql_connect($this->host,$this->usuario,$this->senha) or die($this->mensagem(mysql_error()));
            
      return $this->conexao;
   }
 
   function selecionarDB(){
      /*
      * Método que seleciona o banco de dados
      * com que irá trabalhar
      */
 
      $banco = @mysql_select_db($this->bd) or die($this->mensagem(mysql_error()));
      if($banco){
         return true;
      }else{
         return false;
      }
   }
 
    /*Guilherme: Inclusão minha
    * Método que executa uma query recebida como parâmetro no banco de dados
    */
    function query($sql){
//      echo "<br><br>SQL: ",$sql,"<br><br>";
      $query = mysql_query($sql) or die ($this->mensagem(mysql_error()));      
      return $query;
   }
   
   function executar(){
      /*
      * Método que executa uma query no banco de dados
      */
      $query = mysql_query($this->sql) or die ($this->mensagem(mysql_error()));
      return $query;
   }
   
   function set($propriedade,$valor){
      /*
      * Método criado para atribuir os valores as variáveis de conexão,
      * muito melhor que criar set's para cada variável
      */
      $this->$propriedade = $valor;
   }
 
   function mensagem($erro){
      /*
      * Função para exibir os possíveis erros
      * Separamos em um método, pois este pode ser estilizado,
      * sem alterar outros métodos
      */
      echo $erro;
   }

    public function idGerado() {        
        return mysql_insert_id();        
    }

}
