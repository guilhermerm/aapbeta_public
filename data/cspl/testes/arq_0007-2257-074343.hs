prim (a,b) = a
segd (a,b) = b


duplasAux [] = False
duplasAux ((a,b):r) =  if prim (a,b) == prim ((head r)) || segd (a,b) == segd ((head r)) then True
			 else duplasAux r


desCartas [] = []
desCartas ((a,b):r) = if duplasAux ((a,b):r) == True then desCartas r
		       else (a,b):(desCartas r)
