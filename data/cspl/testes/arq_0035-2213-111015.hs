area x1 x2 y1 y2 = if (x1 > x2) 
      then if (y1 > y2)
             then (x1-x2) * (y1-y2)
             else  (x1-x2) * (y2-y1)
      else if (y1 > y2)
             then (x2-x1) * (y1-y2)
             else (x2-x1) * (y2-y1)

