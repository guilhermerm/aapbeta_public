<?php
    //Endereços do ambiente
    $endatual = getcwd();
    $endaux   = explode("/",$endatual);
    $endamb   = "/".end($endaux);
    $endcomp  = $_SERVER ['HTTP_HOST'].$_SERVER ['REQUEST_URI'];
    //echo "EndAtual: ",$endatual,"<br>Endamb: ",$endamb,"<br>Endcomp: $endcomp <br>Page:$page"; 
    //echo "<pre>",var_dump($_SERVER),"</pre>";
   
    //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
    //"Incluindo bibliotecas"
    require_once "$endatual/db/conection.php";
    require_once "$endatual/apps/admin/mdl/usuario.php";
    require_once "$endatual/apps/admin/mdl/usuario_bd.php";    
    session_start();   
    require_once "$endatual/apps/admin/mdl/trancar.php";
    require_once "$endatual/apps/admin/mdl/trancar_bd.php";
    
    require_once "$endatual/apps/cspl/mdl/solucao.php";
    require_once "$endatual/apps/cspl/mdl/solucao_bd.php";
    require_once "$endatual/apps/cspl/mdl/exercicio.php";
    require_once "$endatual/apps/cspl/mdl/exercicio_bd.php";
    require_once "$endatual/apps/cspl/mdl/exerplanoteste.php";
    require_once "$endatual/apps/cspl/mdl/exerplanoteste_bd.php";      
    
    require_once "$endatual/apps/cspl/mdl/projeto.php";
    require_once "$endatual/apps/cspl/mdl/projeto_bd.php";
    
    //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
    //Inclusão de Templates (MVC)
    require_once("lib/raelgc/Template.php");
    use raelgc\Template;           
    //Construindo a "moldura" da página apartir do modelo
    $tpl = new Template("./apps/ambiente/tpl/layout.html");    
    //Inserindo o TOPO e o RODAPÉ da página
    $tpl->addFile("TOPOBARRA","./apps/ambiente/tpl/topobarra.html");
    $tpl->addFile("FOOTER"   ,"./apps/ambiente/tpl/footer.html");
            
 
    //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    //Verificando qual página requisitada
    if(isset($_GET['page'])){
        $page = $_GET['page'];
    }else{
        $page = 'inicio';        
    }
    

    //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    //Verificando LOGIN    
    if($page!='alterarsenha'){
        if(isset($_SESSION["aeapuser"])){
            //Reconhecimento do Link da página
            $userlogado = $_SESSION['aeapuser'];                
            //tpl: TOPOBARRA.html
            $tpl->LINK_INICIO = "index.php";
            $tpl->LINK_USER   = "$endamb/?page=config";
            $tpl->LINK_SAIR   = "sair.php";
            $tpl->USER_NOME   = $userlogado->nome;
            $tpl->block("BLOCK_LOGADO");
            if($page==='login') {
                header("Location: index.php");
                exit();
            }
        }else{
            $page = 'login';
        }
    }


    //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    //Verificar se ambiente está trancado (horário de prova)
    if(estaTrancado()){        
        $tpl->block("BLOCK_TRANCADO");
    }
    
        
    //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    //Escolhe página a ser mostrada
    switch ($page){
       
        //Inicio
        case 'login':
        case 'alterarsenha':
        case 'inicio': 
        case 'trancado': 
            //Inserindo o conteúdo central da página
            require_once './apps/ambiente/ctrl.php';
        break;
    
        //Administrativo
        case 'admin':
        case 'profile':
        case 'novousuario':
        case 'config':
        case 'listusuario':
        case 'configtranca':
            require_once './apps/admin/ctrl.php';
        break;
    
        //CSPL (Exercícios)        
        case 'cspl':
        case 'mostralistas':    //Lista     - Exibi TODAS as Listas
        case 'mostralistaexer': //Lista     - Mostra 1 Lista de exercícios
        case 'editlistaexer':   //Lista     - Editar Lista de exercícios
        case 'novalistaexer':   //Lista     - Nova Lista de exercícios
            
        case 'relatoriolistas':     //Relatorio - Relatório das Listas de Exercícios
        case 'relatoriolistaexer':  //Relatorio - Relatório de uma lista de exercícios
        case 'relatoriouserlistas': //Relatorio - Relatório de uma lista de exercícios
        case 'relatorioexer':       //Relatorio - Relatório de um exercício
            
        case 'baseexercicio':   //Exercício - Exibi TODOS os exercícios
        case 'exercicio':       //Exercício - Exibir um exercício
        case 'novoexercicio':   //Exercício - Novo exercício
        case 'editexercicio':   //Exercício - Editar exercício
            
        case 'listasolucao':    //Solução   - Soluções
        case 'mostrasolucao':   //Solução   - Mostra uma solucao
        case 'novasolucao':     //Solução   - Nova Solução
        case 'editsolucao':     //Solução   - Editar Solução
        
        case 'socializacao':    //Social    - Socialização das soluções
            
        case 'comparador': 
        case 'formardupla': 
            
        case 'mostraprojetos': 
        case 'mostraprojeto': 
        case 'editprojeto': 
        case 'novoprojeto': 
        
        case 'mostraprojetosolucao':
        case 'novoprojetosolucao':
        case 'editprojetosolucao':
            
            //Inserindo o conteúdo central da página
            require_once './apps/cspl/ctrl.php';
        break;
    
        case 'avaliacao':
            require_once 'testea.php';
        break;              
    
        default:
            //Não encontrado
            //require_once './apps/ambiente/ctrl.php';
            $tpl->addFile("CONTEUDO","./apps/ambiente/tpl/404.html");
        break;
    
    }
    
    //Exibir na tela a Página
    $tpl->show();
    

function inverteData($data){
    $partes = explode("-", $data);
    return $partes[2]."-".$partes[1]."-".$partes[0];
}

function pegaData($datahora){
    $partes = explode(" ", $datahora);
    return $partes[0];
}
function pegaHora($datahora){
    $partes = explode(" ", $datahora);
    return substr($partes[1], 0,5);
}
