<?php
include_once "usuario.php";
require_once "$endatual/db/conection.php";

 
/*
 *  C R E A T E
 */
// Recebe: objeto $usuario (sem id)
function inserirUsuario($objto){    
    //Realizando conexão como BD
    $con = gerarCon();         
    $sql = "INSERT INTO `usuario` (
                        `id_perfil`, 
                        `matricula`, 
                        `nome`, 
                        `email`, 
                        `foto`, 
                        `descricao`, 
                        `senha`,
                        `deletado`)
            VALUES ('$objto->perfil', 
                    '$objto->matricula', 
                    '$objto->nome', 
                    '$objto->email',
                    '$objto->foto', 
                    '$objto->descricao', 
                    '$objto->senha',
                    '$objto->deletado'
            )";
    
    $con->query($sql);
    $id = $con->idGerado();
    return $id;
}

/*
 *  R E A D
 */

//Buscar usuario pelo id
function buscarUsuarioId($id , $deletado=0){    
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT  id, id_perfil as perfil,matricula,nome,email,foto,descricao,senha,deletado
              FROM  usuario
             WHERE  id = $id
               AND  deletado = $deletado
             LIMIT  1";    
    
    $result = $con->query($sql);    
    $linha = mysql_fetch_assoc($result);    
    
//Instancia objeto usuario
    //Preenche os campos do objeto
    if($linha != NULL){
        $objto = new usuario();
        foreach($linha as $campo => $valor){
            $objto->$campo = $valor;
        }
    }
    
    if (isset($objto)){
        //Retorna Array de Usuarios
        return $objto; 
    }else{
        return NULL;
    }
}

//Buscar usuario pelo email
function buscarUsuarioByMail($email , $deletado=0){    
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT  id, id_perfil as perfil,matricula,nome,email,foto,descricao,senha,deletado
              FROM  usuario
             WHERE  email = '$email'
               AND  deletado = $deletado
             LIMIT  1";    
    
    $result = $con->query($sql);    
    $linha = mysql_fetch_assoc($result);    
    
    //Instancia objeto usuario
    $objto = new usuario();
    //Preenche os campos do objeto
    foreach($linha as $campo => $valor){
        $objto->$campo = $valor;
    }
    
    if (isset($objto)){
        //Retorna Array de Usuarios
        return $objto; 
    }else{
        return NULL;
    }          
}

//Buscar usuario pelo id
function autenticaUser($mail , $senha){    
    //Realizando conexão como BD
    $con = gerarCon();  
    $senha = md5($senha);
    $sql = "SELECT  id,id_perfil as perfil,matricula,nome,email,foto,descricao,senha,deletado,primeiroacesso
              FROM  usuario
             WHERE  email = '$mail'
               AND  senha = '$senha'
             LIMIT  1";        
    $result = $con->query($sql);
        
    if(mysql_num_rows($result)>0){
        $linha = mysql_fetch_assoc($result);
    
        //Instancia objeto usuario
        $objto = new usuario();
        //Preenche os campos do objeto
        foreach($linha as $campo => $valor){
            $objto->$campo = $valor;
        }
        
        return $objto; 
        
    }else{
        return NULL;
    }
        
    return NULL;
}


//Buscar todos usuarios
function buscarTodosUsuarios($deletado=0){
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT  id,id_perfil as perfil,matricula,nome,email,foto,descricao,senha,deletado
              FROM  usuario
             WHERE  deletado = $deletado";    
    $result = $con->query($sql);    
    
    while($linha = mysql_fetch_assoc($result)){
        //Instancia objeto usuario
        $objto = new usuario();
        //Preenche os campos do objeto
        foreach($linha as $campo => $valor){
            $objto->$campo = $valor;
        }
        $objtos[] = $objto;
    }
    
    if (isset($objtos)){
        //Retorna Array de Usuarios
        return $objtos; 
    }else{
        return NULL;
    }     
    
}

//Buscar todos os ALUNOS
function buscarTodosUsuariosAluno($deletado=0){
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT  id,id_perfil as perfil,matricula,nome,email,foto,descricao,senha,deletado
              FROM  usuario
             WHERE  deletado = $deletado
               AND  id_perfil = 3";
    $result = $con->query($sql);    
    
    while($linha = mysql_fetch_assoc($result)){
        //Instancia objeto usuario
        $objto = new usuario();
        //Preenche os campos do objeto
        foreach($linha as $campo => $valor){
            $objto->$campo = $valor;
        }
        $objtos[] = $objto;
    }
    
    if (isset($objtos)){
        //Retorna Array de Usuarios
        return $objtos; 
    }else{
        return NULL;
    }     
    
}

/*
 *  U P D A T E
 */
function atualizarUsuario($user){
    $con = gerarCon();
    
    $sql = "UPDATE  usuario
               SET  id_perfil = $user->perfil
                   ,matricula = $user->matricula
                   ,nome = $user->nome
                   ,email = $user->email
                   ,foto = $user->foto
                   ,descricao = $user->descricao
                   ,senha = $user->senha
                   ,deletado = $user->deletado
             WHERE  id = $user->id";    
    $result = $con->query($sql);
    
    return mysql_affected_rows($result);
}


function atualizaUserNome($idusuario,$novonome){
    $con = gerarCon();
    
    $sql = "UPDATE  usuario
               SET  nome = '$novonome' 
             WHERE  id = '$idusuario'";    
    $result = $con->query($sql);    
    return mysql_affected_rows($result);
}

function atualizaUserSenha($idusuario,$senhaatual,$senhanova){
    $con = gerarCon();
    
    $md5sa = md5($senhaatual);
    $md5sn = md5($senhanova);
    
    $sql = "UPDATE  usuario
               SET  senha = '$md5sn'                 
             WHERE  id = '$idusuario'
               AND  senha = '$md5sa'";    
    $result = $con->query($sql);
    
    return mysql_affected_rows();
}      
/*
 *  D E L E T E
 */
function deletarUsuario($id){
    $con = gerarCon();
    
    $sql = "UPDATE  usuario
               SET  deletado = 1
             WHERE  id = $id";    
    $result = $con->query($sql);
    
    return mysql_affected_rows($result);
}




