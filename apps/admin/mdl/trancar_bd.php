<?php
include_once "trancar.php";
require_once "$endatual/db/conection.php";

 
/*
 *  C R E A T E
 */
// Recebe: objeto $usuario (sem id)
function inserirTrancar($obj){    
    //Realizando conexão como BD
    $con = gerarCon();         
    $sql = "INSERT INTO `trancar` (`trancar`)
            VALUES ('$obj->trancar')";
    $con->query($sql);
    $id = $con->idGerado();
    return $id;
}

function inserirTrancarListExer($obj){    
    //Realizando conexão como BD
    $con = gerarCon();         
    $sql = "INSERT INTO `trancarlistexer` (`idtrancar`,`idlst`,`trancar`)
            VALUES ('$obj->idtrancar','$obj->idlst','$obj->trancar')";
    $con->query($sql);
    $id = $con->idGerado();
    return $id;
}

/*
 *  R E A D
 */

function buscarTrancar(){    
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT  id,trancar
              FROM  trancar";        
    $result = $con->query($sql);    
    $linha = mysql_fetch_assoc($result);    
    
    //Instancia objeto trancar
    $obj = new trancar();
    //Preenche os campos do objeto
    foreach($linha as $campo => $valor){
        $obj->$campo = $valor;
    }
    
    if (isset($obj)){
        //Retorna Array de Usuarios
        return $obj; 
    }else{
        return NULL;
    }          
}

function buscarTodosTrancarlistexer(){
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT  id,idtrancar,idlst,trancar
              FROM  trancarlistexer";
    $result = $con->query($sql);    
    
    while($linha = mysql_fetch_assoc($result)){
        //Instancia objeto listaDeExercicio
        $objt = new trancarlistexer();
        //Preenche os campos do objeto
        foreach($linha as $campo => $valor){
            $objt->$campo = $valor;
        }
        $objetos[] = $objt;
    }
    
    if (isset($objetos)){
        //Retorna Array de Lista de Exercícios
        return $objetos; 
    }else{
        return NULL;
    }   
}

function estaTrancado(){
    //Realizando conexão como BD
    $trancar = buscarTrancar();   
    
    if ($trancar->trancar == '0'){
        return false; 
    }else{
        return true;
    }   
}
function estaTrancadoParaLista($idlist,$idperfil='3'){
    //Realizando conexão como BD
    $trancar = buscarTrancar(); 
    
    if ($idperfil != '3'){
        return false;
    }else{
        if ($trancar->trancar == '0'){
            return false; 
        }else{

            $lsts = buscarTodosTrancarlistexer();
            foreach ($lsts as $o => $l){
                if($idlist == $l->idlst){
                    return false;                
                }
            }
            return true;
        }   
    }
}

/*
 *  U P D A T E
 */
function atualizarTrancar($obj){
    $con = gerarCon();
    
    $sql = "UPDATE  trancar
               SET  trancar = '$obj->trancar'
             WHERE  id = '$obj->id'";
    $result = $con->query($sql);
    
    return mysql_affected_rows($result);
}

    
/*
 *  D E L E T E
 */
function deletarTrancarlistexer($id){
    $con = gerarCon();    
    $sql = "DELETE  trancarlistexer
             WHERE  id = $id";
    $result = $con->query($sql);    
    return mysql_affected_rows($result);
}
function deletarTodosTrancarlistexer($id){
    $con = gerarCon();    
    $sql = "DELETE FROM trancarlistexer";
    $result = $con->query($sql);    
    return mysql_affected_rows($result);
}

