<?php
include_once "turma.php";
require_once "$endatual/db/conection.php";

 
/*
 *  C R E A T E
 */
// Recebe: objeto $usuario (sem id)
function inserirTurma($objto){
    //Realizando conexão como BD
    $con = gerarCon();         
    $sql = "INSERT INTO `turma` (
                        `titulo` , `descricao`)
            VALUES ('$objto->titulo', '$objto->descricao')";    
    $con->query($sql);
    $id = $con->idGerado();
    return $id;
}

/*
 *  R E A D
 */

//Buscar usuario pelo id
function buscarTurmaId($id , $deletado=0){    
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT  id, titulo ,descricao
              FROM  turma
             WHERE  id = $id
               AND  deletado = $deletado
             LIMIT  1";    
    
    $result = $con->query($sql);    
    $linha = mysql_fetch_assoc($result);    
    
//Instancia objeto turma
    $objto = new turma();
    //Preenche os campos do objeto
    foreach($linha as $campo => $valor){
        $objto->$campo = $valor;
    }
    
    if (isset($objto)){
        //Retorna Array de Usuarios
        return $objto; 
    }else{
        return NULL;
    }          
}

//Buscar todas turmas
function buscarTodasTurmas($deletado=0){
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT  id, titulo ,descricao
              FROM  turma
             WHERE  deletado = $deletado";    
    $result = $con->query($sql);    
    
    while($linha = mysql_fetch_assoc($result)){
        //Instancia objeto usuario
        $objto = new turma();
        //Preenche os campos do objeto
        foreach($linha as $campo => $valor){
            $objto->$campo = $valor;
        }
        $objtos[] = $objto;
    }
    
    if (isset($objtos)){
        //Retorna Array de Usuarios
        return $objtos; 
    }else{
        return NULL;
    }     
    
}
/*
 *  U P D A T E
 */
function atualizarTurma($objto){
    $con = gerarCon();
    
    $sql = "UPDATE  turma
               SET  titulo = $objto->titulo
                   ,descricao = $objto->descricao
             WHERE  id = $objto->id";    
    $result = $con->query($sql);
    
    return mysql_affected_rows($result);
}


/*
 *  D E L E T E
 */
function deletarTurma($id){
    $con = gerarCon();
    
    $sql = "UPDATE  turma
               SET  deletado = 1
             WHERE  id = $id";    
    $result = $con->query($sql);
    
    return mysql_affected_rows($result);
}

