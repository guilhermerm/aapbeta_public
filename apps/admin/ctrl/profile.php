<?php

//Exibir o Perfil do Usuário
//Inserindo o conteúdo central da página
$tpl->addFile("CONTEUDO", "./apps/admin/tpl/profile.html");

if (isset($_GET['uid'])) {
    $iduser = $_GET['uid'];
    $objusuario = buscarUsuarioId($iduser);

    $tpl->USUARIOID = $objusuario->id;
    $tpl->USUARIOPERFIL = $objusuario->perfil;
    $tpl->USUARIOMATRICULA = $objusuario->matricula;
    $tpl->USUARIONOME = $objusuario->nome;
    $tpl->USUARIOEMAIL = $objusuario->email;
    $tpl->USUARIOFOTO = ($objusuario->foto == "") ? "data/admin/usuariopadrao.png" : "data/admin/imgusuario/" . $objusuario->foto;
    $tpl->USUARIODESCRICAO = $objusuario->descricao;

    //$tpl->LINK_USUARIOID = "$endamb/?page=exercicio&id=".$b->id;
    //$tpl->block("BLOCK_USUARIO");                
}   