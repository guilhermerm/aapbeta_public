<?php

//Inserindo o conteúdo central da página
$tpl->addFile("CONTEUDO", "./apps/admin/tpl/configtranca.html");


if (isset($_POST['formconfigtranca'])) {
    
    //Salvar Trancar
    
    $obj= new trancar();
    $obj->id      = '1';
    $obj->trancar =  $_POST['trancar'];
    
    atualizarTrancar($obj);
    
    
    $trancarlst = new trancarlistexer();
    $trancarlst->id      = '1';
    $trancarlst->trancar = '0';
    
    deletarTodosTrancarlistexer('1');
    foreach ($_POST as $a => $b){        
        if(substr($a, 0,5) == "idlis"){
            $trancarlst->idlst = $b;
            inserirTrancarListExer($trancarlst);
        }
    }    
    header("Location:  $endamb/?page=inicio");
    exit();
}

$objtrancar = buscarTrancar();
$tpl->T0CK = $objtrancar->trancar =="0" ? "checked='checked'":"";
$tpl->T1CK = $objtrancar->trancar =="1" ? "checked='checked'":"";

//Admin ou professor
$objlistas = buscarTodasListas();        
$objts     = buscarTodosTrancarlistexer();

foreach($objts as $o => $v){
    $lsts[] = $v->idlst;
}

if($objlistas != NULL){
    foreach($objlistas as $a => $b){            
        //TODO
        $tpl->EXERID     = $b->id;
        $tpl->EXERTITULO = ($b->oculto!='2')?$b->titulo:"<i class='fa fa-eye-slash'></i> ".$b->titulo;        
        
        //Verificar se deve marcar o checkbox
        $marcar = false;
        foreach ($lsts as  $i=>$v){
            if($b->id == $v){
                $marcar = true;
                break;
            }
            
        }                
        $tpl->EXERCHECKED = ($marcar)?"checked='checked'":"";

        $tpl->block("BLOCK_LISTAS");
    }
}  


$tpl->LINK_FORMACTION = "?page=$page";
$tpl->LINK_CANCELAR = "?page=inicio";

