<?php

//Lista de todos os Usuarios
//Inserindo o conteúdo central da página
$tpl->addFile("CONTEUDO", "./apps/admin/tpl/listusuarios.html");

if (isset($_GET['salvo'])) {
    $tpl->block("BLOCK_MSGSALVO");
}

$objusuarios = buscarTodosUsuarios();

if ($objusuarios != NULL) {
    foreach ($objusuarios as $a => $b) {
        if ($b->perfil != "1") {
            $tpl->USUARIOID = $b->id;
            $tpl->USUARIOPERFIL = $b->perfil;
            //$tpl->USUARIOMATRICULA = $b->matricula;
            $tpl->USUARIONOME = $b->nome;
            $tpl->USUARIOEMAIL = $b->email;
            $tpl->USUARIOFOTO = ($b->foto == "") ? "data/admin/usuariopadrao.png" : $b->foto;
            //$tpl->USUARIODESCRICAO = $b->descricao;                    
            //$tpl->LINK_USUARIOID = "$endamb/?page=exercicio&id=".$b->id;
            $tpl->LINK_USUARIOID = "?page=relatoriouserlistas&id=$b->id";
            $tpl->block("BLOCK_USUARIO");
        }
    }
}            
            