<?php

//Formulário para Cadastrar um Novo Usuário
//Inserindo o conteúdo central da página
$tpl->addFile("CONTEUDO", "./apps/admin/tpl/configusuario.html");

if (isset($_POST['formconfiguser'])) {
    //Salvar Perfil

    $novonome = $_POST['nome'];

    $res1 = atualizaUserNome($userlogado->id, $novonome);
    $userlogado->nome = $novonome;

    header("Location:  $endamb/?page=inicio&useralterado");
    exit();
} else if (isset($_POST['formconfiguseralterar'])) {
    //Alterar Senha
    $senhaatual = $_POST['senhaatual'];
    $senhanova = $_POST['senhanova'];

    if ($senhaatual != "" && $senhanova != "") {
        $res2 = atualizaUserSenha($userlogado->id, $senhaatual, $senhanova);

        if ($res2) {
            header("Location: $endamb/sair.php?email=$userlogado->email");
            exit();
        } else {
            $tpl->block("BLOCK_ERROALTERARSENHA");
        }
    }
}

$tpl->USERNOME = $userlogado->nome;
$tpl->USEREMAIL = $userlogado->email;
$tpl->USERMATRICULA = $userlogado->matricula;

$tpl->LINK_FORMACTION = "?page=$page";
$tpl->LINK_CANCELAR = "?page=inicio";
