<?php
include_once "$endatual/apps/cspl/mdl/comentario.php";
require_once "$endatual/db/conection.php";

 
/*
 *  C R E A T E
 */
// Recebe: objeto $comentario (sem id)
function inserirComentario($comentario){
    //Realizando conexão como BD
    $con = gerarCon();
    $sql = "INSERT INTO `comentario` (`iduser`,`idsolucao`,`texto`,`deletado`)
            VALUES ('$comentario->iduser','$comentario->idsolucao','$comentario->texto','$comentario->deletado')";
    $con->query($sql);
    $id = $con->idGerado();
    return $id;
}


/*
 *  R E A D
 */

//Buscar comentario pelo id
function buscarComentarioId($id , $deletado=0){    
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT  id,iduser,idsolucao,texto,datacriacao,deletado
              FROM  comentario
             WHERE  id = '$id'
               AND  deletado = '$deletado'
             LIMIT  1";    
    $result = $con->query($sql);    
    $linha = mysql_fetch_assoc($result);    
    
    //Instancia objeto comentario
    $obj = new comentario();
    //Preenche os campos do objeto
    foreach($linha as $campo => $valor){
        $obj->$campo = $valor;
    }
    
    if (isset($obj)){
        //Retorna Array de Exercicios
        return $obj; 
    }else{
        return NULL;
    }     
                 
}

//Buscar COMENTARIO dado o ID da Solucao
function buscarComentariosByIDSolu($idsolucao , $deletado=0){    
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT   c.id
                    ,c.iduser as iduser
                    ,c.idsolucao as idsolucao
                    ,c.texto as texto
                    ,c.datacriacao as datacriacao
                    ,c.deletado as deletado
                    ,u.nome as usernome
              FROM  comentario as c
              JOIN  usuario as u ON c.iduser = u.id
             WHERE  c.idsolucao = '$idsolucao'
               AND  c.deletado = '$deletado'";    
    $result = $con->query($sql);        
    
    while($linha = mysql_fetch_assoc($result)){
        //Instancia objeto comentario
        $obj = new comentario();
        //Preenche os campos do objeto
        foreach($linha as $campo => $valor){
            $obj->$campo = $valor;
        }
        $objs[] = $obj;
    }
    
    if (isset($objs)){
        //Retorna Array de Exercicios
        return $objs; 
    }else{
        return NULL;
    }   
                 
}


/*
 *  U P D A T E
 */
function atualizarComentario($comentario){
    $con = gerarCon();
    
    $sql = "UPDATE  comentario
               SET  texto = '$comentario->texto'
                   ,deletado = '$comentario->deletado'
             WHERE  id = '$comentario->id'";    
    $result = $con->query($sql);
    return mysql_affected_rows($result);
}


/*
 *  D E L E T E
 */
function deletarComentario($id){
    $con = gerarCon();
    
    $sql = "UPDATE  comentario
               SET  deletado = '1'
             WHERE  id = '$id'";    
    $result = $con->query($sql);
    
    return mysql_affected_rows($result);
}

