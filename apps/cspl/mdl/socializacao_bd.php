<?php
include_once "socializacao.php";
require_once "$endatual/db/conection.php";

/*
 *  S O C I A L I Z A Ç Ã O
 *  C R E A T E
 */
// Recebe: objeto $socializacao (sem id)
function inserirSocializacao($obj){    
    //Realizando conexão como BD
    $con = gerarCon();         
    $sql = "INSERT INTO `socializacao` (
               `ide`,               
               `idlst`,               
               `socializar`,               
               `para`
           )VALUES(
               '$obj->ide',
               '$obj->idlst',
               '$obj->socializar',
               '$obj->para'
            )";
    $con->query($sql);    
    return $con->idGerado();
}

/*
 *  S O C I A L I Z A Ç Ã O
 *  R E A D
 */

//Buscar socialização pelo id
function buscarSocializacaoId($id){    
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT  id,ide,idlst,socializar,para
              FROM  socializacao
             WHERE  id = $id
             LIMIT  1";
    $result = $con->query($sql);    
    $linha = mysql_fetch_assoc($result);
    
    //Instancia objeto socialização
    $obj = new socializacao();
    //Preenche os campos do objeto
    foreach($linha as $campo => $valor){
        $obj->$campo = $valor;
    }
    
    if (isset($obj)){
        //Retorna Array de Socializacaos
        return $obj; 
    }else{
        return NULL;
    }                 
}

//Buscar socialização pelo id
function buscarSocializacaoByIdExer($id){    
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT  id,ide,idlst,socializar,para
              FROM  socializacao
             WHERE  ide = $id
             LIMIT  1";
    $result = $con->query($sql);    
    $linha = mysql_fetch_assoc($result);
    
    //Instancia objeto socialização
    $obj = new socializacao();
    //Preenche os campos do objeto
    foreach($linha as $campo => $valor){
        $obj->$campo = $valor;
    }
    
    if (isset($obj)){
        //Retorna Array de Socializacaos
        return $obj; 
    }else{
        return NULL;
    }                 
}

/**
 * Verifica se uma socialização existe pelo IDE e IDLST
 * @param Socializacao $socializacao
 * @return BOOLEAN TRUE or FALSE
 */
function existeSocializacaoNoBanco($obj){
    
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT  id,ide,idlst,socializar,para
              FROM  socializacao
             WHERE  ide   = $obj->ide 
               AND  idlst = $obj->idlst
             LIMIT  1";
    $result = $con->query($sql);    
            
    if ($linha = mysql_fetch_assoc($result)){
        //Retorna Array de Socializacaos
        return TRUE; 
    }else{
        return FALSE;
    }       
    
}

//Buscar todos socializações
function buscarTodasSocializacao(){
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT  id,ide,idlst,socializar,para
              FROM  socializacao";    
    $result = $con->query($sql);    
    
    while($linha = mysql_fetch_assoc($result)){
        //Instancia objeto socializacao
        $obj = new socializacao();
        //Preenche os campos do objeto
        foreach($linha as $campo => $valor){
            $obj->$campo = $valor;
        }
        $objs[] = $obj;
    }
    
    if (isset($objs)){
        //Retorna Array de Socializações
        return $objs; 
    }else{
        return NULL;
    }        
}

//Buscar todos socializações
function buscarTodasSocializacaoByIdLista($idlst){
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT  id,ide,idlst,socializar,para
              FROM  socializacao              
             WHERE  idlst = $idlst";    
    $result = $con->query($sql);    
    
    while($linha = mysql_fetch_assoc($result)){
        //Instancia objeto socializacao
        $obj = new socializacao();
        //Preenche os campos do objeto
        foreach($linha as $campo => $valor){
            $obj->$campo = $valor;
        }
        $objs[] = $obj;
    }
    
    if (isset($objs)){
        //Retorna Array de Socializações
        return $objs; 
    }else{
        return NULL;
    }        
}

//Buscar todos socializações
function buscarSocializacaoByExercicioID($ide){
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT  id,ide,idlst,socializar,para
              FROM  socializacao              
             WHERE  ide = $ide";
    $result = $con->query($sql);    
    
    $linha = mysql_fetch_assoc($result);
    //Instancia objeto socializacao
    
    if (isset($linha)){
        $obj = new socializacao();
        //Preenche os campos do objeto
        foreach($linha as $campo => $valor){
            $obj->$campo = $valor;
        }
        //Retorna Array de Socializações
        return $obj; 
    }else{
        return NULL;
    }        
}




/*
 *  S O C I A L I Z A Ç Ã O
 *  U P D A T E
 */

/**
 * Atualiza as informações de uma Socializacao
 * @param Socializacao $socializacao
 * @return mysql_affecter_rows()
 */
function atualizarSocializacao($obj){
    $con = gerarCon();
    
    $sql = "UPDATE  socializacao
               SET  socializar = '$obj->socializar'
                    ,para = '$obj->para'
             WHERE  ide = '$obj->ide' 
               AND  idlst = '$obj->idlst'";

        //"WHERE  id = $obj->id";
    $result = $con->query($sql);
    
    return mysql_affected_rows();
}

/*
 *  S O C I A L I Z A Ç Ã O
 *  D E L E T E
 */

function apagarSocializacoes($idlst){
    $con = gerarCon();         
    $sql = "DELETE FROM `socializacao` 
                  WHERE `idlst` = '$idlst'";
    $con->query($sql);
    return mysql_affected_rows();
}