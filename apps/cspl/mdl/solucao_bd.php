<?php
include_once "$endatual/apps/cspl/mdl/solucao.php";
require_once "$endatual/db/conection.php";

 
/*
 *  C R E A T E 
 *  (inserts)
 */
// Recebe: objeto $solucao (sem id)
function inserirSolucao($obj){    
    //Realizando conexão como BD
    $con = gerarCon();         
    $sql = "INSERT INTO `solucao` (
                `idexercicio`,
                `iduser`,
                `compreensao`, 
                `solucao`,
                `codigohaskell`,
                `funcaoprincipal`,
                `socializar`,
                `estado`,
                `deletado`
          )VALUES (
          '$obj->idexercicio', 
          '$obj->iduser', 
          '$obj->compreensao', 
          '$obj->solucao', 
          '$obj->codigohaskell',
          '$obj->funcaoprincipal',
          '$obj->socializar',
          '$obj->estado',
          '$obj->deletado')";
    $con->query($sql);    
    return $con->idGerado();
}

/*
 *  R E A D
 */

//Buscar solucao pelo id
function buscarSolucaoId($id , $deletado=0){    
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT  id,idexercicio,iduser,compreensao,solucao,codigohaskell,datacriacao,funcaoprincipal,socializar,estado,deletado
              FROM  solucao
             WHERE  id = $id
               AND  deletado = $deletado
             LIMIT  1";
    $result = $con->query($sql);    
    $linha = mysql_fetch_assoc($result);    
    
    //Instancia objeto solucao
    $solucao = new solucao();
    //Preenche os campos do objeto
    foreach($linha as $campo => $valor){
        $solucao->$campo = $valor;
    }
    
    if (isset($solucao)){
        //Retorna Array de Exercicios
        return $solucao; 
    }else{
        return NULL;
    }     
                 
}

//Buscar todas soluções
function buscarTodasSolucoes($deletado=0){
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT  id,idexercicio,iduser,compreensao,solucao,codigohaskell,datacriacao,funcaoprincipal,socializar,estado,deletado
              FROM  solucao
             WHERE  deletado = $deletado";    
    $result = $con->query($sql);    
    
    while($linha = mysql_fetch_assoc($result)){
        //Instancia objeto solucao
        $obj = new solucao();
        //Preenche os campos do objeto
        foreach($linha as $campo => $valor){
            $obj->$campo = $valor;
        }
        $objs[] = $obj;
    }
    
    if (isset($objs)){
        //Retorna Array de Exercicios
        return $objs; 
    }else{
        return NULL;
    }     
    
}

function buscarTodasSolucoesByExercicioID($idexercicio,$estado="completo",$deletado=0){
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT   s.id as id
                    ,s.idexercicio as idexercicio
                    ,s.compreensao as compreensao
                    ,s.solucao as solucao
                    ,s.codigohaskell as codigohaskell
                    ,s.datacriacao as datacriacao
                    ,s.funcaoprincipal as funcaoprincipal
                    ,s.socializar as socializar
                    ,s.estado as estado
                    ,s.deletado as deletado
                    ,s.iduser as iduser
                    ,u.nome as usernome
              FROM  solucao as s
              JOIN  usuario as u ON s.iduser = u.id
             WHERE  s.idexercicio = '$idexercicio'
               AND  s.deletado = '$deletado'
               AND  s.estado = '$estado'  
          ORDER BY  u.nome ASC, s.datacriacao DESC
            ";
    $result = $con->query($sql);    
    
    while($linha = mysql_fetch_assoc($result)){
        //Instancia objeto solucao
        $obj = new solucao();
        //Preenche os campos do objeto
        foreach($linha as $campo => $valor){
            $obj->$campo = $valor;
        }
        $objs[] = $obj;
    }
    
    if (isset($objs)){
        //Retorna Array de Exercicios
        return $objs; 
    }else{
        return NULL;
    }     
    
}

function contaTodasSolucoesByExercicioID($idexercicio,$estado="completo",$deletado=0){
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT  COUNT(*) as qtd
              FROM  solucao as s
              JOIN  usuario as u ON s.iduser = u.id
             WHERE  s.idexercicio = '$idexercicio'
               AND  s.deletado = '$deletado'
               AND  s.estado = '$estado'  
          ORDER BY  u.nome ASC, s.datacriacao DESC
            ";
    $result = $con->query($sql);    
    
    $linha = mysql_fetch_assoc($result);
    
    if ($linha){        
        return $linha['qtd']; 
    }else{
        return 0;
    }     
    
}
function contaUsersSolucoesByExercicioID($idexercicio,$estado="completo",$deletado=0){
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT  COUNT( DISTINCT u.id) as qtd
              FROM  solucao as s
              JOIN  usuario as u ON s.iduser = u.id
             WHERE  s.idexercicio = '$idexercicio'
               AND  s.deletado = '$deletado'
               AND  s.estado = '$estado'  
            ";
    $result = $con->query($sql);    
    
    $linha = mysql_fetch_assoc($result);
    
    if ($linha){        
        return $linha['qtd']; 
    }else{
        return 0;
    }     
    
}

function contaUserSolucoesByExercicioID($idexercicio,$iduser,$estado="completo",$deletado=0){
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT  COUNT(*) as qtd
              FROM  solucao as s
              JOIN  usuario as u ON s.iduser = u.id
             WHERE  s.idexercicio = '$idexercicio'
               AND  u.id = '$iduser'
               AND  s.deletado = '$deletado'
               AND  s.estado = '$estado'
          GROUP BY  u.id
            ";
    $result = $con->query($sql);    
    
    $linha = mysql_fetch_assoc($result);
    
    if ($linha){        
        return $linha['qtd']; 
    }else{
        return 0;
    }     
    
}



function buscarTodasSolucoesSocializadasByExercicioID($idexercicio,$estado="completo",$deletado=0){
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT   s.id as id
                    ,s.idexercicio as idexercicio
                    ,s.compreensao as compreensao
                    ,s.solucao as solucao
                    ,s.codigohaskell as codigohaskell
                    ,s.datacriacao as datacriacao
                    ,s.funcaoprincipal as funcaoprincipal
                    ,s.socializar as socializar
                    ,s.estado as estado
                    ,s.deletado as deletado
                    ,s.iduser as iduser
                    ,u.nome as usernome
              FROM  solucao as s
              JOIN  usuario as u ON s.iduser = u.id
             WHERE  s.idexercicio = '$idexercicio'
               AND  s.socializar = '1'
               AND  s.deletado = '$deletado'
               AND  s.estado = '$estado'
          ORDER BY  u.nome ASC, s.datacriacao DESC
            ";
    $result = $con->query($sql);    
    
    while($linha = mysql_fetch_assoc($result)){
        //Instancia objeto solucao
        $obj = new solucao();
        //Preenche os campos do objeto
        foreach($linha as $campo => $valor){
            $obj->$campo = $valor;
        }
        $objs[] = $obj;
    }
    
    if (isset($objs)){
        //Retorna Array de Exercicios
        return $objs; 
    }else{
        return NULL;
    }     
    
}

/**
 * buscarTodasSolucoesUserByExercicioID($idexercicio,$iduser,$estado="completo",$deletado=0)
 *  
 * return $objs : Vetor de obj solucao
 */
function buscarTodasSolucoesUserByExercicioID($idexercicio,$iduser,$estado="completo",$deletado=0){
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT   s.id as id
                    ,s.idexercicio as idexercicio
                    ,s.compreensao as compreensao
                    ,s.solucao as solucao
                    ,s.codigohaskell as codigohaskell
                    ,s.datacriacao as datacriacao
                    ,s.funcaoprincipal as funcaoprincipal
                    ,s.socializar as socializar
                    ,s.estado as estado
                    ,s.deletado as deletado
                    ,s.iduser as iduser
                    ,u.nome as usernome
              FROM  solucao as s
              JOIN  usuario as u ON s.iduser = u.id
             WHERE  s.idexercicio = '$idexercicio'
               AND  s.iduser = '$iduser'
               AND  s.deletado = '$deletado'
          ORDER BY  s.datacriacao DESC";
               //AND  s.estado = '$estado';
    $result = $con->query($sql);    
    
    while($linha = mysql_fetch_assoc($result)){
        //Instancia objeto solucao
        $obj = new solucao();
        //Preenche os campos do objeto
        foreach($linha as $campo => $valor){
            $obj->$campo = $valor;
        }
        $objs[] = $obj;
    }
    
    if (isset($objs)){
        //Retorna Array de Exercicios
        return $objs; 
    }else{
        return NULL;
    }     
    
}
/*
 *  U P D A T E
 */
function atualizarSolucao($obj){
    $con = gerarCon();
    
    $sql = "UPDATE  solucao
               SET  compreensao = $obj->compreensao
                    ,solucao = $obj->solucao
                    ,codigohaskell = $obj->codigohaskell
                    ,funcaoprincipal = $obj->funcaoprincipal
             WHERE  id = $obj->id";
    $result = $con->query($sql);
    
    return mysql_affected_rows($result);
}

function socializarSolucao($idsolucao, $valor = 1){
    $con = gerarCon();
    
    $sql = "UPDATE  solucao
               SET  socializar = '$valor'
             WHERE  id = $idsolucao";
    $result = $con->query($sql);
    
    return mysql_affected_rows();
}


/*
 *  D E L E T E
 */
function deletarSolucao($id){
    $con = gerarCon();
    
    $sql = "UPDATE  solucao
               SET  deletado = 1
             WHERE  id = $id";    
    $result = $con->query($sql);
    return mysql_affected_rows();

}