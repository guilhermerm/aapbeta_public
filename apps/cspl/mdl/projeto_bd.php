<?php
include_once "projeto.php";
require_once "$endatual/db/conection.php";

 
/*
 *  E X E R C I C I O
 *  C R E A T E
 */
// Recebe: objeto $projeto (sem id)
function inserirProjeto($obj){    
    //Realizando conexão como BD
    $con = gerarCon();         
    $sql = "INSERT INTO `projeto` (
               `titulo`,
               `enunciado`,               
               `deletado`
           )VALUES(
               '$obj->titulo', 
               '$obj->enunciado',
               '$obj->deletado'
            )";
    $con->query($sql);    
    return $con->idGerado();
}

/*
 *  E X E R C I C I O
 *  R E A D
 */

//Buscar projeto pelo id
function buscarProjetoId($id , $deletado=0){    
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT  id,titulo,enunciado,deletado
              FROM  projeto
             WHERE  id = $id
               AND  deletado = $deletado
             LIMIT  1";
    $result = $con->query($sql);    
    $linha = mysql_fetch_assoc($result);    
    
    //Instancia objeto projeto
    $obj = new projeto();
    //Preenche os campos do objeto
    foreach($linha as $campo => $valor){
        $obj->$campo = $valor;
    }
    
    if (isset($obj)){
        //Retorna Array de Projetos
        return $obj; 
    }else{
        return NULL;
    }     
                 
}

//Buscar todos projetos
function buscarTodosProjetos($deletado=0){
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT  id,titulo,enunciado,deletado
              FROM  projeto
             WHERE  deletado = $deletado";    
    $result = $con->query($sql);    
    
    while($linha = mysql_fetch_assoc($result)){
        //Instancia objeto projeto
        $obj = new projeto();
        //Preenche os campos do objeto
        foreach($linha as $campo => $valor){
            $obj->$campo = $valor;
        }
        $objs[] = $obj;
    }
    
    if (isset($objs)){
        //Retorna Array de Projetos
        return $objs; 
    }else{
        return NULL;
    }        
}


/*
 *  P R O J E T O
 *  U P D A T E
 */

/**
 * Atualiza as informações de um Projeto
 * @param Projeto $objeto
 * @return mysql_affecter_rows()
 */
function atualizarProjeto($obj){
    $con = gerarCon();
    
    $sql = "UPDATE  projeto
               SET  titulo = '$obj->titulo'
                   ,enunciado = '$obj->enunciado'
                   ,deletado = '$obj->deletado'
             WHERE  id = $obj->id";    
    $result = $con->query($sql);
    
    return mysql_affected_rows($result);
}

/*
 *  E X E R C I C I O
 *  D E L E T E
 */
function deletarProjeto($id){
    $con = gerarCon();
    
    $sql = "UPDATE  projeto
               SET  deletado = 1
             WHERE  id = $id";    
    $result = $con->query($sql);
    
    return mysql_affected_rows($result);
}