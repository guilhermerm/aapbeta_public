<?php
include_once "exercicio.php";
require_once "$endatual/db/conection.php";

 
/*
 *  E X E R C I C I O
 *  C R E A T E
 */
// Recebe: objeto $exercicio (sem id)
function inserirExercicio($obj){    
    //Realizando conexão como BD
    $con = gerarCon();         
    $sql = "INSERT INTO `exercicio` (
               `titulo`,
               `enunciado`,
               `interface`,
               `pentrada`,
               `mostraplano`,
               `tiposolucao`,
               `deletado`
           )VALUES(
               '$obj->titulo', 
               '$obj->enunciado', 
               '$obj->interface', 
               '$obj->validarteste', 
               '$obj->pentrada', 
               '$obj->mostraplano', 
               '$obj->tiposolucao',  
               '$obj->deletado'
            )";
    $con->query($sql);    
    return $con->idGerado();
}

/*
 *  E X E R C I C I O
 *  R E A D
 */

//Buscar exercicio pelo id
function buscarExercicioId($id , $deletado=0){    
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT  id,titulo,enunciado,interface,validarteste,pentrada,mostraplano,tiposolucao,deletado
              FROM  exercicio
             WHERE  id = $id
               AND  deletado = $deletado
             LIMIT  1";
    $result = $con->query($sql);    
    $linha = mysql_fetch_assoc($result);    
    
    //Instancia objeto exercicio
    $obj = new exercicio();
    //Preenche os campos do objeto
    foreach($linha as $campo => $valor){
        $obj->$campo = $valor;
    }
    
    if (isset($obj)){
        //Retorna Array de Exercicios
        return $obj; 
    }else{
        return NULL;
    }     
                 
}

//Buscar padrão de teste do exercicio pelo id
function buscarValidarTesteByExercicioId($id , $deletado=0){    
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT  pentrada, validarteste
              FROM  exercicio
             WHERE  id = $id
               AND  deletado = $deletado
             LIMIT  1";
    $result = $con->query($sql);    
    $linha = mysql_fetch_assoc($result);    
    
    //Instancia objeto exercicio
    $obj = new exercicio();
    //Preenche os campos do objeto
    foreach($linha as $campo => $valor){
        $obj->$campo = $valor;
    }
    
    if (isset($obj)){
        //Retorna Array de Exercicios
        return $obj; 
    }else{
        return NULL;
    }     
                 
}

//Buscar todos exercicios
function buscarTodosExercicios($deletado=0){
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT  id,titulo,enunciado,interface,validarteste,pentrada,mostraplano,tiposolucao,datacriacao,deletado
              FROM  exercicio
             WHERE  deletado = $deletado
          ORDER BY  datacriacao DESC";    
    $result = $con->query($sql);    
    
    while($linha = mysql_fetch_assoc($result)){
        //Instancia objeto exercicio
        $exercicio = new exercicio();
        //Preenche os campos do objeto
        foreach($linha as $campo => $valor){
            $exercicio->$campo = $valor;
        }
        $exercicios[] = $exercicio;
    }
    
    if (isset($exercicios)){
        //Retorna Array de Exercicios
        return $exercicios; 
    }else{
        return NULL;
    }        
}


/**
 * Função para Buscar Todos os Exercícios de uma Lista de Exercícios
 * @param String $idlista
 * @return array Exercicio 
 */
function buscarTodosExerciciosByIdLista($idlista,$deletado=0){
    //Realizando conexão como BD
    $con = gerarCon();
    $sql = "SELECT   e.id as id
                    ,e.titulo as titulo
                    ,e.enunciado as enunciado
                    ,e.interface as interface
                    ,e.pentrada as pentrada
                    ,e.validarteste as validarteste
                    ,e.mostraplano as mostraplano
                    ,e.tiposolucao as tiposolucao
                    ,e.deletado as deletado
              FROM  listadeexercicio as l
              JOIN  lista_exercicio as le
                ON  l.id = le.idlst
              JOIN  exercicio as e
                ON  e.id = le.ide
             WHERE  e.deletado = $deletado
               AND  le.idlst = $idlista";
    $result = $con->query($sql);    
    
    while($linha = mysql_fetch_assoc($result)){
        //Instancia objeto exercicio
        $obj = new exercicio();
        //Preenche os campos do objeto
        foreach($linha as $campo => $valor){
            $obj->$campo = $valor;
        }
        $objtos[] = $obj;
    }
    
    if (isset($objtos)){
        //Retorna Array de Exercicios
        return $objtos; 
    }else{
        return NULL;
    }     
    
}

/**
 * Função para Buscar os IDs de Todos os Exercícios de uma Lista de Exercícios
 * @param String $idlista
 * @return array IDs Exercícios 
 */
function buscarTodosIDExerciciosByIdLista($idlista,$deletado=0){
    //Realizando conexão como BD
    $con = gerarCon();
    $sql = "SELECT  e.id as id
              FROM  listadeexercicio as l
              JOIN  lista_exercicio as le
                ON  l.id = le.idlst
              JOIN  exercicio as e
                ON  e.id = le.ide
             WHERE  e.deletado = $deletado
               AND  le.idlst = $idlista";
    $result = $con->query($sql);    
    
    while($linha = mysql_fetch_assoc($result)){
        //Preenche os campos do objeto
        foreach($linha as $campo => $valor){            
            $objtos[] = $valor;
        }
    }
    
    if (isset($objtos)){
        //Retorna Array de Exercicios
        return $objtos; 
    }else{
        return NULL;
    }     
    
}

/*
 *  E X E R C I C I O
 *  U P D A T E
 */

/**
 * Atualiza as informações de um Exercício
 * @param Exercicio $objeto
 * @return mysql_affecter_rows()
 */
function atualizarExercicio($obj){
    $con = gerarCon();
    
    $sql = "UPDATE  exercicio
               SET  titulo = '$obj->titulo'
                   ,enunciado = '$obj->enunciado'
                   ,interface = '$obj->interface'
                   ,validarteste = '$obj->validarteste'
                   ,pentrada = '$obj->pentrada'
                   ,mostraplano = '$obj->mostraplano'
                   ,tiposolucao = '$obj->tiposolucao'
                   ,deletado = '$obj->deletado'
             WHERE  id = $obj->id";    
    $result = $con->query($sql);
    
    return mysql_affected_rows($result);
}

/*
 *  E X E R C I C I O
 *  D E L E T E
 */
function deletarExercicio($id){
    $con = gerarCon();
    
    $sql = "UPDATE  exercicio
               SET  deletado = 1
             WHERE  id = $id";    
    $result = $con->query($sql);
    
    return mysql_affected_rows($result);
}


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                 L I S T A S   D E   E X E R C Í C I O S                     *
 *                                                                             *
 *  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
 
/*
 *  L I S T A  D E  E X E R
 *  C R E A T E
 */
function inserirListaDeExercicio($obj){
    //Realizando conexão como BD
    $con = gerarCon();         
    $sql = "INSERT INTO `listadeexercicio` (
                 `titulo`, 
                 `texto`, 
                 `texto2`, 
                 `prazoenvio`,
                 `permitirenvio`, 
                 `usarconstrutor`,
                 `socializar`,
                 `para`,
                 `datasocializacao`,
                 `datapublicacao`,
                 `datacriacao`,
                 `oculto`,
                 `deletado`
            )VALUES(
             '$obj->titulo',
             '$obj->texto',
             '$obj->texto2',
             '$obj->prazoenvio',
             '$obj->permitirenvio',
             '$obj->usarconstrutor',
             '$obj->socializar',
             '$obj->para',
             '$obj->datasocializacao',
             '$obj->datapublicacao',
             '$obj->datacriacao',
             '$obj->oculto',
             '$obj->deletado'
             )";
    $con->query($sql);    
    return $con->idGerado();
}

function relacionarListaExercicio($ide,$idlst){
    $con = gerarCon();         
    $sql = "INSERT INTO `lista_exercicio` (
                `ide`, 
                `idlst`
           )VALUES(
                '$ide',
                '$idlst'
           )";
    $con->query($sql);     
}


/*
 *  L I S T A  D E  E X E R
 *  R E A D
 */

function buscarListaDeExercicio($idlista,$deletado=0){
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT  id,titulo,texto,texto2,prazoenvio,permitirenvio,usarconstrutor,socializar,para,datasocializacao,datapublicacao,datacriacao,oculto
              FROM  listadeexercicio
             WHERE  id = $idlista
               AND  deletado = $deletado";
    $result = $con->query($sql);        
    $linha = mysql_fetch_assoc($result);    
    
    //Instancia objeto Lista de Exercicio
    $obj = new listadeexercicio();
    //Preenche os campos do objeto
    foreach($linha as $campo => $valor){
        $obj->$campo = $valor;
    }
    
    if (isset($obj)){
        //Retorna Array de Exercicios
        return $obj; 
    }else{
        return NULL;
    }   
}

function buscarTodasListas($woculto="",$deletado=0){
    //Realizando conexão como BD
    if($woculto!=""){
        $woculto = " AND oculto = $woculto ";
    }
    $con = gerarCon();      
    $sql = "SELECT  id,titulo,texto,texto2,prazoenvio,permitirenvio,usarconstrutor,socializar,para,datasocializacao,datapublicacao,datacriacao,oculto
              FROM  listadeexercicio
             WHERE  deletado = $deletado
                    $woculto ";
    $result = $con->query($sql);    
    
    while($linha = mysql_fetch_assoc($result)){
        //Instancia objeto listaDeExercicio
        $objt = new listadeexercicio();
        //Preenche os campos do objeto
        foreach($linha as $campo => $valor){
            $objt->$campo = $valor;
        }
        $objetos[] = $objt;
    }
    
    if (isset($objetos)){
        //Retorna Array de Lista de Exercícios
        return $objetos; 
    }else{
        return NULL;
    }   
}

/*
 *  L I S T A  D E  E X E R
 *  U P D A T E
 */

function atualizarListaDeExercicio($obj){
    $con = gerarCon();
    
    $sql = "UPDATE  listadeexercicio
               SET  titulo = '$obj->titulo'
                   ,texto = '$obj->texto'
                   ,texto2 = '$obj->texto2'
                   ,prazoenvio = '$obj->prazoenvio'
                   ,permitirenvio = '$obj->permitirenvio'
                   ,usarconstrutor = '$obj->usarconstrutor'
                   ,socializar = '$obj->socializar'
                   ,para = '$obj->para'
                   ,datasocializacao = '$obj->datasocializacao'
                   ,datapublicacao = '$obj->datapublicacao'
                   ,oculto = '$obj->oculto'
                   ,deletado = '$obj->deletado'
             WHERE  id = $obj->id";    
    $result = $con->query($sql);
    
    return mysql_affected_rows();
}     


/*
 *  L I S T A  D E  E X E R
 *  D E L E T E
 */

function deletarListaDeExercicio($id){
    $con = gerarCon();
    
    $sql = "UPDATE  listadeexercicio
               SET  deletado = 1
             WHERE  id = $id";    
    $result = $con->query($sql);
    
    return mysql_affected_rows();
}

function apagarRelacoesListaExercicio($idlst){
    $con = gerarCon();         
    $sql = "DELETE FROM `lista_exercicio` 
                  WHERE `idlst` = '$idlst'";
    $con->query($sql);
    return mysql_affected_rows();
}





/*
 * gerarRelatorioListaExerByID()
 *  
 * Retorna MySQL $result
 */
function gerarRelatorioListaExerByID($idlst){
    
    $con = gerarCon();      
    $sql = "SELECT  le.idlst, e.id as ide, u.id as iduser, u.nome, count(s.id) as qtdsolucao
              FROM  exercicio as e 
              JOIN  lista_exercicio as le ON e.id = le.ide
              JOIN  listadeexercicio as l ON l.id = le.idlst
              JOIN  solucao as s ON s.idexercicio = e.id
              JOIN  usuario as u ON s.iduser = u.id
             WHERE  s.deletado = 0
               AND  s.estado = 'completo'  
               AND  le.idlst = '$idlst'                     
          GROUP BY  le.idlst, e.id, u.nome
          ORDER BY  ide, u.nome ";
    
    $result = $con->query($sql);        
    
    return $result;
}


/**
 * geraRltQtdUserExerSoluByIDUser($iduser)
 *  
 * Retorna MySQL $result com a quantidade de soluções do aluno para cada exercício das listas
 *  idlst, ide, count(solucao) as qtd
 */
function geraRltQtdUserExerSoluByIDUser($iduser){
    
    $con = gerarCon();      
    $sql = " SELECT  idlst, ide, count(s.id) as qtd
               FROM  lista_exercicio as le 
    LEFT OUTER JOIN  solucao as s ON s.idexercicio = le.ide 
                AND  s.estado = 'completo'
                AND  s.iduser = $iduser                    
    LEFT OUTER JOIN  usuario as u ON s.iduser = u.id               
           GROUP BY  idlst, ide
           ORDER BY  idlst, ide
            ";
    
    $result = $con->query($sql);        
    
    return $result;
}


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                 L I S T A S   D E   R E C U R S O S                         *
 *                                                                             *
 *  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
 

function inserirRecursoDigital($obj){
    //Realizando conexão como BD
    $con = gerarCon();         
    $sql = "INSERT INTO `listaderecursos` (
                 `titulo`, 
                 `texto`, 
                 `texto2`, 
                 `prazoenvio`,
                 `permitirenvio`, 
                 `usarconstrutor`,
                 `socializar`,
                 `para`,
                 `datasocializacao`,
                 `datapublicacao`,
                 `datacriacao`,
                 `oculto`,
                 `deletado`
            )VALUES(
             '$obj->titulo',
             '$obj->texto',
             '$obj->texto2',
             '$obj->prazoenvio',
             '$obj->permitirenvio',
             '$obj->usarconstrutor',
             '$obj->socializar',
             '$obj->para',
             '$obj->datasocializacao',
             '$obj->datapublicacao',
             '$obj->datacriacao',
             '$obj->oculto',
             '$obj->deletado'
             )";
    $con->query($sql);    
    return $con->idGerado();
}

function relacionarRecursoDigital($ide,$idlst){
    $con = gerarCon();         
    $sql = "INSERT INTO `listaderecursos` (
                `ide`, 
                `idlst`
           )VALUES(
                '$ide',
                '$idlst'
           )";
    $con->query($sql);     
}
