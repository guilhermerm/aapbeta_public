<?php
include_once "$endatual/apps/cspl/mdl/exercicio.php";
include_once "$endatual/apps/cspl/mdl/exerplanoteste.php";
require_once "$endatual/db/conection.php";

 
/*
 *  C R E A T E
 */
// Recebe: objeto $planodeteste (sem id)
function inserirExerPlano($exerplanoteste){
    //Realizando conexão como BD
    $con = gerarCon();
    $sql = "INSERT INTO `exerplanoteste` (`idexercicio`,`deletado`)
            VALUES ('$exerplanoteste->idexercicio','$exerplanoteste->deletado')";    
    $con->query($sql);
    $id = $con->idGerado();
    return $id;
}
function inserirExerTeste($exertestes){    
    //Realizando conexão como BD
    $con = gerarCon();
    
    foreach($exertestes as $teste){
        $sql = "INSERT INTO `exerteste` (`idplanoteste`, `descricao`, `entrada`,`saidaesperada`,`deletado`)
                VALUES ('$teste->idplanoteste', '$teste->descricao', '$teste->entrada', '$teste->saidaesperada', '$teste->deletado')";
        $con->query($sql);    
    }
}

/*
 *  R E A D
 */

//Buscar planodeteste pelo id
function buscarExerPlanoId($id , $deletado=0){    
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT  id,idexercicio,deletado
              FROM  exerplanoteste
             WHERE  id = $id
               AND  deletado = $deletado
             LIMIT  1";    
    $result = $con->query($sql);    
    $linha = mysql_fetch_assoc($result);    
    
    //Instancia objeto exercicio
    $obj = new exerplanoteste();
    //Preenche os campos do objeto
    foreach($linha as $campo => $valor){
        $obj->$campo = $valor;
    }
    
    if (isset($obj)){
        //Retorna Array de Exercicios
        return $obj; 
    }else{
        return NULL;
    }     
                 
}

//Buscar IDPLANODETESTE dado o ID do EXERCICIO
function buscarIDPlanoByIDExer($idexercicio , $deletado=0){    
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT  id
              FROM  exerplanoteste
             WHERE  idexercicio = $idexercicio
               AND  deletado = $deletado
             LIMIT  1";    
    $result = $con->query($sql);    
    
    if(mysql_num_rows($result)>0){
        $linha = mysql_fetch_assoc($result);
        return $linha['id'];        
    }else{
        return 0;
    }
    
    //Instancia objeto exercicio
//    $obj = new exerplanoteste();
//    //Preenche os campos do objeto
//    foreach($linha as $campo => $valor){
//        $obj->$campo = $valor;
//    }
//    
//    if (isset($obj)){
//        //Retorna Array de Exercicios
//        return $obj; 
//    }else{
//        return NULL;
//    }     
                 
}



//Buscar teste pelo id
function buscarExerTesteId($id , $deletado=0){    
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT  id,idplanoteste,descricao,entrada,saidaesperada,deletado
              FROM  exerteste
             WHERE  id = $id
               AND  deletado = $deletado
             LIMIT  1";    
    $result = $con->query($sql);    
    $linha = mysql_fetch_assoc($result);    
    
    //Instancia objeto exercicio
    $obj = new exerteste();
    //Preenche os campos do objeto
    foreach($linha as $campo => $valor){
        $obj->$campo = $valor;
    }
    
    if (isset($obj)){
        //Retorna Array de Exercicios
        return $obj; 
    }else{
        return NULL;
    }     
                 
}

//Buscar todos Planos de Teste
function buscarTodosExerPlanos($deletado=0){
    //Realizando conexão como BD
    $con = gerarCon();
    $sql = "SELECT  id,idexercicio,deletado
              FROM  exerplanoteste
             WHERE  deletado = $deletado";
    $result = $con->query($sql);    
    
    while($linha = mysql_fetch_assoc($result)){
        //Instancia objeto exercicio
        $obj = new exerplanoteste();
        //Preenche os campos do objeto
        foreach($linha as $campo => $valor){
            $obj->$campo = $valor;
        }
        $objs[] = $obj;
    }
    
    if (isset($objs)){
        //Retorna Array de Exercicios
        return $objs; 
    }else{
        return NULL;
    }     
    
}
//Buscar todos testes de um plano
function buscarTodosExerTestes($idplanoteste){
    //Realizando conexão como BD
    $con = gerarCon();
    $sql = "SELECT  id,idplanoteste,descricao,entrada,saidaesperada,deletado
              FROM  exerteste
             WHERE  deletado = 0
               AND  idplanoteste = $idplanoteste";
    $result = $con->query($sql); 
    
    //$linha = mysql_fetch_assoc($result);
    while($linha = mysql_fetch_assoc($result)){
        //Instancia objeto exercicio
        $obj = new exerteste();
        //Preenche os campos do objeto
        foreach($linha as $campo => $valor){
            $obj->$campo = $valor;
        }
        $objs[] = $obj;
    }
    
    if (isset($objs)){
        //Retorna Array de Exercicios
        return $objs; 
    }else{
        return NULL;
    }     
    
}
   

/*
 *  U P D A T E
 */
function atualizarExerPlano($exerplano){
    $con = gerarCon();
    
    $sql = "UPDATE  exerplanoteste
               SET  idexercicio = $exerplano->idexercicio
                   ,deletado = $exerplano->deletado
             WHERE  id = $exerplano->id";    
    $result = $con->query($sql);
    return mysql_affected_rows($result);
}

function atualizarExerTeste($exerteste){
    $con = gerarCon();
    
    $sql = "UPDATE  exerteste
               SET  idplanoteste = $exerteste->idplanoteste
                   ,descricao = $exerteste->descricao
                   ,entrada = $exerteste->entrada
                   ,saidaesperada = $exerteste->saidaesperada
                   ,deletado = $exerteste->deletado
             WHERE  id = $exerteste->id";    
    $result = $con->query($sql);
    
    return mysql_affected_rows($result);
}



/*
 *  D E L E T E
 */
function deletarExerPlano($id){
    $con = gerarCon();
    
    $sql = "UPDATE  exerplanoteste
               SET  deletado = 1
             WHERE  id = $id";    
    $result = $con->query($sql);
    
    return mysql_affected_rows($result);
}

function deletarExerTeste($id){
    $con = gerarCon();
    
    $sql = "UPDATE  exerteste
               SET  deletado = 1
             WHERE  id = $id";    
    $result = $con->query($sql);
    
    return mysql_affected_rows($result);
}

