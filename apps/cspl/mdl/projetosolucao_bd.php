<?php
include_once "$endatual/apps/cspl/mdl/projetosolucao.php";
require_once "$endatual/db/conection.php";

 
/*
 *  C R E A T E
 */
// Recebe: objeto $projetosolucao (sem id)
function inserirProjetoSolucao($obj){    
    //Realizando conexão como BD
    $con = gerarCon();         
    $sql = "INSERT INTO `projetosolucao` (
                   `idprojeto`,
                   `iduser`,
                   `solucao`,
                   `codigohaskell`,
                   `instrucoes`,
                   `estado`,
                   `deletado`
            )VALUES (
                   '$obj->idprojeto', 
                   '$obj->iduser', 
                   '$obj->solucao', 
                   '$obj->codigohaskell',
                   '$obj->instrucoes',
                   '$obj->estado',
                   '$obj->deletado')";
    $con->query($sql);    
    return $con->idGerado();
}

/*
 *  R E A D
 */

//Buscar solucao pelo id
function buscarProjetoSolucaoId($id , $deletado=0){    
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT  id,idprojeto,iduser,solucao,codigohaskell,instrucoes,estado,deletado
              FROM  projetosolucao
             WHERE  id = $id
               AND  deletado = $deletado
             LIMIT  1";
    $result = $con->query($sql);    
    $linha = mysql_fetch_assoc($result);    
    
    //Instancia objeto projetosolucao
    $obj = new projetosolucao();
    //Preenche os campos do objeto
    foreach($linha as $campo => $valor){
        $obj->$campo = $valor;
    }
    
    if (isset($obj)){
        //Retorna Array de ProjetoSolucao
        return $obj; 
    }else{
        return NULL;
    }     
                 
}

//Buscar todas soluções
function buscarTodosProjetoSolucoes($deletado=0){
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT  id,idprojeto,iduser,solucao,codigohaskell,instrucoes,estado,deletado
              FROM  projetosolucao
             WHERE  deletado = $deletado";    
    $result = $con->query($sql);    
    
    while($linha = mysql_fetch_assoc($result)){
        //Instancia objeto projetosolucao
        $obj = new projetosolucao();
        //Preenche os campos do objeto
        foreach($linha as $campo => $valor){
            $obj->$campo = $valor;
        }
        $objs[] = $obj;
    }
    
    if (isset($objs)){
        //Retorna Array de ProjetoSolucao
        return $objs; 
    }else{
        return NULL;
    }     
    
}

function buscarTodosProjetoSolucoesByProjetoID($idprojeto,$estado="completo",$deletado=0){
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT   ps.id as id
                    ,ps.idprojeto as idprojeto
                    ,ps.solucao as solucao
                    ,ps.codigohaskell as codigohaskell
                    ,ps.instrucoes as instrucoes
                    ,ps.datacriacao as datacriacao
                    ,ps.estado as estado
                    ,ps.deletado as deletado
                    ,ps.iduser as iduser
                    ,u.nome as usernome
              FROM  projetosolucao as ps
              JOIN  usuario as u ON ps.iduser = u.id
             WHERE  ps.idprojeto = '$idprojeto'
               AND  ps.deletado = '$deletado'
               AND  ps.estado = '$estado'  
          ORDER BY  u.nome ASC, ps.datacriacao DESC
            ";
    $result = $con->query($sql);    
    
    while($linha = mysql_fetch_assoc($result)){
        //Instancia objeto projetosolucao
        $obj = new projetosolucao();
        //Preenche os campos do objeto
        foreach($linha as $campo => $valor){
            $obj->$campo = $valor;
        }
        $objs[] = $obj;
    }
    
    if (isset($objs)){
        //Retorna Array de Projetos Solucoes
        return $objs; 
    }else{
        return NULL;
    }     
    
}

function buscarTodosProjetoSolucoesUserByProjetoID($idprojeto,$iduser,$estado="completo",$deletado=0){
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT   s.id as id
                    ,s.idprojeto as idprojeto
                    ,s.solucao as solucao
                    ,s.codigohaskell as codigohaskell
                    ,s.instrucoes as instrucoes
                    ,s.datacriacao as datacriacao
                    ,s.estado as estado
                    ,s.deletado as deletado
                    ,s.iduser as iduser
                    ,u.nome as usernome
              FROM  projetosolucao as s
              JOIN  usuario as u ON s.iduser = u.id
             WHERE  s.idprojeto = '$idprojeto'
               AND  s.iduser = '$iduser'
               AND  s.deletado = '$deletado'
          ORDER BY  s.datacriacao DESC";
               //AND  s.estado = '$estado';
    $result = $con->query($sql);    
    
    while($linha = mysql_fetch_assoc($result)){
        //Instancia objeto projetosolucao
        $obj = new projetosolucao();
        //Preenche os campos do objeto
        foreach($linha as $campo => $valor){
            $obj->$campo = $valor;
        }
        $objs[] = $obj;
    }
    
    if (isset($objs)){
        //Retorna Array de Projetos Soluções
        return $objs; 
    }else{
        return NULL;
    }     
    
}
/*
 *  U P D A T E
 */
function atualizarProjetoSolucao($obj){
    $con = gerarCon();
    
    $sql = "UPDATE  projetosolucao
               SET   solucao = '$obj->solucao'
                    ,codigohaskell = '$obj->codigohaskell'
                    ,instrucoes = '$obj->instrucoes'
                    ,estado = '$obj->estado'
             WHERE  id = $obj->id";
    $result = $con->query($sql);
    
    return mysql_affected_rows($result);
}

/*
 *  D E L E T E
 */
function deletarProjetoSolucao($id){
    $con = gerarCon();
    
    $sql = "UPDATE  projetosolucao
               SET  deletado = 1
             WHERE  id = $id";    
    $result = $con->query($sql);
    if($result === false){
        return 0;
    }else{
        return mysql_affected_rows($result);        
    }
    return 0;
}

