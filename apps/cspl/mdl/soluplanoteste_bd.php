<?php
include_once "$endatual/apps/cspl/mdl/soluplanoteste.php";
require_once "$endatual/db/conection.php";

 
/*
 *  C R E A T E
 */
// Recebe: objeto $planodeteste (sem id)
function inserirSoluPlano($obj){
    //Realizando conexão como BD
    $con = gerarCon();
    $sql = "INSERT INTO `soluplanoteste` (`iduser`,`idsolucao`,`deletado`)
            VALUES ('$obj->iduser','$obj->idsolucao','$obj->deletado')";    
    $con->query($sql);
    $id = $con->idGerado();
    return $id;
}
function inserirSoluTestes($objts){
    //Realizando conexão como BD
    $con = gerarCon();        
    foreach($objts as $obj){
        $sql = "INSERT INTO `soluteste` (`idplanoteste`, `descricao`, `entrada`,`saidaesperada`,`deletado`)
                VALUES ('$obj->idplanoteste', '$obj->descricao', '$obj->entrada', '$obj->saidaesperada', '$obj->deletado')";
        $con->query($sql); 
    }
}
function inserirSoluResultado($obj){
    //Realizando conexão como BD
    $con = gerarCon();        
    $sql = "INSERT INTO `soluresultado` (`idplanoteste`, `saidahugs`, `saidaresultado`,`deletado`)
            VALUES ('$obj->idplanoteste', '$obj->saidahugs', '$obj->saidaresultado', '$obj->deletado')";
    $con->query($sql); 
    $id = $con->idGerado();
    return $id;
}

/*
 *  R E A D
 */

//Buscar planodeteste pelo id
function buscarSoluPlanoId($id , $deletado=0){    
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT  id,iduser,idsolucao,deletado
              FROM  soluplanoteste
             WHERE  id = $id
               AND  deletado = $deletado
             LIMIT  1";    
    $result = $con->query($sql);    
    $linha = mysql_fetch_assoc($result);    
    
    //Instancia objeto exercicio
    $obj = new soluplanoteste();
    //Preenche os campos do objeto
    foreach($linha as $campo => $valor){
        $obj->$campo = $valor;
    }
    
    if (isset($obj)){
        //Retorna Array de Exercicios
        return $obj; 
    }else{
        return NULL;
    }     
                 
}

//Buscar IDPLANODETESTE dado o ID da Solucao
function buscarIDPlanoByIDSolu($idsolucao , $deletado=0){    
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT  id
              FROM  soluplanoteste
             WHERE  idsolucao = $idsolucao
               AND  deletado = $deletado
             LIMIT  1";    
    $result = $con->query($sql);    
    $linha = mysql_fetch_assoc($result);
    
    return $linha['id'];   
                 
}


//Buscar teste pelo id
function buscarSoluTesteId($id , $deletado=0){    
    //Realizando conexão como BD
    $con = gerarCon();      
    $sql = "SELECT  id,idplanoteste,descricao,entrada,saidaesperada,deletado
              FROM  soluteste
             WHERE  id = $id
               AND  deletado = $deletado
             LIMIT  1";    
    $result = $con->query($sql);    
    $linha = mysql_fetch_assoc($result);    
    
    //Instancia objeto exercicio
    $obj = new soluteste();
    //Preenche os campos do objeto
    foreach($linha as $campo => $valor){
        $obj->$campo = $valor;
    }
    
    if (isset($obj)){
        //Retorna Array de Exercicios
        return $obj; 
    }else{
        return NULL;
    }     
                 
}

//Buscar todos Planos de Teste
function buscarTodosSoluPlanos($deletado=0){
    //Realizando conexão como BD
    $con = gerarCon();
    $sql = "SELECT  id,idexercicio,deletado
              FROM  soluplanoteste
             WHERE  deletado = $deletado";
    $result = $con->query($sql);    
    
    while($linha = mysql_fetch_assoc($result)){
        //Instancia objeto solucao
        $obj = new soluplanoteste();
        //Preenche os campos do objeto
        foreach($linha as $campo => $valor){
            $obj->$campo = $valor;
        }
        $objs[] = $obj;
    }
    
    if (isset($objs)){
        //Retorna Array de Exercicios
        return $objs; 
    }else{
        return NULL;
    }     
    
}
//Buscar todos testes de um plano
function buscarTodosSoluTestes($idplanoteste){
    //Realizando conexão como BD
    $con = gerarCon();
    $sql = "SELECT  id,idplanoteste,descricao,entrada,saidaesperada,deletado
              FROM  soluteste
             WHERE  deletado = 0
               AND  idplanoteste = $idplanoteste";
    $result = $con->query($sql); 
    
    //$linha = mysql_fetch_assoc($result);
    while($linha = mysql_fetch_assoc($result)){
        //Instancia objeto exercicio
        $obj = new soluteste();
        //Preenche os campos do objeto
        foreach($linha as $campo => $valor){
            $obj->$campo = $valor;
        }
        $objs[] = $obj;
    }
    
    if (isset($objs)){
        //Retorna Array de Exercicios
        return $objs; 
    }else{
        return NULL;
    }     
    
}

//Buscar todos testes de um plano
function buscarSoluResuladoByIDPlano($idplanoteste){
    //Realizando conexão como BD
    $con = gerarCon();
    $sql = "SELECT  id,idplanoteste,saidahugs,saidaresultado
              FROM  soluresultado
             WHERE  deletado = 0
               AND  idplanoteste = $idplanoteste";
    $result = $con->query($sql);     
    $linha = mysql_fetch_assoc($result);        
    
    if($linha !== FALSE){
        //Instancia objeto soluresultado
        $obj = new soluresultado();    
        //Preenche os campos do objeto
        foreach($linha as $campo => $valor){
            $obj->$campo = $valor;
        }
    }

    if (isset($obj)){        
        return $obj; 
    }else{
        return NULL;
    }    
    
}


/*
 *  U P D A T E
 */
function atualizarSoluPlano($soluplano){
    $con = gerarCon();
    
    $sql = "UPDATE  soluplanoteste
               SET  idexercicio = $soluplano->idexercicio
                   ,deletado = $soluplano->deletado
             WHERE  id = $soluplano->id";    
    $result = $con->query($sql);
    return mysql_affected_rows($result);
}

function atualizarSoluTeste($soluteste){
    $con = gerarCon();
    
    $sql = "UPDATE  soluteste
               SET  idplanoteste = $soluteste->idplanoteste
                   ,descricao = $soluteste->descricao
                   ,entrada = $soluteste->entrada
                   ,saidaesperada = $soluteste->saidaesperada
                   ,deletado = $soluteste->deletado
             WHERE  id = $soluteste->id";    
    $result = $con->query($sql);
    
    return mysql_affected_rows($result);
}

function atualizarSoluResultado($soluresultado){
    $con = gerarCon();
      
    $sql = "UPDATE  soluresultado
               SET  idplanoteste = $soluresultado->idplanoteste
                   ,saidahugs = $soluresultado->saidahugs
                   ,saidaresultado = $soluresultado->saidaresultado
                   ,deletado = $soluresultado->deletado
             WHERE  id = $soluresultado->id";    
    $result = $con->query($sql);
    
    return mysql_affected_rows($result);
}



/*
 *  D E L E T E
 */
function deletarSoluPlano($id){
    $con = gerarCon();
    
    $sql = "UPDATE  soluplanoteste
               SET  deletado = 1
             WHERE  id = $id";    
    $result = $con->query($sql);
    
    return mysql_affected_rows($result);
}

function deletarSoluTeste($id){
    $con = gerarCon();
    
    $sql = "UPDATE  soluteste
               SET  deletado = 1
             WHERE  id = $id";    
    $result = $con->query($sql);
    
    return mysql_affected_rows($result);
}

function deletarSoluResultado($id){
    $con = gerarCon();
    
    $sql = "UPDATE  soluresultado
               SET  deletado = 1
             WHERE  id = $id";    
    $result = $con->query($sql);
    
    return mysql_affected_rows($result);
}

