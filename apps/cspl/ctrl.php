<?php
   
require_once "$endatual/apps/admin/mdl/usuario.php";
require_once "$endatual/apps/admin/mdl/usuario_bd.php";
require_once "$endatual/apps/cspl/mdl/solucao.php";
require_once "$endatual/apps/cspl/mdl/solucao_bd.php";
require_once "$endatual/apps/cspl/mdl/soluplanoteste.php";
require_once "$endatual/apps/cspl/mdl/soluplanoteste_bd.php";
require_once "$endatual/apps/cspl/mdl/comentario.php";
require_once "$endatual/apps/cspl/mdl/comentario_bd.php";
require_once "$endatual/apps/cspl/mdl/exercicio.php";
require_once "$endatual/apps/cspl/mdl/exercicio_bd.php";
require_once "$endatual/apps/cspl/mdl/exerplanoteste.php";
require_once "$endatual/apps/cspl/mdl/exerplanoteste_bd.php";
require_once "$endatual/apps/cspl/mdl/socializacao.php";
require_once "$endatual/apps/cspl/mdl/socializacao_bd.php";
require_once "$endatual/apps/cspl/mdl/projeto.php";
require_once "$endatual/apps/cspl/mdl/projeto_bd.php";
require_once "$endatual/apps/cspl/mdl/projetosolucao.php";
require_once "$endatual/apps/cspl/mdl/projetosolucao_bd.php";
//require_once "$endatual/db/conection.php";

switch($page){
    //CSPL (Exercícios)
    case 'cspl':

    break;    

    case 'formardupla':
    case 'comparador':
        include "$endatual/apps/cspl/ctrl/$page.php";        
    break;    

        
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *  R E L A T Ó R I O S
     */        

    case 'relatoriolistas':      //Relatório das listas de exercício
    case 'relatorioexer':        //Relatório das listas de exercício
    case 'relatoriolistaexer':   //Relatorio - Relatório de uma lista de exercícios'
    case 'relatoriouserlistas':  //Relatorio - Relatório de uma lista de exercícios'
        include "$endatual/apps/cspl/ctrl/$page.php";        
    break;


    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *  L I S T A   D E 
     *  E X E R C I C I O S
     */    
    case 'novalistaexer':    //Formulário para uma nova lista de exercício
    case 'editlistaexer':    // Formulário para editar uma lista de exercício
    case 'mostralistas':     // Exibi TODAS as Listas
    case 'mostralistaexer':  //Mostrar 1 Lista de exercícios
        include "$endatual/apps/cspl/ctrl/$page.php";        
    break;
    


    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *  E X E R C I C I O S
     */
    case 'exercicio':     //Mostrar exercício
    case 'baseexercicio': //Lista a base de exercícios
    case 'novoexercicio': //Formulário de um novo exercícios
    case 'editexercicio': //Formulário para editar um exercício
        include "$endatual/apps/cspl/ctrl/$page.php";        
    break;

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *  S O L U Ç Ã O
     */        
    case 'mostrasolucao':  //Exibi uma solução
    case 'novasolucao':    //Formulário para uma nova solução
    case 'editsolucao':    //Formulário para editar uma solução
        include "$endatual/apps/cspl/ctrl/$page.php";
    break;   
        
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *  S O C I A L I Z A Ç Ã O
     */        
    case 'socializacao':
        //Formulário para editar uma solução
        include "$endatual/apps/cspl/ctrl/$page.php";
    break;    
    
        
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *  P R O J E T O S   E   T R A B A L H O S
     */         
    
    case 'mostraprojetos': 
        // LISTAR OS PROJETOS
        //Inserindo o conteúdo central da página
        $tpl->addFile("CONTEUDO","./apps/cspl/tpl/mostraprojetos.html");

        //$tpl->LINK_NOVOPROJETO = "?page=novoprojeto";

        if(isset($_GET['salvo'])){
            $tpl->block("BLOCK_MSGSALVO");
        }

        $objts = buscarTodosProjetos();

        if($objts!=NULL){
            foreach($objts as $a => $b){
                $tpl->PROJETOID        = $b->id;
                $tpl->PROJETOTITULO    = $b->titulo;                
                $tpl->LINK_PROJETOID   = "$endamb/?page=mostraprojeto&id=".$b->id;
                $tpl->LINK_PROJETOEDIT = "$endamb/?page=editprojeto&id=".$b->id;
                $tpl->block("BLOCK_PROJETO");                
            }
        }        

    break;            

    case 'novoprojeto': 

        //Inserindo o conteúdo central da página
        $tpl->addFile("CONTEUDO","./apps/cspl/tpl/novoprojeto.html");
       
        //var_dump($_POST);

        if(isset($_POST['formprojeto'])){

            //SALVAR DADOS RECEBIDOS               

            $projeto  = new projeto(); 
            
            $projeto->titulo     = addslashes(trim($_POST['titulo']));
            $projeto->enunciado  = addslashes(trim($_POST['mytextarea1']));
            $projeto->deletado     = 0;

            $idprojetogerado = inserirProjeto($projeto);

            header("Location: $endamb/?page=mostraprojetos");
            exit();
        }               
        //Escrevendo os LInks
        $tpl->LINK_FORMACTION = "?page=$page";
        $tpl->LINK_CANCELAR = "?page=inicio";
    break;    
        
    case 'mostraprojeto': 
        
        //Inserindo o conteúdo central da página
        $tpl->addFile("CONTEUDO","./apps/cspl/tpl/mostraprojeto.html");

        if(isset($_GET['salvo'])){
            $tpl->block("BLOCK_MSGSALVO");
        }

        if(isset($_GET['id'])){              

            $obj = buscarProjetoId($_GET['id']);

            $tpl->PROJETOID         = $obj->id;
            $tpl->PROJETOTITULO     = $obj->titulo;                
            $tpl->PROJETOENUNCIADO  = $obj->enunciado;
            
            //Mostrar as soluções do USUARIO
            $objs = buscarTodosProjetoSolucoesUserByProjetoID($obj->id, $userlogado->id);
//            var_dump($objs);
            foreach($objs as $a){
//                echo "AAAAAAAAAAAAAA <br>";
                $tpl->PROJETOSOLUCAOID          = $a->datacriacao;
                $tpl->LINK_EDITPROJETOSOLUCAO   = "$endamb/?page=editprojetosolucao&id=$a->id";
                $tpl->LINK_MOSTRAPROJETOSOLUCAO = "$endamb/?page=mostraprojetosolucao&id=$a->id";
                $tpl->block("BLOCK_PROJETOSOLUCAO");                
            }

            $tpl->block("BLOCK_PROJETO");

            
                        
            //Links
            $tpl->LINK_VOLTAR = "$endamb/?page=mostraprojetos";
            $tpl->LINK_ENVIARSOLUCAO = "$endamb/?page=novoprojetosolucao&id=$obj->id";
            
        }else{
           $tpl->block("BLOCK_NAOENCONTRADO");
        }
        
        
        
        
    break; 
        
    case 'editprojeto': 
        //Inserindo o conteúdo central da página
        $tpl->addFile("CONTEUDO","./apps/cspl/tpl/editarprojeto.html");
        
        if(isset($_POST['formprojeto'])){
            //ATUALIZAR 
            $obj  = new projeto();                                                
            $obj->id         = addslashes($_POST['idprojeto']);
            $obj->titulo     = addslashes($_POST['titulo']);
            $obj->enunciado  = addslashes($_POST['mytextarea1']);
            $obj->deletado   = 0;
            $idprojetogerado = atualizarProjeto($obj);           
            header("Location: $endamb/?page=mostraprojeto&id=$obj->id");

        }else{
            //EXIBIR PÁGINA DE EDIÇÃO
            
            if(isset($_GET['id'])){
                $idprojeto = $_GET['id'];
            
                //Buscar do Banco as Informações
                $obj    = buscarProjetoId($idprojeto);
                
                //Preenchendo a página com as informações
                $tpl->PROJETOID       = $obj->id;
                $tpl->PROJETOTITULO   = $obj->titulo;
                $tpl->PROJETOENUNCIADO= htmlentities($obj->enunciado);

                //Escrevendo os Links
                $tpl->LINK_FORMACTION = "?page=$page&id=$idprojeto";
                $tpl->LINK_CANCELAR   = "?page=inicio";
            }
        }
                
    break; 

    
   /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    *  P R O J E T O S 
    *                   S O L U C O E S
    */      
    
    case 'novoprojetosolucao':
       
        $idprojeto = (isset($_GET['id']))? $_GET['id']:"";

        //Inserindo o conteúdo central da página
        $tpl->addFile("CONTEUDO","./apps/cspl/tpl/novoprojetosolucao.html");
        $tpl->PROJETOSOLUCAOID = "";

        $obj = buscarProjetoId($idprojeto);
        $tpl->PROJETOID        = "$obj->id";
        $tpl->PROJETOTITULO    = "$obj->titulo";
        $tpl->PROJETOENUNCIADO = "$obj->enunciado";
                               

        if(isset($_POST['formprojetosolucao'])){

            //SALVAR DADOS RECEBIDOS               

            $ps  = new projetosolucao(); 

            $ps->iduser       = $userlogado->id;
            $ps->titulo       = addslashes(trim($_POST['titulo']));
            $ps->solucao      = addslashes(trim($_POST['mytextarea1']));
            $ps->codigohaskell= addslashes(trim($_POST['codigohaskell']));
            $ps->instrucoes   = addslashes(trim($_POST['mytextarea2']));
            $ps->idprojeto    = $_POST['idprojeto'];
            $ps->estado       = addslashes($_POST['projetosolucaoestado']);
            $ps->deletado     = 0;

            $idgerado = inserirProjetoSolucao($ps);                       

            header("Location: $endamb/?page=mostraprojetosolucao&id=$idgerado");

        }                       
        //Escrevendo os LInks
        $tpl->LINK_FORMACTION = "$endamb/?page=$page&id=$idprojeto";
        $tpl->LINK_CANCELAR = "$endamb/?page=mostraprojeto&id=$idprojeto";
    break;   

    case 'editprojetosolucao':
       
        $idps      = (isset($_GET['id']))? $_GET['id']:"";
        $idprojeto = (isset($_GET['proj'])) ? $_GET['proj'] :"";          

        //Inserindo o conteúdo central da página
        $tpl->addFile("CONTEUDO","./apps/cspl/tpl/novoprojetosolucao.html");
        $tpl->PROJETOSOLUCAOID = $idps;
        
        if(isset($_POST['formprojetosolucao'])){

            //ATUALIZAR DADOS RECEBIDOS
            $ps  = new projetosolucao(); 

            $ps->id           = addslashes(trim($_POST['idps']));
            $ps->iduser       = $userlogado->id;
            $ps->solucao      = addslashes(trim($_POST['mytextarea1']));
            $ps->codigohaskell= addslashes(trim($_POST['codigohaskell']));
            $ps->instrucoes   = addslashes(trim($_POST['mytextarea2']));
            $ps->idprojeto    = $_POST['idprojeto'];
            $ps->estado       = addslashes($_POST['projetosolucaoestado']);
            $ps->deletado     = 0;

            $idgerado = atualizarProjetoSolucao($ps);                       

            header("Location: $endamb/?page=mostraprojetosolucao&id=".$ps->id);

        }        
                        
        //Buscar ProjetoSolucao
        $ps = buscarProjetoSolucaoId($idps);
        
        $tpl->EDITSOLUCAO        = $ps->solucao;
        $tpl->EDITCODIGOHASKELL  = $ps->codigohaskell;
        $tpl->EDITINSTRUCOES     = $ps->instrucoes;        
        
        $idprojeto = $ps->idprojeto;

        //Buscar Projeto
        $projeto = buscarProjetoID($idprojeto);
        $tpl->PROJETOID        = "$projeto->id";
        $tpl->PROJETOTITULO    = "$projeto->titulo";
        $tpl->PROJETOENUNCIADO = "$projeto->enunciado";               
        
        //Escrevendo os LInks
        $tpl->LINK_FORMACTION = "$endamb/?page=$page&proj=$idprojeto";
        $tpl->LINK_CANCELAR = "$endamb/?page=mostraprojeto&id=".$idprojeto;
        
    break;     
    
    case 'mostraprojetosolucao':
    break;     
}    
