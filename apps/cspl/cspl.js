//var $jD = 
var $jQ = jQuery.noConflict();

jQuery(document).ready(function($) {

    /*
     *  Eventos de botões em Formulários
     */

    $('#btnsalvarconfigtranca').click(function(){
        document.formconfigtranca.submit();
    });
    
    //BOTAO Submit ALTERAR USUARIO
    $('#btnsalvarconfig').click(function(){         
        //nome
        if($("#nome").val() == ""){        
            alert("Informe o seu Nome");
            $("#nome").focus();
            return false;
        }        
        var sa = $("#senhaatual").val();
        var sn = $("#senhanova").val();
        var cs = $("#confirmasenha").val();        
        if(sa != "" && sn != ""){            
            if(cs != sn){
                alert("Confirmação de senha está diferente da nova senha");
                cs.val("").focus();
                return false;                
            }
            if(sa == sn){
                alert("Sua nova senha deve ser diferente da atual");
                cs.val("");
                sn.val("").focus();
                return false;
            }                        
        }else{
            sa.val("");
            sn.val("");
            cs.val("");            
        document.formconfiguser.submit();
        }
    });

    //BOTAO Submit ADD EXERCICIO
    $('#btnsalvarexercicio').click(function(){ 
        
        if($("#exertitulo").val() === ""){        
            alert("Informe o Título do Exercício");
            $("#exertitulo").focus();
            return false;
        }
                    
        document.formnovoexercicio.submit();
    });           
    
    //BOTAO Adicionar mais um TESTE no plano de teste
    $('#btnaddteste').bind('click',function(){
        
        var numentrada = $(this).attr('value');        
        numentrada++;        
        $(this).attr('value',numentrada);
        
        var htmladd = "<tr id='trteste"+numentrada+"'>"
                     +"      <td><input class='inputapp inputsteste' name='descricao"+numentrada+"' type='text' placeholder=''></td>"
                     +"      <td><input class='inputapp inputsteste' name='entrada"+numentrada+"' type='text' placeholder=''></td>"
                     +"      <td><input class='inputapp inputsteste' name='saidaes"+numentrada+"' type='text' placeholder=''></td>"
                     +"      <td style='padding: 0 4px 0 0'><div class='btnexcluiteste ink-button red pull-right smaller' title='Excluir este teste' style='float: right' value='"+numentrada+"' onclick='return excluiTesteLinha("+numentrada+")'><i class='fa fa-remove'></i></div></td>"
                     +"</tr>";
        
        //$("#bodyteste").append("<tr id='trteste"+numentrada+"'><td><input class='inputapp' name='descricao"+numentrada+"' type='text' placeholder=''></td><td><input class='inputapp' name='entrada"+numentrada+"' id='text-input' type='text' placeholder=''></td><td><input class='inputapp' name='saidaes"+numentrada+"' id='text-input' type='text' placeholder=''></td></tr>");
        $("#bodyteste").append(htmladd);
        $("input[name=descricao"+numentrada+"]").focus();        
    });
    
    //Botão Excluir Linha de Teste
    $('.btnexcluiteste').on('click',function(){
        alert("foi");
        var id = $(this).attr('value');
        $("#trteste"+id).remove();
    });
                    
    //BOTAO Submit ADD PROJETO
    $('#btnsalvarprojeto').click(function(){         
        var campo1 = $("#titulo");
        if( campo1.val() === ""){        
            alert("Informe o Título do Projeto");
            campo1.focus();
            return false;
        }                    
        document.formnovoprojeto.submit();
    });       
    
    $("#publicacaohora").mask("99:99");
    
    
});
/*
 *   -  document.ready() - FIM
 *                                                    
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

//BOTAO Submit ADD PROJETO
function salvarProjetoSolucao(){
    
//        var campo1 = $("#titulo");
//        if( campo1.val() === ""){        
//            alert("Informe o Título do Projeto");
//            campo1.focus();
//            return false;
//        }                    
        document.formnovoprojetosolucao.submit();
}

/*
 * Remover uma linha do plano de Teste
 */
function excluiTesteLinha(id){
    //alert("oi");
    $jQ("#trteste"+id).remove();
}

/*
 * S O L U Ç Ã O
 * 
 */

function iniciarAutoSave(){    
    setTimeout(function(){ 
        //ajaxSalva("autosave");
        //iniciarAutoSave();
    }, 20000);
}

function salvarSolucao(){
    var solucaoestado = $jQ("#solucaoestado");
    ajaxTestaESalva(solucaoestado.val());   
}
function realizarTestes(){    
    ajaxTestaESalva("emteste1");
}

function ajaxTestaESalva(estado){
    
    //Verificações
    //Se possui código
    //var codigohaskell = $jQ("#codigohaskell"); 
    var codigohaskell = editor.getValue();
    if (codigohaskell==""){
        alert("Está faltando a codificação");
        editor.focus();
        return false;
    }
    //Se função Principal está preenchida
    var funcaoprincipal = $jQ("#funcaoprincipal");
    if (funcaoprincipal.val()==""){
        alert("Está faltando indicar a função principal");
        funcaoprincipal.focus();        
        return false;
    }    
    //Se há ao menos 1 teste preenchido
    var testes = $jQ(".inputsteste");    
    //verificar se testes existe
    if(!testes.length){
        alert("Acrescente ao menos um teste no Planejamento de Testes");
        return false;
    }
        
    //TUDO OK [pré-requisitos verificados]    
    //var htmlwait = "<div class='all-100 align-center' style='height: 80px;'> <h1 style='color: #ccc;'><i class='fa fa-cog fa-spin'></i></h1><a id='btnstopteste' class='ink-button basic' value='3' tabindex='0'  href='javascript:void(0);' onclick='pararTestes();' ><i class='fa fa-stop'></i> Interromper</a></div>";
    var htmlwait = "<div class='all-100 align-center' style='height: 80px;'> <h1 style='color: #ccc;'><i class='fa fa-cog fa-spin'></i></h1></div>";
    $jQ(".retornofinal").html(htmlwait);
    
    tinyMCE.triggerSave();
    var compreensao = $jQ('#mytextarea1').val();
    var solucao = $jQ('#mytextarea2').val();
    var ide = $jQ("#idexercicio");
    var ids = $jQ("#ids");
    
    
    
    //Salvar Solução (todos os campos)
    if(ids.val() === '0'){
        //Inserir nova Solução
        var form = $jQ("#formnovasolucao");   
        $jQ.post('apost.php',{
            dataType : "json",
            op:"salvarsolucao",
            idexercicio: ide.val(),
            compreensao: compreensao,
            testes: testes.serialize(),
            solucao: solucao,
            codigohaskell: codigohaskell,
            funcaoprincipal: funcaoprincipal.val(),
            estado: estado
        },function(r){
            //alert("Algo: "+r);
            $jQ(".retornofinal").html(r['html']);
            Ink.Autoload.run();
        }).fail(function(r) {
            $jQ(".retornofinal").html("Ops! :/ (Alguma coisa de errado aconteceu!)");
            alert("Ops! : "+r['responseText']);
            console.log( r['responseText'] );
          });
        
        //Atualizar ID da Solução
        //ids.val( idgerado );
    }else{
        //Atualizar Solução
        $jQ(".result").html("iremos apenas atualizar--tudo de ");
    }
    
    
    //Realizar Testes
    
    //Preencher Tabela de Resultados
    
}

function enviarComentario(idsolucao){
    
    var comentario = $jQ("#comentario"+idsolucao);
    if (comentario.val()==""){
        alert("Escreva um comentário...");
        comentario .focus();  
        return false;
    }else{        
        $jQ.post('apost.php',{            
            dataType : "json",
            op:"enviarcomentario",
            idsolucao: idsolucao,
            texto: comentario.val()
        },function(r){
            if(r=="TRUE"){
                alert("Comentário enviado!");                
                //Construi comentário
                //htmlcoment = "<div class='boxcomentario ink-form column-group all-100'><div class='fw-400 all-100 nome'>Você:</div><div class='fw-300 all-100 comentario'>"+comentario.val("")+"</div></div>";
                window.location.reload();
                //comentario.val("");
                //$jQ("#resultcoment"+idsolucao).append(htmlcoment);
                
            }else{
                alert("Não foi possivel enviar. :/"+r);
            }
        }).fail(function(r) {
            alert("Não foi possivel enviar seu comentário, aconteceu algum erro no ambiente");
        });    
    }          
}

/*
 * Excluir uma solução
 */
function deletarSolucao(idsolucao){
    //alert("oi");
    //$jQ("#trteste"+id).remove();
    var linhasolucao = $jQ(".lisolucao[id="+idsolucao+"]");
    var cor = linhasolucao.css("color");
    linhasolucao.css("color","red");
    if (confirm("Excluir esta solução?")){        
        $jQ.post('apost.php',{ 
            dataType : "json",
            op:"deletarSolucao",
            idsolucao: idsolucao
        },function(r){
            if(r=="TRUE"){
                linhasolucao.remove();
            }else{               
                linhasolucao.css("color","blue");
            }
        }).fail(function(r) {
            linhasolucao.css("color",cor);            
        });    
    }else{
        linhasolucao.css("color",cor);        
    }
}

/*
 * Socializa uma solução
 */
function socializarSolucao(idsolucao){    
    
    var linhasolucao = $jQ(".lisolucao[id="+idsolucao+"]");
    var valor        = linhasolucao.attr("value");        
    
    var cor = linhasolucao.css("color");
    linhasolucao.css("color","blue");    
    
    if(valor === "1" || valor === 1){
        valor = "0";
    }else{
        valor = "1";
    }
    
    $jQ.post('apost.php',{ 
        dataType : "json",
        op:"socializarSolucao",
        idsolucao: idsolucao,
        valoratual: valor
    },function(r){
        if(r=="TRUE"){    
            linhasolucao.attr("value",valor);
            if (valor === "1" || valor === 1) {                
                linhasolucao.css("color","#FF9C00");
                linhasolucao.addClass("socializado");
            } else {
                linhasolucao.css("color","inherit");
                linhasolucao.removeClass("socializado");
            }
        }else{
            //Volta ao normal
            linhasolucao.css("color",cor);            
            //linhasolucao.css("color","red");
        }
    }).fail(function(r) {
        linhasolucao.css("color",cor);            
    });    
    
}

 /*    <!--- Ink (Modal) --->
  * 
  */
 
function inkAutoloadRun(){    
    Ink.Autoload.run();
}

           
function abrirSolucao(id){
    Ink.requireModules( ['Ink.UI.Modal_1'], function(Modal){
        var modal = new Modal('#modalsolucao');                        

        //Montando o Conteúdo central do Modal (textsolucao) 
        var textsolucao = "vazio";
        //Buscando Solução via ajax
        $jQ.post('apost.php',{
            dataType : "json",
            op:"solucao2modal",
            id: id
            },function(r){
                textsolucao = r;
                    //Construindo o layout do Modal                            
                    var text = "";                            
                    text+="    <div class='modal-header'>";
                    text+="        <button class='modal-close ink-dismiss'></button> <h3 id='modal-title'>Solução</h3>";
                    text+="    </div>";
                    text+="    <div class='modal-body' id='modalContent'>";                    
                    text+="        <div id='modalboxcontent'>";
                    text+= textsolucao;
                    text+="        </div>";
                    text+="    </div>";
                    text+="    <div class='modal-footer'>";
                    text+="        <div class='push-right'>";
                    text+="            <!-- Anything with the ink-dismiss class will close the modal -->";
                    text+="            <button class='ink-button caution ink-dismiss'>Fechar</button>";
                    text+="        </div>";
                    text+="    </div>";                                
                    
                //Montar modal
                modal.setContentMarkup(text);
                
            }).fail(function(r) {
                alert(r);
            }); 



        //modal.setContentMarkup(textsolucao);
    });
}

function realizaContagem(id){
    var soma = 0;
    $jQ(".inputsexerbase").each(function(){
        if($jQ(this).is(':checked')){            
            soma++;
        }else{            
        }
    });
    if(soma == 0){
        $jQ("#contagemexercicios").html("Nenhuma atividade selecionada");    
        $jQ("#msgrecursos").show();        
        
    }else{
        if(soma == 1){
            $jQ("#contagemexercicios").html("1 atividade selecionada");
            $jQ("#msgrecursos").hide();        
        }else{
            $jQ("#contagemexercicios").html(soma+" atividades selecionadas");
            $jQ("#msgrecursos").hide();
        }
    }
    if($jQ("#exercheck"+id).is(':checked')){
        $jQ(".liexer[id="+id+"]").css("background","#CCC");   
        $jQ("#exerrecurso"+id).slideDown();
    }else{
        $jQ(".liexer[id="+id+"]").css("background","white");
        $jQ("#exerrecurso"+id).slideUp();
    }
}

function realizaContagemA(){
    var soma = 0;
    $jQ(".inputsexerbase").each(function(){
        var exerchekid = $jQ(this).attr("id"); //alert(id);
        var id = exerchekid.substr(9);
        if($jQ(this).is(':checked')){      
            $jQ(".liexer[id="+id+"]").css("background","#CCC");        
            soma++;
        }else{
            $jQ(".liexer[id="+id+"]").css("background","white");
            $jQ("#exerrecurso"+id).hide();
        }
    });
    if(soma == 0){
        $jQ("#contagemexercicios").html("Nenhuma atividade selecionada");
    }else{
        $jQ("#msgrecursos").hide();
        if(soma == 1){
            $jQ("#contagemexercicios").html("1 atividade selecionada");
        }else{
            $jQ("#contagemexercicios").html(soma+" atividades selecionadas");
        }
    }
}

function configMural(id){    
    $jQ("#configmural"+id).toggle();
}

function mostraTab(id){        
    
    $jQ(".closetabaluno1").hide();
    $jQ("#tabaluno1"+id).show();
    
}




/*
 * Esta função provém do onKeyUp do textarea "interface"
 * Esta função altera a entrada da "validação de teste" de 
 * acordo com as entradas denifinas na "interface".
 */
function alterarEntradas(){    
    var interface = $jQ("#interface").val();
    //retirar o nome da função
    interface = interface.trim();    
    //encontra o espaço
    var pos = interface.indexOf(" ");
    //corta o que tem antes do espaço
    if(pos != -1){
        result = interface.substring(pos);        
        var pos2 = result.indexOf("=");
        if(pos2 != -1){
            var result = result.substring(0,pos2);
        }
    }else{
        result ="";
    }
    
    $jQ("#spanpentradas").text(result.trim());
    $jQ("#pentrada").val(result.trim());
    
}


/*
 */
function maisMural(idatividade){    
    //$jQ(".rmural").show("fast");
    var mural = $jQ(".rmural").html();
    //var mural = $jQ(".rmural");    
    $jQ(".listarecursos[id=exerrecurso"+idatividade+"]").append("<div class='column all-100 boxformsocial boxazul rmural' id='mural2'>"+mural+"</div>");
    //$jQ(".listarecursos[id=exerrecurso"+idatividade+"]").append(mural);
}
function removeMural(){    
    //$jQ(this).css("color","red");
    //$jQ(".rmural[id=rmural2]").hide();
    //$jQ(this).parent(".rmural").hide();
    $jQ(".rmural").remove();
}


function maisConstrutor(idatividade){    
    //$jQ(".rconstrutor").show("fast");
    var mural = $jQ(".rconstrutor").html();
    //var mural = $jQ(".rmural");    
    $jQ(".listarecursos[id=exerrecurso"+idatividade+"]").append("<div class='column all-100 boxformsocial boxazul rconstrutor' id='const2'>"+mural+"</div>");
    //$jQ(".listarecursos[id=exerrecurso"+idatividade+"]").append(mural);
}
function removeConstrutor(){    
    //$jQ(".rmural[id=rmural2]").hide();
    $jQ(".rconstrutor").remove();
    
}


function maisConstrutorColab(idatividade){    
    $jQ(".rmural").show("fast");
    var mural = $jQ(".rconstrutorcolab").html();
    //var mural = $jQ(".rmural");    
    $jQ(".listarecursos[id=exerrecurso"+idatividade+"]").append("<div class='column all-100 boxformsocial boxazul rconstrutorcolab' id='constcolab2'>"+mural+"</div>");
}
function removeConstrutorColab(){    
    //$jQ(".rmural[id=rmural2]").hide();
    $jQ(".rconstrutorcolab").remove();
    
}