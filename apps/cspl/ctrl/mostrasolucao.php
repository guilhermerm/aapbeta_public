<?php
/*
 * Mostrar uma Solução
 * 
 * > Exibe uma solução completa
 */
   
$id = (isset($_GET['id'])) ? $_GET['id'] : "";
if ($id != "") {

    //Inserindo o conteúdo central da página
    $tpl->addFile("CONTEUDO", "./apps/cspl/tpl/mostrasolucao.html");

    //REALIZANDO BUSCAS NO BANCO
    $solucao = buscarSolucaoId($id);
    $coments = buscarComentariosByIDSolu($id);
    $usersolu = buscarUsuarioId($solucao->iduser);
    $exercicio = buscarExercicioId($solucao->idexercicio);

    $idplanoteste = buscarIDPlanoByIDSolu($id);
    $testes = buscarTodosSoluTestes($idplanoteste);
    
    //PREENCHER O EXERCÍCIO
    $tpl->EXERID = $exercicio->id;
    $tpl->EXERTITULO = $exercicio->titulo;
    $tpl->EXERENUNCIADO = $exercicio->enunciado;        

    //PREENCHER OS COMENTÁRIOS
    if ($coments != NULL) {
        foreach ($coments as $c) {
            $tpl->COMENTARIOUSER = $c->usernome;
            $tpl->COMENTARIO = $c->texto;
            $tpl->block("BLOCK_COMENTARIO");
        }
    }

    //PREENCHER OS TESTES
    if ($testes != NULL) {
        foreach ($testes as $c) {
            $tpl->TESTEDESCRICAO = $c->descricao;
            $tpl->TESTEENTRADA = $c->entrada;
            $tpl->TESTESAIDAES = $c->saidaesperada;
            $tpl->block("BLOCK_TESTES");
        }
        $tpl->block("BLOCK_BOXTESTES");
    }

    //PREENCHER RESULTADOS
    $soluresultado = buscarSoluResuladoByIDPlano($idplanoteste);

    if ($soluresultado != NULL) {
        $htmlsaidahugs = $soluresultado->saidahugs;
        $htmltableresultados = $soluresultado->saidaresultado;
        
        $tpl->RESULTTABLES = str_replace("hide-all"," ",$htmltableresultados);
        $tpl->RESULTHUGS   = $htmlsaidahugs;
        $tpl->block("BLOCK_RESULTADOSOBTIDOS");
    }


    $tpl->USERSOLUCAOID = $solucao->id;
    $tpl->USERCOMPREENSAO = $solucao->compreensao;
    $tpl->USERCODIGOHASKELL = $solucao->codigohaskell;
    $tpl->USERSOLUCAOTEXTO = $solucao->solucao;    
    $tpl->USERSOLUCAONOME = $usersolu->nome;
    $tpl->USERLOGADONOME = $userlogado->nome;
    //$tpl->block("BLOCK_SOLUCAO");            
}