<?php
/*
 * MOSTRAR COMPARAÇÕES
 * 
 * > Página que mostra todos os exercícios Socializados
 */
include_once "$endatual/apps/cspl/agt/agt_comparador.php";
        
$idlista     = (isset($_GET['lst'])) ? $_GET['lst'] :"";
$idexercicio = (isset($_GET['exer']))? $_GET['exer']:41;
   
//Inserindo o conteúdo central da página
$tpl->addFile("CONTEUDO","./apps/cspl/tpl/comparador.html");


$exercicio = buscarExercicioID($idexercicio);

$tpl->EXERID        = $exercicio->id;
$tpl->EXERTITULO    = $exercicio->titulo;
$tpl->EXERENUNCIADO = $exercicio->enunciado;    
    

    
    $solucoes = buscarTodasSolucoesByExercicioID($idexercicio);
    $qtdsolu  = count($solucoes);

    foreach ($solucoes as $s){    
        $codigos[] = simplificarCodigo($s->codigohaskell); 
    }
        
    $htmlsaida = "";
    $htmlsaida .=  "<table class='tablecomp' style='border:1px solid #ccc; padding: 10px;'>";
    $htmlsaida .=  "<tr><td>-</td>";    
    for($i=0; $i<$qtdsolu; $i++){
        $htmlsaida .=  "<th> Aprendiz 0".($i+1)."<br><a href='#' class='small fw-300'>Ver solução</a></td>";    
    }
    $htmlsaida .=  "</tr>";    

    $similaridades = NULL;


    for($i=0; $i<$qtdsolu; $i++){
        $htmlsaida .=  "<tr>";    
        $htmlsaida .=  "<th> Aprendiz 0".($i+1)."<br><a href='#' class='small fw-300'>Ver solução</a></td>";
        for($j=0; $j<$qtdsolu; $j++){
            if($i<=$j){
                $htmlsaida .=  "<td>-</td>";    
                continue;
            }
            $porcentagem = calcularSimilaridade($codigos[$i], $codigos[$j]);
    //        $similaridades[] = "simi(".$i.",".$j.",".$porcentagem.")";
            $similaridades[] = [$i+1 , $j+1 , $porcentagem ];
            $tamfont = 100+($porcentagem/1);
            $htmlsaida .=  "<td class='tdcomp' style='width:50px; font-size:$tamfont%' >";
                        
            $htmlsaida .= number_format($porcentagem,2)."%";            
            
            $htmlsaida .=  "</td>";        
        }    
        $htmlsaida .=  "</tr>";    
    }
    $htmlsaida .=  "</table>";
    $htmlsaida .=  "</div>";
    
    
    $tpl->RESULTADO = $htmlsaida;
    
    
    