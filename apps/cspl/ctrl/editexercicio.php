<?php
/*
 * EDITAR EXERCÍCIO
 * 
 * > Formulário para editar um exercício
 */
    
//Inserindo o conteúdo central da página
$tpl->addFile("CONTEUDO", "./apps/cspl/tpl/formexercicio.html");

if (isset($_POST['formexercicio'])) {

    $exercicio = new exercicio();

    $exercicio->id = addslashes($_POST['idexercicio']);
    $exercicio->titulo = addslashes($_POST['exertitulo']);
    $exercicio->enunciado = addslashes($_POST['mytextarea1']);
    $exercicio->interface = addslashes($_POST['interface']);
    $exercicio->validarteste = addslashes($_POST['validarteste']);
    $exercicio->mostraplano = (isset($_POST['mostraplano'])) ? 1 : 0;
    $exercicio->tiposolucao = addslashes($_POST['tiposolucao']);
    $exercicio->nivel = addslashes($_POST['nivel']);
    $exercicio->deletado = 0;

    atualizarExercicio($exercicio);

    //Um novo Plano de Teste é gerado (Mesmo que igual ao anterior)
    $exerplano = new exerplanoteste();
    $exerplano->idexercicio = $exercicio->id;
    $exerplano->datacriacao = date('Y-m-d H:i:s');
    $exerplano->deletado = 0;

    $idplanotestegerado = inserirExerPlano($exerplano);

    $exertestes = array();

    //ORGANIZANDO OS TESTES NO VETOR DE TESTES
    foreach ($_POST as $a => $b) {

        if (trim(substr($a, 0, 5)) === "descr") {

            $indice = trim(substr($a, 9));

            $exteste = new exerteste();

            $exteste->idplanoteste = $idplanotestegerado;
            $exteste->descricao = addslashes(trim($b));
            $exteste->entrada = addslashes(trim($_POST["entrada$indice"]));
            $exteste->saidaesperada = addslashes(trim($_POST["saidaes$indice"]));
            $exteste->deletado = 0;

            $exertestes[] = $exteste;
        }
    }
    inserirExerTeste($exertestes);

    header("Location: $endamb/?page=baseexercicio");
    exit();
} else {
    //EXIBIR PÁGINA DE EDIÇÃO
    $tpl->TITULOFORMEXER = "Editar Exercício";

    if (isset($_GET['id'])) {
        $idexer = $_GET['id'];

        //Buscar do Banco as Informações
        $exer  = buscarExercicioId($idexer);
        $idplano = buscarIDPlanoByIDExer($idexer);
        $testes  = buscarTodosExerTestes($idplano);

        //Preenchendo a página com as informações
        $tpl->EXERID = $exer->id;
        $tpl->EXERTITULO = $exer->titulo;
        $tpl->EXERENUNCIADO = htmlentities($exer->enunciado);
        $tpl->EXERINTERFACE = htmlentities($exer->interface);
        $tpl->EXERVALTESTE  = htmlentities($exer->validarteste);
        
        $tpl->MOSTRAPLANOCHECK = ($exer->mostraplano == '1') ? "checked='checked'":"";
        echo "sa: $exer->mostraplano";
        if ($exer->tiposolucao == "mcp") {
            $tpl->TIPOMCP = "checked='checked'";
            $tpl->TIPOTEXTO = "";
        } else {
            $tpl->TIPOMCP = "";
            $tpl->TIPOTEXTO = "checked='checked'";
        }

        //Imprimindo cada linha de teste
        if ($testes != NULL) {
            $i = 1;
            foreach ($testes as $obj) {
                $tpl->TESTECONT = $i;
                $tpl->TESTEID = $obj->id;
                $tpl->TESTEDESCRICAO = $obj->descricao;
                $tpl->TESTEENTRADA = $obj->entrada;
                $tpl->TESTESAIDAES = $obj->saidaesperada;
                $i++;
                $tpl->block("BLOCK_EXERTESTE");
            }
            $tpl->TESTEQTD = $i;
        }
        //Escrevendo os LInks
        $tpl->LINK_FORMACTION = "?page=$page&id=$idexer";
        $tpl->LINK_CANCELAR = "?page=inicio";
    }
}
        