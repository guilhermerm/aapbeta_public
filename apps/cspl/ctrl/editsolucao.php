<?php
/*
 * EDITAR SOLUÇÃO
 * 
 * > Formulário para editar uma solução
 */

$ids = (isset($_GET['id'])) ? $_GET['id'] : "";
$idlista = (isset($_GET['lst'])) ? $_GET['lst'] : "";

//Inserindo o conteúdo central da página
$tpl->addFile("CONTEUDO", "./apps/cspl/tpl/formsolucao.html");


//Buscar Solucao
$solucao = buscarSolucaoId($ids);

$tpl->EDITCOMPREENSAO = $solucao->compreensao;
$tpl->EDITSOLUCAO = $solucao->solucao;
$tpl->EDITCODIGOHASKELL = $solucao->codigohaskell;
$tpl->EDITFUNCAOPRINCIPAL = $solucao->funcaoprincipal;


$idexercicio = $solucao->idexercicio;

//Buscar Exercicio
$exercicio = buscarExercicioID($idexercicio);
$tpl->IDEXERCICIO = "$exercicio->id";
$tpl->TITULOEXERCICIO = "$exercicio->titulo";
$tpl->ENUNCIADOEXERCI = "$exercicio->enunciado";


//Buscar os testes
$idplanoteste = buscarIDPlanoByIDSolu($ids);
$testes = buscarTodosSoluTestes($idplanoteste);
$cont = 0;
foreach ($testes as $a => $b) {
    $tpl->CONT = $cont++;
    $tpl->TESTEDESCRICAO = $b->descricao;
    $tpl->TESTEENTRADA = $b->entrada;
    $tpl->TESTESAIDAES = $b->saidaesperada;
    $tpl->block("BLOCK_SOLUTESTE");
}
$tpl->QTDTESTES = $cont;

//Escrevendo os LInks
$tpl->LINK_FORMACTION = "$endamb/?page=$page&exer=$idexercicio";
$tpl->LINK_CANCELAR = "$endamb/?page=listaexer&id=" . $idlista;
