<?php
/*
 * MOSTRA LISTAS
 * 
 * > Página para Relatório de todas as listas de exercícios
 */

//Listas de Exercícios de uma TURMA
$tpl->addFile("CONTEUDO","./apps/cspl/tpl/relatoriolistas.html");

if(isset($_GET['salvo'])){
    $tpl->block("BLOCK_MSGSALVO");
}

//Preenchendo o cabeçalho com as listas
$objlistas = buscarTodasListas();
//$objexers  = buscarTodosExercicios();
$objusers  = buscarTodosUsuarios();
if($objlistas != NULL){
    
    //CABEÇALHO
    foreach($objlistas as $indx => $lst){        
        //Melhorar, precisa somente da quantidade de exercícios        
        $exercicios = buscarTodosExerciciosByIdLista($lst->id);
        $qtdexers = count($exercicios);
        for($i=1;$i<=$qtdexers;$i++){            
            $tpl->INDEXEXER    = $i;            
            $tpl->block("BLOCK_INDEXEXER");            
        }
        $tpl->LSTNOME    = $lst->titulo;
        $tpl->LSTQTDEXER = $qtdexers;
        $tpl->block("BLOCK_LISTANOME");            
    }
    
    //LINHA DE CADA USER
    $total = 0;
    foreach($objusers as $indx1 => $usu){
        if($usu->perfil != '3'){
            continue;
        }
        $vetor = geraRltQtdUserExerSoluByIDUser($usu->id);
        while($l = mysql_fetch_assoc($vetor)){
                    if($l['qtd'] > 0){
                        $tpl->RLTUSEREXER = '<i class="fa fa-check-square-o"></i>';
                        //$tpl->RLTUSEREXER = $l['qtd'];
                        $total += $l['qtd'];
                    }else{
                        $tpl->RLTUSEREXER = '-';
                    }
            $tpl->block("BLOCK_RLTUSEREXER");
        }
        /*
        foreach($objlistas as $indx2 => $lst){
            $exercicios = buscarTodosExerciciosByIdLista($lst->id);
            $qtdexers = count($exercicios);            
            foreach($exercicios as $indx3 => $exer){
                //Verificar se tem solução do $usu->id para $exer->id na lista $lst->id
                //Resultado
                $tpl->RLTUSEREXER = "1";
                $tpl->block("BLOCK_RLTUSEREXER");
            }
        }
        */
        $tpl->RLTUSERNAME = $usu->nome;
        $tpl->block("BLOCK_RLTUSERLINHA");    
    }
    $tpl->block("BLOCK_LISTAS");
    
}