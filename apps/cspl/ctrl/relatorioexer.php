<?php
/*
 * MOSTRA Relatório de um Exercicio
 * 
 * > Página para Relatório de entregas para um exercício
 */

//Listas de Exercícios de uma TURMA
$tpl->addFile("CONTEUDO","./apps/cspl/tpl/relatorioexer.html");

if(isset($_GET['salvo'])){
    $tpl->block("BLOCK_MSGSALVO");
}

if(isset($_GET['id'])){
    $idexer = $_GET['id'];
}

//Preenchendo o cabeçalho com as listas
//$objlistas = buscarTodasListas();
//$objexers  = buscarTodosExerciciosByIdLista($idexer);

//BUSCAR O EXERCÍCIO
$exercicio = buscarExercicioID($idexer);

$tpl->EXERID        = $exercicio->id;
$tpl->EXERTITULO    = $exercicio->titulo;
$tpl->EXERENUNCIADO = $exercicio->enunciado;

//BUSCAR Lista dos Alunos
$objusers = buscarTodosUsuarios();

$cortes ='#656565';
$icontes='cog'; 
$corinc ='#996600';
$iconinc='save';
$corcom ='#004d99';
$iconcom='save';

if($exercicio != NULL){       
    //LINHA DE CADA USER
    $total = 0;
    foreach($objusers as $indx1 => $usu){
        if($usu->perfil != '3'){ // perfil 3 é o 'aluno'
            continue;
        }
        //$vetor = geraRltQtdUserExerSoluByIDUser($usu->id);
        $vetor  = buscarTodasSolucoesUserByExercicioID($idexer, $usu->id);
        $linhahist = "";
        
        $qtdtes = 0;
        $qtdinc = 0;
        $qtdcom = 0;
        
        if($vetor != NULL){
            $vetor = array_reverse($vetor);
            foreach($vetor as $solu){
                
                $cor='';$icon='';
                switch ($solu->estado){
                    case 'emteste1'  : $cor=$cortes; $icon=$icontes; $qtdtes++; break;
                    case 'incompleto': $cor=$corinc; $icon=$iconinc; $qtdinc++; break;
                    case 'completo'  : $cor=$corcom; $icon=$iconcom; $qtdcom++; break;
                }                
                
                $link_mostrasolucao = "$endamb/?page=mostrasolucao&id=$solu->id";
                
                $linhahist .= "<a href='$link_mostrasolucao' target='_blank' style='color:$cor;' title='$solu->datacriacao'><i class='fa fa-$icon'></i></a>  ";
            }//foreach                    
        }else{
            $linhahist = "-";            
        }                        
        
        $tpl->QTDTES = ($qtdtes==0?'-':$qtdtes);
        $tpl->QTDINC = ($qtdinc==0?'-':$qtdinc);
        $tpl->QTDCOM = ($qtdcom==0?'-':$qtdcom);
        
        $tpl->RLTUSERHISTSOLUCOES = $linhahist;
        $tpl->block("BLOCK_RLTUSEREXER");

        $tpl->RLTUSERNAME = $usu->id." ".$usu->nome;
        $tpl->block("BLOCK_RLTUSERLINHA");
        $total++;
        
    }//foreach
    
    $tpl->CORTES  = $cortes ;
    $tpl->ICONTES = $icontes;
    $tpl->CORINC  = $corinc ;
    $tpl->ICONINC = $iconinc;
    $tpl->CORCOM  = $corcom ;
    $tpl->ICONCOM = $iconcom;
    
    $tpl->block("BLOCK_LISTAS");
    //$tpl->OUTPUT = $total;
    
}