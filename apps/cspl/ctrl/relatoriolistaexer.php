<?php
/*
 * MOSTRAR Relatório de uma Lista de Exercícios
 * 
 */

//Listas de Exercícios de uma TURMA
$tpl->addFile("CONTEUDO","./apps/cspl/tpl/relatoriolistaexer.html");

if(isset($_GET['salvo'])){
    $tpl->block("BLOCK_MSGSALVO");
}

if(isset($_GET['id'])){
    $idlstexer = $_GET['id'];
}

//Preenchendo o cabeçalho com as listas
$objlistas = buscarTodasListas();
foreach ($objlistas as $lst){
    if($lst->id == $idlstexer){
        $tpl->LISTANOMEATUAL = $lst->titulo;
        $tpl->LINKLISTAATUAL = "$endamb/?page=mostralistaexer&id=$lst->id";
    }

    $tpl->LINK_LISTA = "$endamb/?page=relatoriolistaexer&id=$lst->id";
    $tpl->LISTANOME  = $lst->titulo;
    
    
    $tpl->block("BLOCK_LINKSLISTAS");    
}

//BUSCAR O EXERCÍCIO
$objexers  = buscarTodosExerciciosByIdLista($idlstexer);

//BUSCAR Lista dos Alunos
$objusers = buscarTodosUsuarios();

$cortes ='#656565';
$icontes='cog'; 
$corinc ='#996600';
$iconinc='save';
$corcom ='#004d99';
$iconcom='save';

if($objexers != NULL){    
    $qtdexers  = count($objexers);               
    
    /*  
     * R E L A T Ó R I O   P O R   A L U N O     
     */
    
    //LINHA DE CADA USER
    $total = 0;
    foreach($objusers as $indx1 => $usu){
        if($usu->perfil != '3'){ // perfil 3 é o 'aluno'
            continue;
        }                
        
        $numexer = 1;
        
        foreach($objexers as $exer){
            
            if($numexer==1){
                //$tpl->QTDEXER = $qtdexers;
                $tpl->RLTUSERNAME = "<i class='fa fa-user'></i> ".$usu->nome;
                $tpl->block("BLOCK_TBLUSERNAME");
            }
            
            //$vetor = geraRltQtdUserExerSoluByIDUser($usu->id);
            $vetor  = buscarTodasSolucoesUserByExercicioID($exer->id, $usu->id);
            $linhahist = "";

            $qtdtes = 0;
            $qtdinc = 0;
            $qtdcom = 0;

            if($vetor != NULL){
                $vetor = array_reverse($vetor);
                foreach($vetor as $solu){

                    $cor='';$icon='';
                    switch ($solu->estado){
                        case 'emteste1'  : $cor=$cortes; $icon=$icontes; $qtdtes++; break;
                        case 'incompleto': $cor=$corinc; $icon=$iconinc; $qtdinc++; break;
                        case 'completo'  : $cor=$corcom; $icon=$iconcom; $qtdcom++; break;
                    }                

                    $link_mostrasolucao = "$endamb/?page=mostrasolucao&id=$solu->id";

                    $linhahist .= "<a href='$link_mostrasolucao' target='_blank' style='color:$cor;' title='$solu->datacriacao'><i class='fa fa-$icon'></i></a>  ";
                }//foreach                    
            }else{
                $linhahist = "-";            
            }

            $tpl->EXERNUM = $numexer;
            $tpl->EXERTIT = $exer->titulo;
            
            $tpl->QTDTES = ($qtdtes==0?'-':$qtdtes);
            $tpl->QTDINC = ($qtdinc==0?'-':$qtdinc);
            $tpl->QTDCOM = ($qtdcom==0?'-':$qtdcom);                                    
            
            $tpl->RLTUSERHISTSOLUCOES = $linhahist;
            $tpl->block("BLOCK_RLTUSEREXER");
            
            $numexer++;
            $total++;
        }//foreach                
        
        $tpl->block("BLOCK_RLTUSERLINHA");
    
    }//foreach
    
    $tpl->CORTES  = $cortes ;
    $tpl->ICONTES = $icontes;
    $tpl->CORINC  = $corinc ;
    $tpl->ICONINC = $iconinc;
    $tpl->CORCOM  = $corcom ;
    $tpl->ICONCOM = $iconcom;
    
    $tpl->block("BLOCK_LISTAS");    
    
    
    /*  
     *  R E L A T Ó R I O   P O R   E X E R C Í C I O
     */
    
    
    //LINHA DE CADA EXER
    $total = 0;    
    $numexer=1;
    
    $objexers = buscarTodosExerciciosByIdLista($idlstexer);
    
    foreach($objexers as $exer){
            
        //Exibe o Enunciado
        $tpl->EXERID_2 = $exer->id;
        $tpl->EXERTITULOALIAS_2 = "$exer->titulo";
        $tpl->EXERENUNCIADO_2 = $exer->enunciado;                
        
        $tpl->block("BLOCK_ENUNEXER_2");
        
        //Lista de Alunos 
        foreach($objusers as $indx1 => $usu){
            if($usu->perfil != '3'){ // perfil 3 é o 'aluno'
                //pula
                continue;
            }                

            $vetor  = buscarTodasSolucoesUserByExercicioID($exer->id, $usu->id);
            $linhahist = "";

            $qtdtes = 0;
            $qtdinc = 0;
            $qtdcom = 0;

            if($vetor != NULL){
                $vetor = array_reverse($vetor);
                foreach($vetor as $solu){

                    $cor='';$icon='';
                    switch ($solu->estado){
                        case 'emteste1'  : $cor=$cortes; $icon=$icontes; $qtdtes++; break;
                        case 'incompleto': $cor=$corinc; $icon=$iconinc; $qtdinc++; break;
                        case 'completo'  : $cor=$corcom; $icon=$iconcom; $qtdcom++; break;
                    }                

                    $link_mostrasolucao = "$endamb/?page=mostrasolucao&id=$solu->id";

                    $linhahist .= "<a href='$link_mostrasolucao' target='_blank' style='color:$cor;' title='$solu->datacriacao'><i class='fa fa-$icon'></i></a>  ";
                }//foreach                    
            }else{
                $linhahist = "-";            
            }
            
            $tpl->QTDTES_2 = ($qtdtes==0?'-':$qtdtes);
            $tpl->QTDINC_2 = ($qtdinc==0?'-':$qtdinc);
            $tpl->QTDCOM_2 = ($qtdcom==0?'-':$qtdcom);              
            
            $tpl->RLTUSERHISTSOLUCOES_2 = $linhahist;
            
            $tpl->RLTUSERNAME_2 = $usu->nome;
            $tpl->block("BLOCK_RLTUSERLINHA_2");

        }//foreach

        $numexer++;
        $total++;
        
        $tpl->CORTES_2  = $cortes ;
        $tpl->ICONTES_2 = $icontes;
        $tpl->CORINC_2  = $corinc ;
        $tpl->ICONINC_2 = $iconinc;
        $tpl->CORCOM_2  = $corcom ;
        $tpl->ICONCOM_2 = $iconcom;          
        
        $tpl->block("BLOCK_LISTAEXERNUM_2");
    }//foreach  
    
    $tpl->block("BLOCK_LISTAS_2");
    
    
}