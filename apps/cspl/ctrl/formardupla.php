<?php
/*
 * MOSTRAR COMPARAÇÕES
 * 
 * > Página que mostra todos os exercícios Socializados
 */
include_once "$endatual/apps/cspl/agt/agt_comparador.php";
        
$idlista     = (isset($_GET['lst'])) ? $_GET['lst'] :"";
$idexercicio = (isset($_GET['exer']))? $_GET['exer']:41;
   
//Inserindo o conteúdo central da página
$tpl->addFile("CONTEUDO","./apps/cspl/tpl/formardupla.html");


$exercicio = buscarExercicioID($idexercicio);

$tpl->EXERID        = $exercicio->id;
$tpl->EXERTITULO    = $exercicio->titulo;
//$tpl->EXERENUNCIADO = $exercicio->enunciado;    
    

    
    $solucoes = buscarTodasSolucoesByExercicioID($idexercicio);
    $qtdsolu  = count($solucoes);

    foreach ($solucoes as $s){    
        $codigos[] = simplificarCodigo($s->codigohaskell); 
    }
        
    $htmlsaida = "";
    $htmlsaida .=  "<table class='tablecomp2 small' style='border:1px solid #ccc; padding: 6px;'>";
    $htmlsaida .=  "<tr><td class='fw-700'>Alunos</td>";    
    for($i=0; $i<$qtdsolu; $i++){
        $htmlsaida .=  "<th> Aprendiz 0".($i+1)."<br><a href='#' class='small fw-300'>Ver solução</a></td>";    
    }
    $htmlsaida .=  "</tr>";    

    $similaridades = NULL;


    for($i=0; $i<$qtdsolu; $i++){
        $htmlsaida .=  "<tr>";    
        $htmlsaida .=  "<th> Aprendiz 0".($i+1)."<br><a href='#' class='small fw-300'>Ver solução</a></td>";
        for($j=0; $j<$qtdsolu; $j++){
            if($i<=$j){
                $htmlsaida .=  "<td>-</td>";    
                continue;
            }
            $porcentagem = calcularSimilaridade($codigos[$i], $codigos[$j]);
    //        $similaridades[] = "simi(".$i.",".$j.",".$porcentagem.")";
            $similaridades[] = [$porcentagem, $i+1 , $j+1 ];
            $tamfont = 100+($porcentagem/1);
            $htmlsaida .=  "<td class='tdcomp' style='width:50px; font-size:$tamfont%' >";
                        
            $htmlsaida .= number_format($porcentagem,2)."%";            
            
            $htmlsaida .=  "</td>";        
        }    
        $htmlsaida .=  "</tr>";    
    }
    $htmlsaida .=  "</table>";
    $htmlsaida .=  "</div>";
    
    
    $tpl->RESULTADO = $htmlsaida;
    
    $htmlsaida2 = NULL;
    sort($similaridades);
    $similaridades2 = array_reverse($similaridades);
    
    $htmlsaida2 .= "<table class='ink-table hover small alternating bordered'>";
    $htmlsaida2 .= "<tr><th colspan='2'>Alunos</th><th>(%)</th></tr>";
    foreach($similaridades2 as $l){
        $htmlsaida2 .= "<tr><td>Aprendiz 0$l[1] </td><td> Aprendiz 0$l[2] </td><td>".number_format($l[0],2)."% </td></tr>";
    }
    $htmlsaida2 .= "</table>";
            
    $tpl->RESULTADO2 = $htmlsaida2;
            
  
    for($i=0;$i<=$qtdsolu-1;$i++){
        $tpl->ALUNO1NOME = " <i class='fa fa-check'></i> Aprendiz 0".($i+1).""; 
        $tpl->ALUNO1ID   = ($i+1);
        $tpl->block("LINHAALUNO");
        
        for($j=0;$j<=$qtdsolu-1;$j++){
            $simila = -1;
            foreach ($similaridades as $s){
                if( ($s[1]==($i+1) && $s[2]==($j+1)) || ($s[1]==($j+1) && $s[2]==($i+1))){
                   $simila = number_format($s[0],2)."%"; 
                }
            }
            if($simila != -1){
                $tpl->TABALUNO1ID= ($i+1);
                $tpl->ALUNO2NOME = "<i class='fa fa-check'></i> ($simila) Aprendiz 0".($j+1);     
                $tpl->block("LINHAALUNO2");
            }
        }
        $tpl->block("BLOCOALUNO2");        
        
    }
    
      

    
    