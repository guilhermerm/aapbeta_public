<?php

/*
 * LISTA EXERCICIOS
 * 
 * > Página para Listar todos os exercícios de uma lista
 */

//Listas de Exercícios de uma TURMA
$tpl->addFile("CONTEUDO", "./apps/cspl/tpl/mostralistaexer.html");

if (isset($_GET['salvo'])) {
    $tpl->block("BLOCK_MSGSALVO");
}

if (isset($_GET['id'])) {            
    $idlista = $_GET['id'];
    
    if(estaTrancadoParaLista($idlista,$userlogado->perfil)){
        header("Location:  $endamb/?page=trancado");
        exit();
    }
    
    //Informações da LIsta
    $lista = buscarListaDeExercicio($idlista);
    $tpl->LISTATITULO = $lista->titulo;
    $tpl->LISTATEXTO = $lista->texto;
    $tpl->LISTATEXTO2 = $lista->texto2;
    
    if($userlogado->perfil!="3"){
        $tpl->LINKRELATORIOLISTA = "?page=relatoriolistaexer&id=$lista->id";
        $tpl->block("BLOCK_ACOMPANHARLISTA");
    }    

    //Exercícios da lista
    $exercicios = buscarTodosExerciciosByIdLista($idlista);
    if ($exercicios != NULL) {
        $cont = 1;
        foreach ($exercicios as $b) {

            //BUSCAR SOLUCOES PARA ESTE EXERCICIO
            $solus = buscarTodasSolucoesUserByExercicioID($b->id, $userlogado->id);
            if ($solus != NULL) {
                foreach ($solus as $a2 => $b2) {
                    if ($b2->estado == "completo") {

                        $tpl->EXERSOLUCAOTIT     = $b2->id . "-" . $b2->datacriacao;
                        $tpl->IDSOLUCAO          = $b2->id;
                        $tpl->LINK_MOSTRASOLUCAO = "$endamb/?page=mostrasolucao&id=" . $b2->id;                        

                        $tpl->SOCIALIZACAO       = $b2->socializar;
                        $tpl->CLASSSOCIALIZACAO  = ($b2->socializar=='0')?"":"socializado";

                        $tpl->block("BLOCK_SOLUCAOCOMPLETA");
                    } else if ($b2->estado == "incompleto") {
                        $tpl->EXERSOLUCAOTIT = $b2->datacriacao;
                        $tpl->IDSOLUCAO = $b2->id;

                        $tpl->LINK_EDITSOLUCAO = "$endamb/?page=editsolucao&id=" . $b2->id . "&lst=$idlista";
                        $tpl->block("BLOCK_SOLUCAOINCOMPLETA");
                    }
                }//foreach
                $tpl->block("BLOCK_MINHASSOLUCOES");
            }


            $tpl->EXERID = $b->id;
            //$tpl->EXERTITULOALIAS = "Exercício " . $cont++;
            $tpl->EXERTITULOALIAS = $b->titulo;
            $tpl->EXERENUNCIADO = $b->enunciado;
            //$tpl->EXERTITULO = $b->titulo;
            //$tpl->LINK_EXERCICIOID = "$endamb/?page=exercicio&id=".$b->id;
            
            //Verificar "UsarConstrutor"
            if( $lista->usarconstrutor === '1'){
                //Usar Construtor                
                $tpl->block("BLOCK_USARCONSTRUTOR");                
            }
            
            //Verificar "Socialização"         
            //$soc = buscarSocializacaoByIdExer($b->id);
            //$listaexer->socializar;
            $soc = $lista->socializar;
            if( $soc === '1'){
                //Socializar            
                $tpl->block("BLOCK_SOCIALIZAR");                
            }
            

            //Buscar a quantidade de soluções socializadas
            $qntdsolus = contaTodasSolucoesByExercicioID($b->id);
            $qntdusers = contaUsersSolucoesByExercicioID($b->id);
            //echo $b->id," - ",$qntdsolus," <br>";
            //echo $b->id," - ",$qntdusers," <br>";


            $ptotal = "";
            if($qntdsolus > 0){
                if($qntdusers > 0){
                    $ptotal .= "(<i class='fa fa-file-o' title='Total de soluções enviadas: $qntdsolus' ></i> $qntdsolus) (<i class='fa fa-user' title='Total de alunos que enviaram soluções: $qntdusers' ></i> $qntdusers)";
                }else{
                    $ptotal .= "(<i class='fa fa-file-o' title='Total de soluções enviadas: $qntdsolus' ></i> $qntdsolus)";                                
                }
            }
            $tpl->TOTALSOCIALIZACOES = "";//$ptotal;


            $tpl->LINK_ENVIARSOLUCAO = "$endamb/?page=novasolucao&exer=$b->id&lst=$idlista";
            $tpl->LINK_SOCIALIZACAO  = "$endamb/?page=socializacao&exer=$b->id&lst=$idlista";                
            
            if($userlogado->perfil!="3"){
                $tpl->LINK_RELATORIOEXER = "$endamb/?page=relatorioexer&id=$b->id";
                $tpl->block("BLOCK_ACOMPANHAR");                                
            }
            $tpl->block("BLOCK_EXERCICIO");
        }
    }
} else {

    //TODO: está errado, acertar esta mensagem abaixo
    $tpl->block("BLOCK_MSGSALVO");
}
