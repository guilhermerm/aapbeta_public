<?php
/*
 * MOSTRA SOCIALIZACAO
 * 
 * > Página que mostra todos os exercícios Socializados
 */
        
$idlista     = (isset($_GET['lst'])) ? $_GET['lst'] :"";
$idexercicio = (isset($_GET['exer']))? $_GET['exer']:"";
   
if(estaTrancadoParaLista($idlista,$userlogado->perfil)){
    header("Location:  $endamb/?page=trancado");
    exit();
}else{

    //Inserindo o conteúdo central da página
    $tpl->addFile("CONTEUDO","./apps/cspl/tpl/socializacao.html");

    //BUSCAR O LISTA DE EXERCÍCIO
    $listaexer = buscarListaDeExercicio($idlista);
    //BUSCAR O EXERCÍCIO
    $exercicio = buscarExercicioID($idexercicio);

    $tpl->EXERID        = $exercicio->id;
    $tpl->EXERTITULO    = $exercicio->titulo;
    $tpl->EXERENUNCIADO = $exercicio->enunciado;

    //Buscar as configurações para esta socialização

    //Verificar se é para socializar
    //$solusocializar = buscarSocializacaoByExercicioID($idexercicio);
    $solusocializar = $listaexer->socializar;
    $socializarpara = $listaexer->para;

    //Verificar se user já enviou ao menos uma solução para acessar a socialização
    $qtdusersolucoes= contaUserSolucoesByExercicioID($idexercicio,$userlogado->id);

    //É para Socializar?
    if($solusocializar == '1' || $userlogado->perfil!==3){
    //if(TRUE){

        //Restrição de USER
        //Se não-ALUNO OR Se para TODOS OR para Somente quem Envio uma solução?
        if($userlogado->perfil!="3" || $socializarpara=="0" || ($socializarpara=="1" && $qtdusersolucoes > 0)){
        //if(TRUE){

            //BUSCAR AS SOLUCOES
            $solus = buscarTodasSolucoesByExercicioID($idexercicio);
            //$solus = buscarTodasSolucoesSocializadasByExercicioID($idexercicio);

            $qntdsolus = contaTodasSolucoesByExercicioID($idexercicio);


            if ($solus != NULL) {
                foreach ($solus as $s) {

                    //BUSCAR OS COMENTARIOS DE UMA SOLUCAO                
                    $coments = buscarComentariosByIDSolu($s->id);
                    //var_dump($coments);

                    if ($coments != NULL) {
                        foreach ($coments as $c) {
                            $tpl->COMENTARIOUSER = $c->usernome;
                            $tpl->COMENTARIO = $c->texto;
                            $tpl->block("BLOCK_COMENTARIO");
                        }
                    }

                    $tpl->USERSOLUCAOID = $s->id;
                    //$tpl->USERCOMPREENSAO = $s->compreensao;
                    $tpl->USERCODIGOHASKELL = $s->codigohaskell;
                    //$tpl->USERSOLUCAOTEXTO = $s->solucao;
                    $tpl->USERSOLUCAONOME = $s->usernome;
                    $tpl->USERLOGADONOME = $userlogado->nome;
                                        
                    
                        $idplanoteste = buscarIDPlanoByIDSolu($s->id);
                        $testes = buscarTodosSoluTestes($idplanoteste);

                        //PREENCHER OS TESTES
                        if ($testes != NULL) {
                            foreach ($testes as $c) {
                                $tpl->TESTEDESCRICAO = $c->descricao;
                                $tpl->TESTEENTRADA = $c->entrada;
                                $tpl->TESTESAIDAES = $c->saidaesperada;
                                $tpl->block("BLOCK_TESTES");
                            }
                            $tpl->block("BLOCK_BOXTESTES");
                        }                    
                        
                        //Resutlado dos Testes
                        $soluresultado = buscarSoluResuladoByIDPlano($idplanoteste);

                        if ($soluresultado != NULL) {
                            $htmlsaidahugs = $soluresultado->saidahugs;
                            $htmltableresultados = $soluresultado->saidaresultado;
                            $tpl->RESULTTABLES = $htmltableresultados;
                            //$tpl->RESULTHUGS = $htmlsaidahugs;
                            $tpl->block("BLOCK_RESULTADOSOBTIDOS");
                        }

                    $tpl->LINK_EDITARSOLUCAO = "$endamb/?page=editsolucao&id=$s->id";
                    $tpl->LINK_MOSTRASOLUCAO = "$endamb/?page=mostrasolucao&id=".$s->id;

                    $tpl->block("BLOCK_SOLUCAO");
                }
            }else{            
                $tpl->block("BLOCK_SEMSOLUCAO");    
            }
        }else{
            $tpl->block("BLOCK_USERSEMSOLUCAO");    
        }
        if($userlogado->perfil!==3){
            //Informar que socialização está fechada para os alunos
            $tpl->block("BLOCK_SOCIALIZACAOFECHADA");
        }
    }else{    
        $tpl->block("BLOCK_NAOSOCIALIZAR");
    }
    //LINKS DA PÀGINA
    $tpl->LINK_VOLTARLISTEXER = "$endamb/?page=mostralistaexer&id=".$idlista;

}