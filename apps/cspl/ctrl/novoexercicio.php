<?php
/*
 * NOVO EXERCÍCIO
 * 
 * > Formulário para um novo Exercício
 */
 if (isset($_POST['formexercicio'])) {

    $exercicio = new exercicio();

    $exercicio->titulo = addslashes($_POST['exertitulo']);
    $exercicio->enunciado = addslashes($_POST['mytextarea1']);
    $exercicio->interface = addslashes($_POST['interface']);
    $exercicio->validarteste = addslashes($_POST['validarteste']);
    $exercicio->mostraplano = (isset($_POST['mostraplano'])) ? 1 : 0;
    $exercicio->tiposolucao = addslashes($_POST['tiposolucao']);
    $exercicio->nivel = addslashes($_POST['nivel']);
    $exercicio->deletado = 0;

    $idexergerado = inserirExercicio($exercicio);

    $exerplano = new exerplanoteste();
    $exerplano->idexercicio = $idexergerado;
    $exerplano->datacriacao = date('Y-m-d H:i:s');
    $exerplano->deletado = 0;

    $idplanotestegerado = inserirExerPlano($exerplano);

    $exertestes = array();

    //ORGANIZANDO OS TESTES NO VETOR DE TESTES
    $temtestes = false;
    foreach ($_POST as $a => $b) {

        if (trim(substr($a, 0, 5)) === "descr") {

            $indice = trim(substr($a, 9));

            $exteste = new exerteste();

            $exteste->idplanoteste = $idplanotestegerado;
            $exteste->descricao = addslashes(trim($b));
            $exteste->entrada = addslashes(trim($_POST["entrada$indice"]));
            $exteste->saidaesperada = addslashes(trim($_POST["saidaes$indice"]));
            $exteste->deletado = 0;

            $exertestes[] = $exteste;
            $temtestes = true;
        }
    }
    if ($temtestes) {
        inserirExerTeste($exertestes);
    }

    header("Location: $endamb/?page=baseexercicio&salvo");
    exit();
}
//EXIBI PÁGINA Normalmente
//Inserindo o conteúdo central da página
$tpl->addFile("CONTEUDO", "./apps/cspl/tpl/formexercicio.html");

$tpl->TITULOFORMEXER = "Nova Atividade";

//EXIBINDO CAMPOS EM BRANCO
$tpl->EXERID = "";
$tpl->EXERTITULO = "";
$tpl->EXERENUNCIADO = "";
$tpl->EXERINTERFACE = "";
$tpl->EXERVALTESTE = "saidaesperada == resultadoobtido";
$tpl->TESTEQTD = "0";

$tpl->TIPOMCP = "checked='checked'";
$tpl->TIPOTEXTO = "";

//Escrevendo os LInks
$tpl->LINK_FORMACTION = "?page=$page";
$tpl->LINK_CANCELAR = "?page=inicio";
