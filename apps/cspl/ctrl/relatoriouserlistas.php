<?php
/*
 * MOSTRAR Relatório de um Usuário e as Listas de Exercícios
 * 
 */

//Acompanhamento de um Usuário
$tpl->addFile("CONTEUDO","./apps/cspl/tpl/relatoriouserlistas.html");

if(isset($_GET['salvo'])){
    $tpl->block("BLOCK_MSGSALVO");
}

if(isset($_GET['id'])){
    $idaluno = $_GET['id'];
}

//Preenchendo o cabeçalho com os alunos
$objalunos = buscarTodosUsuariosAluno();
foreach ($objalunos as $alu){
    if($idaluno == $alu->id){
        $tpl->LISTAALUNOATUAL = $alu->nome;
        $tpl->TITNOMEALUNO    = $alu->nome;
    }
    $tpl->LINK_ALUNOS = "$endamb/?page=relatoriouserlistas&id=$alu->id";
    $tpl->LISTAALUNONOME  = $alu->nome;

    $tpl->block("BLOCK_LINKSALUNOS");    
}

//Buscando todas as listas de exercícios
$objlistas = buscarTodasListas();

//BUSCAR Lista dos Alunos
$objusers = buscarTodosUsuarios();

$cortes ='#656565';
$icontes='cog'; 
$corinc ='#996600';
$iconinc='save';
$corcom ='#004d99';
$iconcom='save';

if($objlistas != NULL){                     
    
    /*  
     * R E L A T Ó R I O   D E   U M   A L U N O     
     */
    
    //LINHA DE CADA LISTA
    $total = 0;
    foreach($objlistas as $lst){            
        
        $numexer = 1;
        
        $tpl->RLTLISTATITULO = "<i class='fa fa-user'></i> ".$lst->titulo;
        $tpl->block("BLOCK_LISTATITULO");
        
        //Buscar os exerícios da LISTA
        $objexers = buscarTodosExerciciosByIdLista($lst->id);
        //$qtdexers  = count($objexers);

        //em cada exercício
        foreach($objexers as $exer){                        
            
            //Buscar as soluções do aluno para este exercício
            $vetor  = buscarTodasSolucoesUserByExercicioID($exer->id,$idaluno);
            $linhahist = "";

            $qtdtes = 0; //solução "emmteste1"
            $qtdinc = 0; //solução "incompleto" 
            $qtdcom = 0; //solução "completao"

            //Se houverem soluções
            if($vetor != NULL){
                //Inverter vetor para iterar em ordem crescente
                $vetor = array_reverse($vetor);
                foreach($vetor as $solu){
                    $cor='';$icon='';
                    switch ($solu->estado){
                        case 'emteste1'  : $cor=$cortes; $icon=$icontes; $qtdtes++; break;
                        case 'incompleto': $cor=$corinc; $icon=$iconinc; $qtdinc++; break;
                        case 'completo'  : $cor=$corcom; $icon=$iconcom; $qtdcom++; break;
                    }                
                    $link_mostrasolucao = "$endamb/?page=mostrasolucao&id=$solu->id";
                    $linhahist .= "<a href='$link_mostrasolucao' target='_blank' style='color:$cor;' title='$solu->datacriacao'><i class='fa fa-$icon'></i></a>  ";
                }//foreach                    
            }else{
                $linhahist = "-";            
            }

            $tpl->EXERNUM = $numexer;
            $numexer++;
            
            $tpl->QTDTES = ($qtdtes==0?'-':$qtdtes);
            $tpl->QTDINC = ($qtdinc==0?'-':$qtdinc);
            $tpl->QTDCOM = ($qtdcom==0?'-':$qtdcom);                                    
            
            $tpl->RLTUSERHISTSOLUCOES = $linhahist;
            $tpl->block("BLOCK_RLTUSEREXER");
            
            $total++;
        }//foreach                
        
        $tpl->block("BLOCK_RLTUSERLINHA");
    
    }//foreach
    
    $tpl->CORTES  = $cortes ;
    $tpl->ICONTES = $icontes;
    $tpl->CORINC  = $corinc ;
    $tpl->ICONINC = $iconinc;
    $tpl->CORCOM  = $corcom ;
    $tpl->ICONCOM = $iconcom;
    
    $tpl->block("BLOCK_LISTAS");    
       
}