<?php
/*
 * NOVA SOLUCAO
 * 
 * > Formulário para um nova solução
 */
   
$idlista = (isset($_GET['lst'])) ? $_GET['lst'] : "";
$idexercicio = (isset($_GET['exer'])) ? $_GET['exer'] : "";

//Inserindo o conteúdo central da página
$tpl->addFile("CONTEUDO", "./apps/cspl/tpl/formsolucao.html");

//TODO ID do Exercicio!!!!!!!!
$exercicio = buscarExercicioID($idexercicio);
$tpl->IDEXERCICIO = "$exercicio->id";
$tpl->TITULOEXERCICIO = "$exercicio->titulo";
$tpl->ENUNCIADOEXERCI = "$exercicio->enunciado";

$tpl->QTDTESTES = 0;

//            var_dump($_POST);

if (isset($_POST['formnovasolucao'])) {

    //SALVAR DADOS RECEBIDOS               

    $solucao = new solucao();

    $solucao->iduser          = $iduser;
    $solucao->idexercicio     = addslashes(trim($_POST['idexercicio']));
    $solucao->titulo          = addslashes(trim($_POST['exertitulo']));
    $solucao->compreensao     = addslashes(trim($_POST['mytextarea1']));
    $solucao->solucao         = addslashes(trim($_POST['mytextarea2']));
    $solucao->codigohaskell   = addslashes(trim($_POST['codigohaskell']));
    $solucao->funcaoprincipal = addslashes(trim($_POST['funcaoprincipal']));
    $solucao->estado          = addslashes($_POST['estado']);
    $solucao->deletado        = 0;

    $idsolugerado = inserirSolucao($solucao);

    $soluplano = new soluplanoteste();
    $soluplano->idsolucao = $idsolugerado;
    $soluplano->datacriacao = date('Y-m-d H:i:s');
    $soluplano->deletado = 0;

    $idplanotestegerado = inserirSoluPlano($soluplano);

    $solutestes = array();

    //ORGANIZANDO OS TESTES NO VETOR DE TESTES
    foreach ($_POST as $a => $b) {

        if (trim(substr($a, 0, 5)) === "descr") {

            $indice = trim(substr($a, 9));

            $soluteste = new soluteste();

            $soluteste->idplanoteste = $idplanotestegerado;
            $soluteste->descricao = addslashes(trim($b));
            $soluteste->entrada = addslashes(trim($_POST["entrada$indice"]));
            $soluteste->saidaesperada = addslashes(trim($_POST["saidaes$indice"]));
            $soluteste->deletado = 0;

            $solutestes[] = $soluteste;
        }
    }
    inserirSoluTeste($solutestes);

    header("Location: $endamb/?page=mostrasolucao&id=$idsolugerado");
} else {
    //EXIBIR PÁGINA
}
//Escrevendo os LInks
$tpl->LINK_FORMACTION = "$endamb/?page=$page&exer=$idexercicio";
$tpl->LINK_CANCELAR = "$endamb/?page=mostralistaexer&id=" . $idlista;
