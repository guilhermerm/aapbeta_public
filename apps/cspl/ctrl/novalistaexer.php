<?php
/*
 * NOVA LISTA DE EXERCÍCIO
 * 
 * > Formulário para uma nova lista de exercício
 */

        //Inserindo o conteúdo central da página
$tpl->addFile("CONTEUDO", "./apps/cspl/tpl/formlista.html");

if (isset($_POST['formlista'])) {

    $tempo = date('Y-m-d H:i:s');
    
    $hora  = ($_POST['publicacaohora']!="")? $_POST['publicacaohora'].":00": "00:00:00";
    $datapublicacao = inverteData($_POST['publicacaodata'])." ".$hora;

    //SALVAR DADOS RECEBIDOS                                               
    $lstexer = new listadeexercicio();

    $lstexer->titulo           = addslashes($_POST['titulo']);
    $lstexer->texto            = addslashes(trim($_POST['mytextarea1']));
    $lstexer->prazoenvio       = $tempo;//addslashes($_POST['prazoenvio']);
    $lstexer->permitirenvio    = 1; //(isset($_POST['permitirenvio']))?1:0;
    $lstexer->oculto           = (isset($_POST['publicacao'])) ? $_POST['publicacao'] : 2;
    $lstexer->datapublicacao   = $datapublicacao;    
    $lstexer->usarconstrutor   = (isset($_POST['usarconstrutor'])) ? 1 : 0;
    $lstexer->socializar       = $_POST['socializar'];
    $lstexer->para             = $_POST['socipara'];
    $lstexer->datasocializacao = $tempo;
    $lstexer->datacriacao      = $tempo;
    $lstexer->deletado         = 0;
    
    
    
    $socializar = $_POST['socializar'];
    $para       = $_POST['socipara'];
    
    $idlstgerado = inserirListaDeExercicio($lstexer);

    $objsocializar = new socializacao();
    //PEGAR DO $_POST OS ID DOS EXERCÍCIOS
    foreach ($_POST as $campo => $valor) {
        if ("exercheck" == substr($campo, 0, 9)) {
            //Relacionar Exercício e Lista de Exercícios
            relacionarListaExercicio($valor, $idlstgerado);
            //Inserir as Configurações para a Socialização
            $objsocializar->prepararSocializacao($idlstgerado,$valor,$socializar,$para);
            inserirSocializacao($objsocializar);
        }
    }
    header("Location: $endamb/?page=mostralistas&salvo");
    exit();
}
//EXIBIR PÁGINA            
$exers = buscarTodosExercicios();
$tpl->FORMTITULO = "Nova Arquitetura Pedagógica";

foreach ($exers as $e) {
    $tpl->BASEEXERNOME = $e->titulo;
    $tpl->BASEEXERID   = $e->id;
    $tpl->BASEEXERCHECK= "";
    //$tpl->BASEEXERENUNCIADO = $e->enunciado;
    $tpl->block("BLOCK_LIEXERCICIO");
}

$tpl->PU0CK = "checked='checked'";
$tpl->PU1CK = "";
$tpl->PU2CK = "";

//$tpl->MOSTRAPLANOCHECK = "";
$tpl->S0CK = "checked='checked'";
$tpl->S1CK = "";
$tpl->P0CK = "checked='checked'";
$tpl->P1CK = "";

$tpl->CKCONSTRUTOR  = "checked='checked'";

$tpl->LINK_FORMACTION = "?page=$page";
$tpl->LINK_CANCELAR = "?page=inicio";
