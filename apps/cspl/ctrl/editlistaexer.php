<?php
/*
 * EDITAR EXERCÍCIO
 * 
 * > Formulário para editar um exercício
 */
    
//Inserindo o conteúdo central da página
$tpl->addFile("CONTEUDO", "./apps/cspl/tpl/formlista.html");


if (isset($_POST['formlista'])) {

    $tempo = date('Y-m-d H:i:s');
    $hora  = ($_POST['publicacaohora']!="")? $_POST['publicacaohora'].":00": "00:00:00";
    $datapublicacao = inverteData($_POST['publicacaodata'])." ".$hora;

    //SALVAR DADOS RECEBIDOS                                               
    $lstexer = new listadeexercicio();

    $lstexer->id               = addslashes($_POST['idlista']);
    $lstexer->titulo           = addslashes($_POST['titulo']);
    $lstexer->texto            = addslashes(trim($_POST['mytextarea1']));
    $lstexer->texto2           = addslashes(trim($_POST['mytextarea2']));
    $lstexer->prazoenvio       = "";//addslashes($_POST['prazoenvio']);
    $lstexer->permitirenvio    = 1; //(isset($_POST['permitirenvio']))?1:0;
    $lstexer->oculto           = (isset($_POST['publicacao'])) ? $_POST['publicacao'] : 2;
    $lstexer->datapublicacao   = $datapublicacao;
    $lstexer->usarconstrutor   = (isset($_POST['usarconstrutor'])) ? 1 : 0;
    $lstexer->socializar       = $_POST['socializar'];
    $lstexer->para             = $_POST['socipara'];
    $lstexer->datasocializacao = $tempo;
    $lstexer->deletado         = 0;
    
//    echo "<pre>";
//    var_dump($_POST);
//    echo "-------------------------------------";
//    var_dump($lstexer);
//    echo "</pre>";
//    die("ops");

    atualizarListaDeExercicio($lstexer);
    
    $idsocializar = (isset($_POST['idsocializar'])) ? $_POST['idsocializar'] : 0 ;
    $socializar   = $_POST['socializar'];
    $para         = $_POST['socipara'];

    apagarRelacoesListaExercicio($lstexer->id);
    apagarSocializacoes($lstexer->id);
    //PEGAR DO $_POST OS ID DOS EXERCÍCIOS
    $objsocializar = new socializacao();
    foreach ($_POST as $campo => $valor) {
        if ("exercheck" == substr($campo, 0, 9)) {
            //Relacionar Exercício e Lista de Exercícios
            relacionarListaExercicio($valor, $lstexer->id);
            //Salvar configurações da socialização
            $objsocializar = new socializacao();
            //$objsocializar->id = $idsocializar;
            $objsocializar->prepararSocializacao($lstexer->id,$valor,$socializar,$para);

            //if(existeSocializacaoNoBanco($objsocializar)){                
            //    atualizarSocializacao($objsocializar);                            
            //}else{
                inserirSocializacao($objsocializar);
            //}
                
        }
    }
    header("Location: $endamb/?page=mostralistas&salvo");
    exit();    
    

} else {
    //EXIBIR PÁGINA DE EDIÇÃO
    
    $tpl->FORMTITULO = "Editar AP";

    if (isset($_GET['id'])) {
        $idlst = $_GET['id'];

        //Buscar do Banco as Informações
        $listaexer = buscarListaDeExercicio($idlst);
                
        //$checkeds  = buscarTodosExerciciosByIdLista($idlst); //Melhorar esta função, criar outra para retornar apenas os Ids
        $checkeds  = buscarTodosIDExerciciosByIdLista($idlst);
        $todosexer = buscarTodosExercicios();                
        
        //Socialização
        $socializar= buscarTodasSocializacaoByIdLista($idlst);
        
        $exers = buscarTodosExercicios();
        foreach ($exers as $e) {
            $tpl->BASEEXERNOME = $e->titulo;
            $tpl->BASEEXERID   = $e->id;
            
            $marcar = FALSE;
            if($checkeds != NULL){
                foreach($checkeds as $b){
                    if($e->id == $b){
                        $marcar = TRUE;
                        break;
                    }
                }     
            }
            $tpl->BASEEXERCHECK= $marcar ? "checked='checked'":"";
            
            //$tpl->BASEEXERENUNCIADO = $e->enunciado;
            $tpl->block("BLOCK_LIEXERCICIO");
            
            
            
            $tpl->BASEEXERENUNCIADO = $e->enunciado;            
            $tpl->block("BLOCK_EXERCICIOENUM");
            
        }
        
        $datapub = inverteData(pegaData($listaexer->datapublicacao));
                
        $tpl->LISTAID        = $listaexer->id;
        $tpl->LISTATITULO    = $listaexer->titulo;
        $tpl->TXTOBJETIVO    = $listaexer->texto;
        $tpl->TXTMETODO      = $listaexer->texto2;
        $tpl->DATAPUBLICACAO = $datapub;
        $tpl->INKSTARTDATAPU = "data-start-date=\"$datapub\"";
        $tpl->HORAPUBLICACAO = pegaHora($listaexer->datapublicacao);
        
        $tpl->CKCONSTRUTOR   = $listaexer->usarconstrutor=="1" ? "checked='checked'":"";
        
        $tpl->PU0CK          = $listaexer->oculto =="0" ? "checked='checked'":"";
        $tpl->PU1CK          = $listaexer->oculto =="1" ? "checked='checked'":"";
        $tpl->PU2CK          = $listaexer->oculto =="2" ? "checked='checked'":"";
        
        //SOCIALIZAR (checkbox's)        
        //$tpl->SOCIALIZARID  = $socializar[0]->id;
//        $tpl->S0CK          = $socializar[0]->socializar=="0" ? "checked='checked'":"";
//        $tpl->S1CK          = $socializar[0]->socializar=="1" ? "checked='checked'":"";
//        $tpl->P0CK          = $socializar[0]->para =="0" ? "checked='checked'":"";
//        $tpl->P1CK          = $socializar[0]->para =="1" ? "checked='checked'":"";
        $tpl->S0CK          = $listaexer->socializar=="0" ? "checked='checked'":"";
        $tpl->S1CK          = $listaexer->socializar=="1" ? "checked='checked'":"";
        $tpl->P0CK          = $listaexer->para =="0" ? "checked='checked'":"";
        $tpl->P1CK          = $listaexer->para =="1" ? "checked='checked'":"";        

        
                       
        //Escrevendo os LInks
        $tpl->LINKFORMARDUPLAS = "?page=formardupla";
        
        $tpl->LINK_FORMACTION = "?page=$page&id=$idlst";
        $tpl->LINK_CANCELAR = "?page=inicio";
    }
}
        
