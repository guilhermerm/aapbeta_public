<?php
/*
 * MOSTRA LISTAS
 * 
 * > Página para Listar todas as listas de exercícios
 */

//Listas de Exercícios de uma TURMA
$tpl->addFile("CONTEUDO","./apps/cspl/tpl/mostralistas.html");            

if(isset($_GET['salvo'])){
    $tpl->block("BLOCK_MSGSALVO");
}

$objlistas = buscarTodasListas();
if($objlistas != NULL){
    foreach($objlistas as $a => $b){
        
        //TODO
        $tpl->EXERID     = $b->id;
        $tpl->EXERTITULO = ($b->oculto!='2')?"<i class='fa fa-eye'></i> $b->titulo":"<i class='fa fa-eye-slash'></i> $b->titulo";                
        $tpl->LINK_MOSTRALISTA        = "$endamb/?page=mostralistaexer&id=".$b->id;
        $tpl->LINK_EDITARLISTA        = "$endamb/?page=editlistaexer&id=$b->id";
        $tpl->LINK_NOVALSTEXER        = "$endamb/?page=novalistaexer";
        $tpl->LINK_RELATORIOLISTAS    = "$endamb/?page=relatoriolistas";
        $tpl->LINK_RELATORIOLISTAEXER = "$endamb/?page=relatoriolistaexer&id=$b->id";
        $tpl->block("BLOCK_LISTAS");
    }
}  
