<?php
/*
 * NOVA LISTA DE EXERCÍCIO
 * 
 * > Lista a base de exercício
 */
    
    //Inserindo o conteúdo central da página
    $tpl->addFile("CONTEUDO","./apps/cspl/tpl/baseexercicio.html");

    $tpl->LINK_NOVOEXER = "?page=novoexercicio";

    if(isset($_GET['salvo'])){
        $tpl->block("BLOCK_MSGSALVO");
    }

    $objexercicios = buscarTodosExercicios();

    if($objexercicios!=NULL){
        $mes = 0;
        $ano = 0;
        $a=0;
        foreach($objexercicios as $b){
            if($ano !== getAnoData($b->datacriacao) || $mes !== getMesData($b->datacriacao) ){
                $ano = getAnoData($b->datacriacao);
                $mes = getMesData($b->datacriacao);
                $tpl->DATATIT = getNomeMes($mes)." ($ano)";
                $tpl->block("BLOCK_DATATIT");
            }            
            $tpl->EXERID     = $b->id;
            //$tpl->EXERTITULO = "$b->titulo ($b->datacriacao) $ano - $mes ($a)";
            $tpl->EXERTITULO = "$b->titulo";
            $tpl->LINK_EXERCICIOID = "$endamb/?page=exercicio&id=".$b->id;
            $tpl->LINK_EXEREDITAR  = "$endamb/?page=editexercicio&id=".$b->id;
            $tpl->block("BLOCK_EXERCICIOLINK");
            $a++;
        }
        $tpl->block("BLOCK_EXERCICIO");
    } 
    
    
function getMesData($data){
    $mes = substr($data, 5,2);
    return $mes;
}
function getNomeMes($mes){
    $meses = ["Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"];
    return $meses[$mes-1];
}
function getAnoData($data){
    $ano = substr($data, 0,4);
    return $ano;
}