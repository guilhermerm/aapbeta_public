<?php
    
    //Adicionando o conteudo
    $tpl->addFile("CONTEUDO","./apps/ambiente/tpl/inicio.html");
    $tpl->OLA_USER = $userlogado->nome;
    $tpl->LINK_CONFIGUSUARIO = "?page=config";
    
    //Somente Administradores    
    if($userlogado->perfil<=2){
        $tpl->LINK_BASEEXER     = "?page=baseexercicio";
        $tpl->LINK_NOVOEXER     = "?page=novoexercicio";
        $tpl->LINK_LISTEXER     = "?page=mostralistas";
        $tpl->LINK_NOVALIST     = "?page=novalistaexer";
        $tpl->LINK_LISTPROJETO  = "?page=mostraprojetos";
        $tpl->LINK_NOVOPROJETO  = "?page=novoprojeto";        
        $tpl->LINK_LISTAUSUARIO = "?page=listusuario";
        $tpl->LINK_TRANCARAMB = "?page=configtranca";
        
        
        $tpl->block("BLOCK_ADMINISTRATIVO");
    }
    
    //Buscar as Listas de Exercícios 
    if($userlogado->perfil == '3'){
        //Aluno
        $objlistas = buscarTodasListas("0");        
    }else{
        //Admin ou professor
        $objlistas = buscarTodasListas();        
    }
    if($objlistas != NULL){
        foreach($objlistas as $a => $b){            
            //TODO
            $tpl->EXERID     = $b->id;
            $tpl->EXERTITULO = ($b->oculto!='2')?$b->titulo:"<i class='fa fa-eye-slash'></i> ".$b->titulo;
            $tpl->LINK_MOSTRALISTA = "$endamb/?page=mostralistaexer&id=".$b->id;
            $tpl->block("BLOCK_LISTAS");
        }
    }  
    
    //Buscar os Projetos 
    $objprojetos = buscarTodosProjetos();
    if($objprojetos != NULL){
        foreach($objprojetos as $a => $b){
            //TODO
            $tpl->PROJETOID     = $b->id;
            $tpl->PROJETOTITULO = $b->titulo;                
            $tpl->LINK_MOSTRAPROJETO = "$endamb/?page=mostraprojeto&id=".$b->id;
            $tpl->block("BLOCK_PROJETOS");
        }
    }          
