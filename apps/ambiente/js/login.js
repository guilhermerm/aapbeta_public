//var $jD = 
var $jQ = jQuery.noConflict();

jQuery(document).ready(function($) {

    /*
     *  Eventos de botões no formulário de LOGIN
     */

    //Botão "Esqueci minha senha"
    $('#btnesquecisenha').click(function(){
        $("#controleformentrar").hide();
        $("#controleformesqueci").show();
        $("#camposenha").slideUp("fast");
        $("#email").focus();        
    });
    
    //Botão "Cancelar" solicitação de nova senha
    $('#btnlogincancelar').click(function(){
        $("#controleformentrar").show();
        $("#controleformesqueci").hide();
        $("#camposenha").slideDown("fast");
        $("#senha").focus();        
    });


    //Botão "Voltar" após e-mail ser enviado
    // volta para o form de login
    $('#btnvoltar').click(function(){
        $("#camposenha").show();
        $("#controleformentrar").show();
        $("#controleformesqueci").hide();        
        $("#boxsucesso").slideUp("fast");
        $("#boxformlogin").slideDown("fast");
        $("#senha").focus();        
    });
    //Botão "Voltar" após erro ao enviar email
    // volta para o form de login
    $('#btnvoltar2').click(function(){
        $("#camposenha").show();
        $("#controleformentrar").show();
        $("#controleformesqueci").hide();        
        $("#boxerro").slideUp("fast");
        $("#boxformlogin").slideDown("fast");
        $("#senha").focus();        
    });

    //Botão solicitar senha, enviar e-mail
    $('#btnsolicitarsenha').click(function(){
        var email = $("#email").val();
        if(email == ""){
            alert("Informe o seu e-mail cadastrado");
            $("#email").focus();
        }else{
            
            //Carregando
            $("#boxformlogin").slideUp("fast");
            $("#boxloading").slideDown("fast");
            
            //Enviando e-mail (ajax)
            $.post('apost.php',{
            dataType : "json",
            op:"solicitarnovasenha",
            email: email
            },function(r){
                alert(r);
                if(r=="TRUE"){                
                    $("#boxloading").slideUp("fast");           
                    $("#boxsucesso").slideDown("fast");
                }else{               
                    $("#boxloading").slideUp("fast");           
                    $("#boxerro").slideDown("fast");
                }
            }).fail(function(r) {
                linhasolucao.css("color",cor);
            }); 
            
           
        }//ifelse
       
        
    });
    
});