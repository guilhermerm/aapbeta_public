<?php
/* Segurança das páginas para usuário LOGADO
 */
if(isset($_SESSION["aeapuser"])){
    //Reconhecimento do Link da página    
    $userlogado = $_SESSION['aeapuser'];

    //File: TOPOBARRA
    $tpl->LINK_INICIO = "index.php";
    $tpl->LINK_USER   = "$endamb/?page=config";
    $tpl->LINK_SAIR   = "sair.php";
    $tpl->USER_NOME   = $userlogado->nome;
    $tpl->block("BLOCK_LOGADO");
}else{
    $page = 'login';   
}
