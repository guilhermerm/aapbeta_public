<?php
echo '<meta http-equiv="content-type" content="text/html;charset=utf-8" />';

$endatual = getcwd(); 
require_once "$endatual/apps/cspl/mdl/solucao.php";
require_once "$endatual/apps/cspl/mdl/solucao_bd.php";

echo "<style> pre{padding:0;margin:0; !important;} 
              table,td,tr{border: 1px solid black;}
              td{width:15px; height:12px;}
      </style>";

echo "<div class='ink-grid vertical-space divconteudo' style='border: 1px solid #ccc; padding: 10px'> <h3>Avaliação !</h3>";


$solucoes = buscarTodasSolucoesByExercicioID(41);
$qtdsolu  = count($solucoes);

//Se Impar, retirar o último
if($qtdsolu % 2 == 0){
    $solucoes = array_slice($solucoes, 0,$qtdsolu-1);
}

//Padronizar o código
foreach ($solucoes as $s){    
    $codigos[] = simplificarCodigo($s->codigohaskell); 
}

echo "<h2>Resultado:<h2>";
echo "<table>";
echo "<tr><td>-</td>";    
for($i=0; $i<$qtdsolu; $i++){
    echo "<td>",$i+1,"</td>";    
}
echo "</tr>";    

$similaridades = NULL;

for($i=0; $i<$qtdsolu; $i++){
    echo "<tr>";    
    echo "<td>", $i+1,"</td>";    
    for($j=0; $j<$qtdsolu; $j++){
        if($i<=$j){
            echo "<td>-</td>";    
            continue;
        }
        echo "<td>";
        $porcentagem = calcularSimilaridade($codigos[$i], $codigos[$j]);
//        $similaridades[] = "simi(".$i.",".$j.",".$porcentagem.")";
        $similaridades[] = [$i+1 , $j+1 , $porcentagem ];
        printf("%3.2f", $porcentagem);
        echo "</td>";        
    }    
    echo "</tr>";    
}
echo "</table>";
echo "</div>";

echo "<h2>Duplas:</h2><br>";


for($i=0; $i<$qtdsolu; $i++){
    echo "<tr>";    
    echo "<td>", $i+1,"</td>";    
    for($j=0; $j<$qtdsolu; $j++){
        if($i<=$j){
            echo "<td>-</td>";    
            continue;
        }
    }    
    echo "</tr>";    
}



/* - - - - - - - - -
 *    Funções Auxiliares para calcular similaridade
 */

function simplificarCodigo($codigohaskell){

        //Realizando identificação
        //Funções
        $nomefuncoes = pegarNomesFuncoesPrim($codigohaskell);

        //Assinaturas
        $assinaturas = pegarAssinFuncoesPrim($codigohaskell);

        //Cada identificador (inclui nome da função)
        foreach($assinaturas as $assin){
            $identfs = pegarIdentifFuncoesPrim($assin);
        }

        //FAZENDO LIMPEZAS
        // Substituindo os identificadores para nomes comuns        
        $novocodigo = $codigohaskell;
        $concatc = 1;
        //Substituindo os nomes das funções por identificadores comuns de função
        foreach($nomefuncoes as $fun){
            $novocodigo = preg_replace("/\b$fun\b/", "F$concatc", $novocodigo);        
            //$novocodigo = preg_replace("/\b$fun\b/", "", $novocodigo);        
            $concatc++;
        }
        //Substituindo os nomes dos parâmetros por identificadores comuns de função
        foreach($assinaturas as $assin){
            $identfs = pegarIdentifFuncoesPrim($assin);
            foreach($identfs as $id1){
                $novocodigo = preg_replace("/\b$id1\b/", "P$concatc", $novocodigo);             
                //$novocodigo = preg_replace("/\b$id1\b/", "", $novocodigo);             
                $concatc++;
            }
        }

        return $novocodigo;    

}

function calcularSimilaridade($codigoSimplesA,$codigoSimplesB){    
    similar_text($codigoSimplesA, $codigoSimplesB,$porcent);
    return $porcent;
}


/*
 *    Funções Auxiliares para limpeza do código
 */
/* - - - - - - - -
 * Retorna todas as Funções (somente nome, SEM DUPLICATAS)
 * return: Array de Strings
 */
function pegarNomesFuncoesPrim($codigo){
    //Quebrando em linhas    
    $re = "/^\w+/im";
    /* ^\w+   <- incio de linha, quaisquer letras e números
     */
    preg_match_all($re, $codigo, $mts);
    return array_unique($mts[0]);
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ASSINATURA - - - 
 * 
 * Retorna todas as ASSINATURAS (assinatura = nome + parâmetros + =/| )
 * return: Array de Strings
 */
function pegarAssinFuncoesPrim($codigo){    
    $re = "/^[^ \t\n\r\f\v]([\w \(\)\,\:\@\[\]\"\'])+([^\|\=])?/im";
    /* ^[^ \t\n\r\f\v]             <- no inicio de linha NAO espaço \t \n \r \f \v
     * ([\w \(\)\,\:\@\[\]\"\'])+  <- assinatura em sí, \w para [A-Za-z0-9] e mais espaços e ou : @ [ ] " ' ( ) ,
     * ([\|\=])?                   <- Retirar terminar com um sinal de = ou um |
     */
    preg_match_all($re, $codigo, $mts);        
    return array_unique($mts[0]);
}

/* - - - - - - - -
 * Retorna Parâmetros de uma Assinatura
 * return: String
 */
function pegarParamFuncoesPrim($assinatura){
    $re = "/^\w+[^\(\[\@]/im";
    /* ^\w+   <-1 ou mais caracteres no inicio de linha
     * [^\(\[\@] <- e terminar qnd encontrar "[" "(" ou "@"
     */
    $mts = preg_replace($re, "", $assinatura);
    return $mts;
}
/* - - - - - - - -
 * Retorna Nome da função de uma Assinatura
 * return: String
 */
function pegarNomeFuncoesPrim($assinatura){
    $mts= explode(" ", $assinatura,2);    
    return $mts[0];
}

/* - - - - - - - -
 * Retorna os Identificadores de uma Assinatura
 *    (excluindo o nome função!)
 * return: String
 */
function pegarIdentifFuncoesPrim($assinatura){
    $re = "/[\w]+/im";
    /* [\w]+   <- casa com palavras e _
     */
    preg_match_all($re, $assinatura, $mts);
    //Retirar o primeiro elemento (que é nome da função)    
    $mts = array_shift($mts);
    return $mts;
}


/* - - - - - - - -
 * Retorna Array de Parâmetros dado os parâmetros
 * return: array
 */
function pegarParametros($parametrosl){
    $result = array();
    $resto = $parametrosl ;
    while($resto != ""){      
        $seek = 0;

        //Verificar parenteses
        if($resto[$seek] == "("){
            $caract = 1;        
            $seek++;
            while($caract > 0){
                if($resto[$seek] == "(")
                    $caract++;
                if($resto[$seek] == ")")
                    $caract--;                
                $seek++;
                echo $caract;
            }
            $par = substr($resto,0,$seek);
            $result[] = $par;
            $resto = trim(substr($resto,$seek));
            continue;
        }

        //Verificar colchetes
        if($resto[$seek] == "["){
            $caract = 1;        
            $seek++;
            while($caract > 0){
                if($resto[$seek] == "[")
                    $caract++;
                if($resto[$seek] == "]")
                    $caract--;                
                $seek++;
            }
            $par = substr($resto,0,$seek);
            $result[] = $par;
            $resto = trim(substr($resto,$seek));
            continue;
        }    

        //Verificar chaves
        if($resto[$seek] == "{"){
            $caract = 1;        
            $seek++;
            while($caract > 0){
                if($resto[$seek] == "{")
                    $caract++;
                if($resto[$seek] == "}")
                    $caract--;                
                $seek++;
            }
            $par = substr($resto,0,$seek);
            $result[] = $par;
            $resto = trim(substr($resto,$seek));
            continue;
        }    

        //Verificar aspas duplas
        if($resto[$seek] == "\""){
            $seek++;
            while($resto[$seek] != "\""){             
                $seek++;
            }
            $par = substr($resto,0,$seek);
            $result[] = $par;
            $resto = trim(substr($resto,$seek));
            continue;
        }

        //Verificar aspas simples
        if($resto[$seek] == "'"){
            $seek++;
            while($resto[$seek] != "'"){             
                $seek++;
            }
            $par = substr($resto,0,$seek);
            $result[] = $par;
            $resto = trim(substr($resto,$seek));
            continue;
        }

        //Verificar parametro normal
        if($resto[$seek] != " "){
            $re = "/^[\w']+[^\(\[\@]/im";
            /* ^\w+   <- 1 ou mais caracteres no inicio de linha
             * [^\(\[\@] <- e terminar qnd encontrar ( [ ou @
             */
            $mts = preg_replace($re, "", $assinatura);
            return $mts;
        }

    }//while
    
}





/**
 * SAIDA DE TESTES
 */

function relatorio01($idExercicio=23){
 
    $solucoes = buscarTodasSolucoesByExercicioID($idExercicio);
    //$solucoes = buscarTodasSolucoesByExercicioID(39);

    $codigoAnterior = "";
    $txtComparativo = "";    
    
    foreach ($solucoes as $s){

        //$tam = strlen($s->codigohaskell);
        //$lines = explode(PHP_EOL, $s->codigohaskell);

        //FAZENDO LIMPEZAS

        //Funções
        $nomefuncoes = pegarNomesFuncoesPrim($s->codigohaskell);
            $limpo1saida = "<b>Funções:</b>\n";
            foreach($nomefuncoes as $linha){
                $limpo1saida .= $linha." \n";
            }

        //Assinaturas
        $assinaturas = pegarAssinFuncoesPrim($s->codigohaskell);
            $limpo2saida = "<b>Assinaturas:</b>\n";
            foreach($assinaturas as $linha){
                $limpo2saida .= $linha." \n";
            }

        $limpo3saida = "<b>Identificadores:</b>\n";
        foreach($assinaturas as $assin){
            $identfs = pegarIdentifFuncoesPrim($assin);
            foreach($identfs as $id1){
                $limpo3saida .= "$".$id1." ";
            }        
            $limpo3saida .= "<br>";
        }


        //
        // Substituindo os identificadores para comuns
        $code1saida = "<b>Codigo para Comparação:</b>\n";
        $novocodigo = $s->codigohaskell;
        $concatc = 1;
        //Substituindo os nomes das funções por identificadores comuns de função
        foreach($nomefuncoes as $fun){
    //        $novocodigo = preg_replace("/\b$fun\b/", "\$iF$concatc", $novocodigo);        
            $novocodigo = preg_replace("/\b$fun\b/", "", $novocodigo);        
            $concatc++;
        }
        //Substituindo os nomes dos parâmetros por identificadores comuns de função
        foreach($assinaturas as $assin){
            $identfs = pegarIdentifFuncoesPrim($assin);
            foreach($identfs as $id1){
    //            $novocodigo = preg_replace("/\b$id1\b/", "\$iP$concatc", $novocodigo);             
                $novocodigo = preg_replace("/\b$id1\b/", "", $novocodigo);             
                $concatc++;
            }
        }

        $code1saida.=$novocodigo;


        //
        //  REALIZANDO O CÁLCULO DE SIMILARIDADE

        if($codigoAnterior==""){
            //Primeira vez
            $codigoAnterior = $novocodigo;        
        }else{
            similar_text($codigoAnterior, $novocodigo,$porcent);
            $codigoAnterior = $novocodigo;                        
            $txtComparativo = "<- este é $porcent similar ao anterior.";
        }


        //$limpo1 = preg_grep($pattern, $s->codigohaskell);

        //retirar espaços em branco

        //separar palavras


        // IMPRIMINDO NA TELA
        echo "<div style='width:100%; border: 1px solid #ccc; margin: 10px 0; overflow: hidden; clear:both'>",
                "<a href='./?page=mostrasolucao&id=",$s->id,"'>",$s->id,"</a> $txtComparativo <br>",
                "<div style='float:left; margin: 0 0 5px 10px;'>",
                "<pre><b>Codigo Haskell:</b>\n",$s->codigohaskell,"</pre>",
                "</div>",
                "<div style='float:right; padding: 0 10px; margin: 0 0 5px 10px; border: 1px solid blue'>",
                "<pre>",$code1saida,"</pre>",
                "</div>",
                "<div style='clear:both'></div>",
                "<div style='float:left; padding: 0 10px; margin: 0 0 5px 10px; border-left: 1px solid green'>",
                "<pre>",$limpo1saida,"</pre>",
                "</div>",            
                "<div style='float:left; padding: 0 10px; margin: 0 0 5px 10px; border-left: 1px solid green'>",
                "<pre>",$limpo2saida,"</pre>",
                "</div>",
                "<div style='float:left; padding: 0 10px; margin: 0 0 5px 10px; border-left: 1px solid green'>",
                "<pre>",$limpo3saida,"</pre>",
                "</div>",
             "</div>";

    }    
    
}
