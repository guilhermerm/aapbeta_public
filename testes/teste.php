<?php
echo "TESTEEEEE<br>";


$texto = '<div id="resultados" class="tabs-content" style="display:none">
            <div class="control-group resultadotestes">
                <div class="control">            
                    <table class="ink-table alternating" style="border-bottom: 1px solid #AAA; margin-bottom: 5px">
                        <thead>
                            <tr>
                                <th class="align-left all-30">Descrição</th>
                                <th class="align-left all-25">Entrada</th>
                                <th class="align-left all-25">Saída Esperada</th>
                                <th class="align-left">Resultado</th>
                                <th style="width: 10px"></th>
                            </tr>
                        </thead>
                        <tbody id="bodyteste">
                            
                    <tr id="trteste1">
                        <td>Adjacentes na ordem</td>
                        <td>[3,4,5,7,8] 4 5</td>
                        <td style="color:green">True</td>
                        <td style="color:green"> True</td>
                        <td style="color:green"><i class="fa fa-check-square-o"></i></td>                        
                    </tr>
                    <tr id="trteste1">
                        <td>Adjacentes fora de ordem</td>
                        <td>[3,4,7,8,10] 4 3</td>
                        <td style="color:green">False</td>
                        <td style="color:green"> False</td>
                        <td style="color:green"><i class="fa fa-check-square-o"></i></td>                        
                    </tr>
                    <tr id="trteste1">
                        <td>Sem adjacência</td>
                        <td>[3,4,7,8,10] 4 8</td>
                        <td style="color:green">False</td>
                        <td style="color:green"> False</td>
                        <td style="color:green"><i class="fa fa-check-square-o"></i></td>                        
                    </tr>
                        </tbody>
                    </table>
                </div>        
            </div>
        </div>';

//echo str_replace("none", "block", $texto);
echo $texto;