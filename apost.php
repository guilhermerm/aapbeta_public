<?php
//Funções para as requisições AJAX via post
//
//error_reporting (0);
    //Endereços do ambiente
    $endatual = getcwd();
    $endaux   = explode("/",$endatual);
    $endamb   = "/".end($endaux);
require_once "$endatual/apps/admin/mdl/usuario.php";
require_once "$endatual/apps/admin/mdl/usuario_bd.php";
require_once "$endatual/apps/cspl/mdl/comentario.php";
require_once "$endatual/apps/cspl/mdl/comentario_bd.php";
require_once "$endatual/apps/cspl/mdl/exercicio.php";
require_once "$endatual/apps/cspl/mdl/exercicio_bd.php";
require_once "$endatual/apps/cspl/mdl/solucao.php";
require_once "$endatual/apps/cspl/mdl/solucao_bd.php";
require_once "$endatual/apps/cspl/mdl/soluplanoteste.php";
require_once "$endatual/apps/cspl/mdl/soluplanoteste_bd.php";
session_start();

//Verificar se sessão ainda não expirou
if (isset($_SESSION["aeapuser"])){
    $userlogado = $_SESSION["aeapuser"];
}
//}else{
//  //logar
//}

if(isset($_POST['op'])){
    
    $op =$_POST['op'];
    
    switch ($op){
        case 'solicitarnovasenha':
            //require_once "$endatual/lib/enviaEmail.php";    
            
            //Buscar informações do usuário
            $email = $_POST['email'];
            $usuario = buscarUsuarioByMail($email);
            
            //Gerar chave de segurança e link para renovação da senha
            $chave    = $usuario->gerarChave();
            $httphost = $_SERVER ['HTTP_HOST'];
            $link     = "http://$httphost/$endamb/?page=renovarsenha&k=$chave";
                        
            $mensageiro = new enviaEmail();
                       
            $destinatarionome = $usuario->nome;
            $assunto          = "[AAP.inf.ufes.br] Renovação de senha";
            $mensagemHtml     = "<p>Olá $usuario->nome, <br> Foi solicitado a renovação de senha para acessar o ambiente, para proseguir, acesse o link abaixo para criar uma nova senha: <br>
                                 <a href='$link'>$link</a> </p><br> <p style='color:#999'>Caso não solicitou a renovação de senha desconsidere este e-mail.</p>
                                 <p style='text-align: center'><a href='http://aap.inf.ufes.br' style='color:#EEE'><b>AAP</b>.inf.ufes.br</a></p>";
                    
            $mensageiro->prepararEmail($destinatarionome, $email, $assunto, $mensagemHtml);
            
            if($mensageiro->enviarEmail() === true){
                echo "TRUE";
            }else{
                echo "FALSE";
            }   
//            ;
//            
            
        break;
    
        case 'salvarsolucao':
            realizarTeste($_POST,$userlogado);
        break;
    
        case 'socializarSolucao':
            require_once "$endatual/apps/cspl/mdl/solucao.php";
            require_once "$endatual/apps/cspl/mdl/solucao_bd.php";
            $idsolucao = $_POST['idsolucao'];
            $valor     = $_POST['valoratual'];
            //$valor     = ($_POST['valoratual']=='0')?"1":"0";
            
            $result = socializarSolucao($idsolucao,$valor);
            
            if($result > 0){
                echo "TRUE";
            }else{
                echo "FALSE";
            }            
        break;
            
        case 'enviarcomentario':
           enviarComentario($_POST,$userlogado);
        break;
    
        case 'removerLinhaTeste':
            require_once "$endatual/apps/cspl/mdl/exerplanoteste.php";
            require_once "$endatual/apps/cspl/mdl/exerplanoteste_bd.php";
            $idteste = $_POST['idteste'];
            $result = deletarExerTeste($idteste);
            if($result > 0){
                echo "TRUE";
            }else{
                echo "FALSE";
            }
        break;
        
        case 'deletarComentario':
            require_once "$endatual/apps/cspl/mdl/comentario.php";
            require_once "$endatual/apps/cspl/mdl/comentario_bd.php";
            $idcomentario = $_POST['idcomentario'];
            $result = deletarComentario($idcomentario);
            if($result > 0){
                echo "TRUE";
            }else{
                echo "FALSE";
            }            
        break;
        
        case 'deletarSolucao':
            require_once "$endatual/apps/cspl/mdl/solucao.php";
            require_once "$endatual/apps/cspl/mdl/solucao_bd.php";
            $idsolucao = $_POST['idsolucao'];
            $result = deletarSolucao($idsolucao);
            
            if($result > 0){
                echo "TRUE";
            }else{
                echo "FALSE";
            }            
        break;
        
        /*RELATORIOS EXer*/
        case 'solucao2modal':            
            $idsolucao = $_POST['id'];            
            $obj = buscarSolucaoId($idsolucao);
            //Montar o HTML para o Modal
            $html="<div> E ae 
                    </div>";
            echo $html;
        break;
            
    }//switch
    
}else{
    echo "Error - Operação não especificada";
}


/*
 * OPERAÇÕES e Funções
 */

function realizarTeste($p,$userlogado){    
          
    //Inserindo a Solução (MCP)
    $idsolu = ajaxInsereSolucao($p,$userlogado);
    
    //Inserindo Plano de Teste 
    $idsoluplano = ajaxInserePlanoTeste($p,$userlogado,$idsolu);
    
    //Inserindo os Testes
    $solutestes = ajaxInsereTestes($p,$idsoluplano);
    
    
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *  
     *     R E A L I Z A N D O  O  T E S T E
     * 
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    $endereco = './data/cspl/testes/';
    $namecodigo = str_pad($userlogado->id, 4, 0, STR_PAD_LEFT)."-".$idsolu."-".date("His");
    $namehs= "arq_$namecodigo.hs";
    $nameen= "arq_$namecodigo.txt";
    
    //Conteúdo com o código
    $arqhs =  $p["codigohaskell"];
    
    //Conteúdo com as entradas
    $arqen = "";
    foreach($solutestes as $s){
        $arqen .=  $p["funcaoprincipal"]." ".$s->entrada."\n";    
    }

    file_put_contents($endereco.$namehs,$arqhs);
    file_put_contents($endereco.$nameen,$arqen);

    exec("hugs ".$endereco.$namehs." < ".$endereco.$nameen,$out);
    
    //Verificar se não tem erro de sintaxe em $out
    $hugserror = false;
    if("ERROR" == substr($out[9],0,5)){
        $hugserror = true;
    }
    
    $textdebug = "";
    
    if(!$hugserror){
        
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
         *  Genrando a Saída para View
         */        
        
        //$resultados 
        $htmlhugs = "";
        foreach($out as $l){
            if(substr($l,0,5)=='Main>'){
                $resultados[] = substr($l,5);
            }
            $htmlhugs .= $l."<br>";
        }           
        
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
         *  
         *      V a l i d a n d o   o s   t e s t e s
         * 
         */         
        
        //Buscar código para validar teste
        $objValidacao = buscarValidarTesteByExercicioId($p["idexercicio"]);        
        
        //Conteúdo com o código
        //$arqhs =  $p["codigohaskell"];
        $codTeste  = "validar $objValidacao->pentrada saidaesperada resultadoobtido = ";
        $codTeste .= " ".$objValidacao->validarteste;

        //Conteúdo com as entradas
        $arqen = "";
        $arqent = "";
        $c1=0;
        foreach($solutestes as $s){
            //$arqen .=  $p["funcaoprincipal"]." ".$s->entrada."\n";    
            if($objValidacao->pentrada != ""){
                $arqent.= "validar $s->entrada $s->saidaesperada $resultados[$c1] \n";
            }else{
                $arqent.= "validar $s->saidaesperada $resultados[$c1] \n";                
            }
            $c1++;
        }        
                
        file_put_contents($endereco . "valtestecod.hs", $codTeste);
        file_put_contents($endereco . "valtesteent.txt", $arqent);

        exec("hugs " . $endereco . "valtestecod.hs" . " < " . $endereco . "valtesteent.txt", $out2);
                        
        $autiii = "";
        foreach ($out2 as $l2) {
            $autiii .= $l2;
            if (substr($l2, 0, 5) == 'Main>') {
                $resultados2[] = substr($l2, 5);
            }
        }
        

//        foreach($solutestes as $t){
//              //Validação padrão
////            if($t->saidaesperada == trim($resultados[$i])){
////               $valido = true;
////            }
//
//            /* Validar Teste usando o "validarTeste" do exercício */
//        }
        
        
        
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
         *  
         *     G E R A N D O   A   S A I D A    H T M L
         * 
         */   
        // MONTAR O RESULTADO
        //CADA TESTE
        $tudopositivo = true;
        $htmltestestr = "";          
        $i = 0;

        foreach($solutestes as $t){
           $valido = false;                      
                
            if( trim($resultados2[$i]) === "True"){
                $valido = true;
            }
                
            //Exibe o teste
            if($valido){    
            //if($t->saidaesperada == trim($resultados[$i])){
                //Teste invalidado
                $aval = "<i class='fa fa-check-square-o'></i>";
                $style = "style='color:green'";
            }else{
                //Teste validado
                $aval = "<i class='fa fa-square'></i>";
                $style = "style='color:red'";
                $tudopositivo =false;
            }
            $htmltestestr .="
                    <tr id='trteste1'>
                        <td >$t->descricao</td>
                        <td >$t->entrada</td>
                        <td $style >$t->saidaesperada</td>
                        <td $style >$resultados[$i]</td>
                        <td $style >$aval</td>                        
                    </tr>";                    
            $i++;
            //$c2++;
        }

        if($tudopositivo){
                $msgfinal = "<div class='all-100 align-center' style='margin-bottom:15px' >            
                        <h2><i class='fa fa-angellist vertical-nospace'></i> Parabéns!</h2>Todos os resultados foram positivos! <br> Caso já tenha concluído, salve como <b>Solução Completa</b>.
                        <br>
                </div>";
        }else{
            $msgfinal = "<div class='all-100 align-center' style='margin-bottom:15px' >            
                <h2><i class='fa fa-thumbs-o-up vertical-nospace'></i> Continue!</h2>Os resultados parecem não ser positivos, mas continue tentando!
                <br>
            </div>";                                 
        }                
        
        //A SAIDA DO HUGS
        $htmlsaidahugs = "        
            <div class='result200 all-100' style=''>
                $htmlhugs
            </div>";


        //A TABELA DE RESULTADOS
        $htmltableresultados = "        
            <div class='control-group resultadotestes'>
                <div class='control'>            
                    <table class='ink-table alternating' style='border-bottom: 1px solid #AAA; margin-bottom: 5px'>
                        <thead>
                            <tr>
                                <th class='align-left all-30'>Descrição</th>
                                <th class='align-left all-25'>Entrada</th>
                                <th class='align-left all-25'>Saída Esperada</th>
                                <th class='align-left'>Resultado</th>
                                <th style='width: 10px'></th>
                            </tr>
                        </thead>
                        <tbody id='bodyteste'>
                            $htmltestestr
                        </tbody>
                    </table>
                </div>        
            </div>         
            ";            
        
        //SALVANDO O RESULTADO
        $soluresultado = new soluresultado();
        $soluresultado->idplanoteste   = $idsoluplano;
        $soluresultado->saidahugs      = addslashes($htmlhugs);
        $soluresultado->saidaresultado = addslashes($htmltableresultados);
        $idsoluresultado = inserirSoluResultado($soluresultado);


        //TODO O CONJUNTO INK (RESULTADOS +SAIDA HUGS)
        $htmlfinal = "
        <div class='ink-tabs left boxresultados' data-prevent-url-change='true' style='margin-bottom: 10px'>            
            <ul class='tabs-nav' style='margin: 0 10px 0 0'>
                <li><a class='tabs-tab' href='#resultados' style='padding: 2px; font-size: 12px'>Resultados</a></li>
                <li><a class='tabs-tab' href='#saidahugs'  style='padding: 2px; font-size: 12px'>Saída Hugs</a></li>                
            </ul>
            <div id='resultados' class='tabs-content'>
                $htmltableresultados
            </div>
            <div id='saidahugs' class='tabs-content'>
                $htmlsaidahugs
            </div>
        </div>            
            $msgfinal
            ";
                                
        //Montando JSON para retorno Ajax
        //Alteramos o cabeçalho para não gerar cache do resultado
        header('Cache-Control: no-cache, must-revalidate'); 
        //Alteramos o cabeçalho para que o retorno seja do tipo JSON
        header('Content-Type: application/json; charset=utf-8');
        //Criando Vetor
        $vetor['idsolucao'] = $idsolu;
        $vetor['html']      = $htmlfinal;
        $vetor['textdebug'] = $textdebug;
        //Codificando vetor para json
        echo json_encode($vetor);        
        
    }else{
        //$hugserror
        // Erro de Sintaxe, exibir apenas a saída do hugs        
        $htmlhugs = "";
        foreach($out as $l){
            $htmlhugs .= $l."<br>";
        }        
        //A SAIDA DO HUGS
        $htmlsaidahugs = "
        <div id='saidahugs' class='tabs-content'>
            <div class='result all-100' style=''>
                $htmlhugs
            </div>
        </div>        
        ";    
                
        //Montagem final
        $htmlfinal = $htmlsaidahugs;

        //Montando JSON para retorno Ajax
        //Alteramos o cabeçalho para não gerar cache do resultado
        header('Cache-Control: no-cache, must-revalidate'); 
        //Alteramos o cabeçalho para que o retorno seja do tipo JSON
        header('Content-Type: application/json; charset=utf-8');
        //Criando Vetor
        $vetor['idsolucao'] = $idsolu;
        $vetor['html']      = $htmlfinal;
        $vetor['textdebug'] = $textdebug;
        //Codificando vetor para json
        echo json_encode($vetor);        
        
    }
    
        
}


function ajaxInsereSolucao($p,$userlogado){
    
    $solucao = new solucao();       
    
    $solucao->idexercicio     = $p["idexercicio"];
    $solucao->iduser          = $userlogado->id;
    $solucao->compreensao     = addslashes($p["compreensao"]);
    $solucao->solucao         = addslashes($p["solucao"]);
    $solucao->codigohaskell   = addslashes($p["codigohaskell"]);
    $solucao->funcaoprincipal = addslashes($p["funcaoprincipal"]);
    $solucao->estado          = addslashes($p["estado"]);    
    $solucao->socializar      = 0;    
    $solucao->deletado        = 0;    
    
    $idsolu = inserirSolucao($solucao);
    return $idsolu;
    
}
function ajaxInserePlanoTeste($p,$userlogado,$idsolu){
    //CRIANDO O PLANO DE TESTE
    $soluplano = new soluplanoteste();    
    
    $soluplano->idsolucao = $idsolu;
    $soluplano->iduser    = $userlogado->id;
    $soluplano->deletado  = 0;
    
    $idsoluplano = inserirSoluPlano($soluplano);
    return $idsoluplano;
}

function ajaxInsereTestes($p,$idsoluplano){    
//CRIANDO OS TESTES
    $tests = array();
    parse_str($p['testes'], $tests);
    //ORGANIZANDO OS TESTES NO VETOR DE TESTES

    foreach ($tests as $a => $b) {

        if (trim(substr($a, 0, 5)) === "descr") {

            $indice = trim(substr($a, 9));

            $soluteste = new soluteste();

            $soluteste->idplanoteste  = $idsoluplano;
            $soluteste->descricao     = addslashes($b);
            $soluteste->entrada       = addslashes(trim($tests["entrada$indice"]));
            $soluteste->saidaesperada = addslashes(trim($tests["saidaes$indice"]));
            $soluteste->deletado = 0;

            $solutestes[] = $soluteste;
        }
    }
    inserirSoluTestes($solutestes);
    return $solutestes;
}


//Operação AJAX para enviar um comentário
function enviarComentario($p,$userlogado){
    
    $comentario = new comentario();
    
    $comentario->iduser    = $userlogado->id;
    $comentario->idsolucao = $p['idsolucao'];
    $comentario->texto     = $p['texto'];
    
    $idgerado = inserirComentario($comentario);
    
    if($idgerado !== FALSE){
        echo "TRUE";
    }else{
        echo "Error";        
    }
    
}